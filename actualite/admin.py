# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from sh.admin import admin_site

from django.contrib import admin
from actualite.models import Actualite
from actualite.forms import ActualiteForm
# Register your models here.


@admin.register(Actualite, site=admin_site)
class ActualiteAdmin(admin.ModelAdmin):
#	form = ActualiteForm

	fieldsets = (
		("Actualité", {
			'fields': (
				('titre', 'date'),
                ('nom','prenom','resume'),
                ('img_actu'),
				('post'),

			)}
		),
    )

	list_display = ('date', 'titre','nom','prenom')
	list_display_links = ('date','titre','nom','prenom')
	list_filter = ('date','titre','nom')
	search_fields = ('date', 'titre','nom','prenom')

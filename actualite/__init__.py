# -*- coding: utf-8 -*-


TYPES_BIENS = (
	('', u"Type de Bien"),
	('a', u"Appartement"),
	('m', u"Maison"),
	('l', u"Loft"),
	('h', u"Hôtel Particulier"),
	('c', u"Château"),
	('v', u"Villa"),
	('p', u"Parking"),
	('b', u"Box"),
	('i', u"Immeuble"),
	('e', u"Chalet"),
	('t', u"Terrain")
)

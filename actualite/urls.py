from django.conf.urls import url
from actualite.views import ActualiteListView
from actualite.views import ActualiteView

app_name = 'actualite'

urlpatterns = [

	url(r'actualites/$',
		ActualiteListView.as_view(),
		name='actualiteList'
	),

	url(r'actualite/(?P<pk>\d+)/$',
		ActualiteView.as_view(),
		name='actualite'
	)


]

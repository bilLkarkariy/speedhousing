import logging

from django import template
from num2words import num2words

logger = logging.getLogger(__name__)

register = template.Library()
@register.filter
def divide(value,arg):
    return int(value%arg)

@register.filter
def getlist(query_dict, key):
    return query_dict.getlist(key)


@register.filter
def getlist(query_dict, key):
    return query_dict.getlist(key)

@register.filter
def hash(h, key):
    return key[1]

@register.filter
def hash_value(h, key):
    return key[0]

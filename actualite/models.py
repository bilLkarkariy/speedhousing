# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from actualite.utils import get_file_path
from tinymce.models import HTMLField


class Actualite(models.Model):

	titre = models.CharField(u"Titre", blank=True, null=False, max_length=255)
	nom = models.CharField(u"Nom", blank=True, null=False, max_length=255)
	prenom = models.CharField(u"Prenom", blank=True, null=False, max_length=255)
	date = models.DateTimeField(editable=True)
	post = HTMLField()
	img_actu= models.FileField(u"Image Article", upload_to=get_file_path, null=False, default=None)

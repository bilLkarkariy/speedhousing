# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from actualite.forms import ActualiteForm
from django.views.generic.list import ListView
from actualite.models import Actualite
from django.views.generic.detail import DetailView
from biens.models import Bien
from actualite import TYPES_BIENS

# Create your views here

class ActualiteListView(ListView):
    model=Actualite

    # def get_context_data(self, **kwargs):
	# 	context = super(ActualiteView, self).get_context_data(**kwargs)
    #
	# 	return context
    def getTypeBien(self):
        return TYPES_BIENS


    def actualites(self):
        return Actualite.objects.all().order_by('-date')

    def get_queryset(self):
		queryset = super(ActualiteListView, self).get_queryset()
		return queryset


class ActualiteView(DetailView):
	model = Actualite

	def actualite(self):
		return Actualite.objects.filter(self.object)

	def get_queryset(self):
		queryset = super(ActualiteView, self).get_queryset()
		return queryset

	def getTypeBien(self):
		return TYPES_BIENS

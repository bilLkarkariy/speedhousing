from django.contrib.admin.models import LogEntry, CHANGE
from django.contrib.contenttypes.models import ContentType
from os.path import join
from uuid import uuid4

from sh.settings import MEDIA_ROOT


def get_file_path(instance, filename):
    import uuid, os
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    path = "documents/%s/" % uuid.uuid4()
    return os.path.join(path, filename)

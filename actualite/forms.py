from django import forms
from actualite.models import Actualite

class ActualiteForm(forms.ModelForm):

    class Meta:
        model = Actualite
        fields = ['titre','date','nom','prenom','img_actu','post']

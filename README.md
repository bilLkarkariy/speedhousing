
Branche de Théophane
User (In the fixture clean.json):
------------------------------------------

| Role      | Email                  | Password |
|-----------|------------------------|----------|
| Admin     | admin@speedhousing.com | test     |
| Agent     | agent1@gmail.com       | test     |
| Agent     | agent2@gmail.com       | test     |
| Acquereur | acquereur1@gmail.com   | test     |
| Acquereur | acquereur2@gmail.com   | test     |
| Vendeur   | vendeur1@gmail.com     | test     |
| Vendeur   | vendeur2@gmail.com     | test     |


Generate the database:
----------------------

This will initialise the database structure & populate it
`python manage.py migrate`
`python manage.py loaddata clean.json`



Envoyer une notification:
-------------------------
Importer la méthode utils
`from utilisateurs.utils import notify`

Executer le code suivant pour envoyer une notif
`notify(agent, u'Nouvelle visite')`



Envoyer un email:
-------------------------
Importer la méthode utils
`from utilisateurs.utils import notify_email`

Executer le code suivant pour envoyer un email
`notify_email(agent.email, u"Nouvelle visite", u"email/pro/nouvelle_visite.html", {
					u"fullname": agent.get_full_name(),
					u"fullname_acheteur": acheteur.get_full_name(),
					u"date": rdv.creneau,
					u"titre_annonce": bien,
					u"reference": bien.nb_mandat
				})`


Gérer les tâches crons (pour envoi d'email régulier):
--------------------------------------------------------
Le plugin installé pour gérer les tâches crons est https://github.com/kraiz/django-crontab.
Il faut que crontab soit installé sur son environnement.

A chaque fois que le fichier sh/cron.py est mis à jour, il faut supprimer la tâche cron précédement définie et la recréer grâce aux commandes suivantes :
> python manage.py crontab remove
> python manage.py crontab add


Problèmes observé dans le projet
--------------------------------

- Il ne faut pas mettre la base de donnée (meme de test) dans le dépot, cela alourdi le dépot (plutot utiliser des fixtures)
- Dans le requierements.txt, mettre des versions précises des librairies peut-etre une bonne chose (ex: asn1crypto==0.23.0) mais entraine un veillissement des librairies et diminue la compatibilité
- Le fichier README.md devrait contenir plus d'information sur le fonctionnement du projet et ses caractéristiques, les commandes indiqué actuellement ne sont pas très propres
- Les fichiers *.pyc ne doivent jamais être sur le dépot (ils sont relatif a l'architecture qui a build le projet)
- (Du à l'utilisation de Python 3) Erreur d'installation (pour suds -> suds-jerko)
- (Du à l'utilisation de Python 3) Erreur de démarrage ("Could not find the GDAL library"), après plusieurs test seule une installation unix à permit de régler l'erreur (sudo apt-get install libgal1-dev)

Remarques
----------

- Le projet est codé en Python 2, il faut passer en Python 3 car le python 2 ne sera plus suporté d'ici quelques temps
- Le projet utilise Django 1.11 alors que la version 2.0 est sorti depuis quelques temps et qu'elle évolue vite
- Il y a énormement de librairie dans le requirements.txt c'est étrange par rapport a la taille du projet
- Dans le Readme/Fixtures il manque des comptes de test (de toutes categories) (afin de pouvoir tester chaques fonctionnalitées)
- Il y a une application "geo" qui n'a pas l'air d'etre une vrai application (1 model: GeoLocalisable mais pas de migrations) -> Inutile?
- Il y a un dossier "documents" c'est un media (terme Django) il devrait être dans un dossier média et ne surtout pas être sur le dépot (sauf fixtures)
- Pareil pour le dossier "images"
- Les noms des fichiers statiques c'est le bazar (home_1.png.png, home_2.png, home_4.1.png...)

Questions
----------
- Pourquoi avoir besoin d'un serveur Node sur un projet Python?


OLD
========================

installation spatialite
------------------------

sudo apt install libsqlite3-mod-spatialite


installation node.js et npm
----------------------------

curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs


installation bower
-------------------

npm install -g bower



installation du projet
----------------------

	* virtualenv
	```
	$ virtualenv dossier
	$ cd dossier
	$ source bin/activate
	```

	* recuperation du code source
	```
	$ git clone https://bitbucket.org/shneda/dev-sh-v1.git
	$ cd dev-sh-v1
	```


installation des dependances
-----------------------------

./manage.py bower install


lancement du serveur de developpement
--------------------------------------
git pull &&
	find . -name *pyc -exec rm {} \; &&
	./manage.py migrate &&
	./manage.py runserver <ip>:<port>

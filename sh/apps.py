# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.admin.apps import AdminConfig

class SHAdminConfig(AdminConfig):
    default_site = 'sh.admin.SHAdminSite'


# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import redirect
from django.urls import reverse
from django.http import HttpResponse
from django.views.generic.list import ListView
from django.contrib.auth.views import PasswordResetView as BasePasswordResetView

from biens.models import Bien
from biens import TYPES_BIENS
from utilisateurs.models import Client, Agent

import logging

logger = logging.getLogger(__name__)


class PasswordResetView(BasePasswordResetView):
	email_template_name = 'registration/password_reset_email.txt'
	html_email_template_name = 'registration/password_reset_email.html'


class HomeBienListView(ListView):
	model = Bien
	template_name = "base.html"
	queryset = Bien.objects.exclude(valide=None).order_by(
		'-modification', '-creation')[:10]

	def get_context_data(self, *args, **kwargs):
		context = super(HomeBienListView, self).get_context_data(*args, **kwargs)
		context['TYPES_BIENS'] = TYPES_BIENS
		return context 

	# def get_queryset(self):
	# 	queryset = Bien.objects.exclude(valide=None)

	# 	search = self.request.POST.get('search', None)
	# 	if search:
	# 		queryset = queryset.filter(
	# 			Q(ville=search) | Q(code_postal=search)
	# 		)
	# 	type = self.request.POST.get('type', None)
	# 	if type:
	# 		queryset = queryset.filter(type=type)
	# 	prix = self.request.POST.get('prix', None)
	# 	if prix:
	# 		queryset = queryset.filter(prix__lte=prix)
	# 	surface = self.request.POST.get('surface', None)
	# 	if surface:
	# 		queryset = queryset.filter(surface=surface)


	# 	return queryset.order_by('-modification', '-creation')

	def get(self, request, *args, **kwargs):
		return super(HomeBienListView, self).get(request, *args, **kwargs)

def cron_task(request):
	"""
	Check if any actions must be executed, if yes, execute it
	"""
	print(u'Cron Task execution')
	# METTRE ICI LES CONDITIONS POUR FAIRE DES TACHES AUTOMATIQUES
	return HttpResponse("")


def login_redirect(request):
	if request.user.is_authenticated:
		if request.user.is_staff or request.user.is_superuser:
			return redirect(reverse('admin:index'))
		if hasattr(request.user, 'agent') and request.user.agent is not None:
			return redirect(reverse('utilisateurs:espace-agent'))
		if hasattr(request.user, 'client') and request.user.client is not None:
			return redirect(reverse('utilisateurs:espace-client'))
	return redirect(reverse('home'))


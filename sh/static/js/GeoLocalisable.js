$(function(){

	var $adresse = $('#id_adresse');
	var $code_postal = $('#id_code_postal');
	var $ville = $('#id_ville');


	var placesAutocomplete = places({
		container: $adresse.get(0),
		type: 'address',
		countries: ['fr'],
		language: 'fr'
	});

	placesAutocomplete.on('change', function(e){
		var result = e.suggestion; 
		$adresse.val(result.name);
		$code_postal.val(result.postcode);
		$ville.val(result.city);
	});

	placesAutocomplete.on('clear', function(e){
		$adresse.val('');
		$code_postal.val('');
		$ville.val('');
	});

});
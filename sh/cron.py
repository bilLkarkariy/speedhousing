# -*- coding: utf-8 -*-

from agenda.models import RDV
from dateutil.relativedelta import relativedelta
import datetime, logging
from utilisateurs.utils import notify, notify_email


logger = logging.getLogger(__name__)

# ================================[ cron jobs ]===============================


from django.utils import timezone
from datetime import timedelta
from biens.models import Bien

def notif_rappel():
	#biens = Bien.objects.all()
	MAINTENANT = datetime.datetime.now()
	rdvs = RDV.objects.filter(crenau__gt=MAINTENANT, deleted=None).all()
	logger.info("Running cron on time %s got rdvs %s" % (MAINTENANT, list(rdvs)))
	try:
		for rdv in rdvs:
			delta1 = rdv.crenau - relativedelta(hours=2)
			delta2 = rdv.crenau - relativedelta(hours=1, minutes=45, seconds=1)
			logger.info("checking if it's between %s and %s" % (delta1, delta2))

			if rdv.acheteur is None:
				acheteur = rdv.agent_client
			else:
				acheteur = rdv.acheteur

			if delta1 <= MAINTENANT <= delta2:
				# acheteur
				if rdv.acheteur is not None:
					notify(acheteur.user, u'Rappel de visite')
					notify_email(acheteur.user.email, u"Rappel visite", u"email/acheteur/rappel_visite.html", {
						u"fullname": acheteur.get_full_name(),
						u"date": rdv.crenau,
						u"titre_annonce": rdv.bien,
						u"reference": rdv.bien.nb_mandat,
						u"civilite_agent": rdv.agent.civilite,
						u"fullname_agent": rdv.agent.get_full_name(),
						u"telephone_agent": rdv.agent.mobile
					})
					logger.info("Notification sent to the buyer %s" % acheteur.get_full_name())
				# vendeur
				notify(rdv.vendeur.user, u'Rappel de visite')
				notify_email(rdv.vendeur.user.email, u"Rappel visite", u"email/vendeur/rappel_visite.html", {
					u"fullname": rdv.vendeur.get_full_name(),
					u"date": rdv.crenau,
					u"titre_annonce": rdv.bien,
					u"reference": rdv.bien.nb_mandat,
					u"civilite_agent": rdv.agent.civilite,
					u"fullname_agent": rdv.agent.get_full_name(),
					u"telephone_agent": rdv.agent.mobile
				})
				logger.info("Notification sent to the seller %s" % rdv.vendeur.get_full_name())

				# agent
				notify(rdv.agent.user, u'Rappel de visite')
				notify_email(rdv.agent.user.email, u"Rappel visite", u"email/pro/rappel_visite.html", {
					u"fullname": rdv.agent.get_full_name(),
					u"fullname_client": acheteur.get_full_name(),
					u"date": rdv.crenau,
					u"titre_annonce": rdv.bien,
					u"reference": rdv.bien.nb_mandat,
					u"civilite_vendeur": rdv.vendeur.civilite,
					u"fullname_vendeur": rdv.vendeur.get_full_name(),
					u"telephone_vendeur": rdv.vendeur.mobile,
					u"adresse_bien": rdv.vendeur.get_adresse()
				})
				logger.info("Notification sent to the agent %s" % rdv.agent.get_full_name())
				logger.info("~~~ SUCCESS ~~~")
	except Exception as ex:
		logger.exception(ex)


def send_notifications():
	MAINTENANT = datetime.datetime.now()
	rdvs = RDV.objects.filter(crenau__gt=MAINTENANT, deleted=None).all()
	logger.info("Running cron on time %s got rdvs %s" % (MAINTENANT, list(rdvs)))

	try:
		for rdv in rdvs:
			delta1 = rdv.crenau - relativedelta(hours=2)
			delta2 = rdv.crenau - relativedelta(hours=1, minutes=45, seconds=1)
			print "rdv delta 1 : " + str(delta1)
			logger.info("checking if it's between %s and %s" % (delta1, delta2))

			if rdv.acheteur is None:
				acheteur = rdv.agent_client
			else:
				acheteur = rdv.acheteur

			if delta1 <= MAINTENANT <= delta2:
				print str(MAINTENANT)
				# acheteur
				if rdv.acheteur is not None:
					notify(acheteur.user, u'Rappel de visite')
					notify_email(acheteur.user.email, u"Rappel visite", u"email/acheteur/rappel_visite.html", {
						u"fullname": acheteur.get_full_name(),
						u"date": rdv.crenau,
						u"titre_annonce": rdv.bien,
						u"reference": rdv.bien.nb_mandat,
						u"civilite_agent": rdv.agent.civilite,
						u"fullname_agent": rdv.agent.get_full_name(),
						u"telephone_agent": rdv.agent.mobile
					})
					logger.info("Notification sent to the buyer %s" % acheteur.get_full_name())
				# vendeur
				notify(rdv.vendeur.user, u'Rappel de visite')
				notify_email(rdv.vendeur.user.email, u"Rappel visite", u"email/vendeur/rappel_visite.html", {
					u"fullname": rdv.vendeur.get_full_name(),
					u"date": rdv.crenau,
					u"titre_annonce": rdv.bien,
					u"reference": rdv.bien.nb_mandat,
					u"civilite_agent": rdv.agent.civilite,
					u"fullname_agent": rdv.agent.get_full_name(),
					u"telephone_agent": rdv.agent.mobile
				})
				logger.info("Notification sent to the seller %s" % rdv.vendeur.get_full_name())

				# agent
				notify(rdv.agent.user, u'Rappel de visite')
				notify_email(rdv.agent.user.email, u"Rappel visite", u"email/pro/rappel_visite.html", {
					u"fullname": rdv.agent.get_full_name(),
					u"fullname_client": acheteur.get_full_name(),
					u"date": rdv.crenau,
					u"titre_annonce": rdv.bien,
					u"reference": rdv.bien.nb_mandat,
					u"civilite_vendeur": rdv.vendeur.civilite,
					u"fullname_vendeur": rdv.vendeur.get_full_name(),
					u"telephone_vendeur": rdv.vendeur.mobile,
					u"adresse_bien": rdv.vendeur.get_adresse()
				})
				logger.info("Notification sent to the agent %s" % rdv.agent.get_full_name())
				logger.info("~~~ SUCCESS ~~~")
	except Exception as ex:
		logger.exception(ex)

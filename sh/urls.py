from django.conf.urls import include, url

from sh.admin import admin_site

from django.views.generic import TemplateView
from sh.views import PasswordResetView

from sh import settings
from django.conf.urls.static import static

from sh.views import HomeBienListView, cron_task, login_redirect
import notifications.urls

urlpatterns = [
    url(r'^sh/', include('jet.urls', 'jet')),
    # url(r'^sh/dashboard/', include('jet.dashboard.urls', 'jet-dashboard')),
    url(r'^admin/', admin_site.urls),
    url(r'^auth/password_reset/$', PasswordResetView.as_view(), name='password_reset'),
    url(r'^auth/', include('django.contrib.auth.urls')),
    url(r'^biens/', include('biens.urls')),
    url(r'^utilisateurs/', include('utilisateurs.urls')),
    url(r'^agenda/', include('agenda.urls')),
    url(r'^actualite/', include('actualite.urls')),

    url(r'^support/', include('support.urls')),
    url(r'^inbox/notifications/', include(notifications.urls, namespace='notifications')),
    url(r'^$', HomeBienListView.as_view(), name='home'),
    url(r'^$', HomeBienListView.as_view(), name='contact'),
    url(r'^a-propos/$', TemplateView.as_view(template_name='about.html'),
        name='about'),
    url(r'^legal/$', TemplateView.as_view(template_name='legal.html'),
        name='legal'),
    url(r'^cron$', cron_task),
	# Post login
	url(r'^accounts/profile/', login_redirect, name='login-redirect'),
    #url(r'^redactor/', include('redactor.urls'))
    url(r'^tinymce/', include('tinymce.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

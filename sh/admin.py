# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext as _

from django.contrib import admin
from django.views.decorators.cache import never_cache

from biens.models import BienAValider, Bien, Offre
from agenda.models import RDV
from utilisateurs.models import AgentAValider


import logging

logger = logging.getLogger(__name__)


class SHAdminSite(admin.AdminSite):
	site_header = 'Admin SPEEDHOUSING'

	@never_cache
	def index(self, request, extra_context=None):
		"""
			Surcharge de la vue index admin pour inserer des donnees
			de contexte supplementaires.
		"""

		if request.user.is_superuser:
			if extra_context is None:
				extra_context = {}

			extra_context['show_counters'] = True
			extra_context['cpt_biens_ajoutes'] = BienAValider.objects.filter(source=None).count()
			extra_context['cpt_biens_modifies'] = BienAValider.objects.exclude(source=None).count()
			extra_context['cpt_agents_ajoutes'] = AgentAValider.objects.filter(source=None).count()
			extra_context['cpt_agents_modifies'] = AgentAValider.objects.exclude(source=None).count()
			extra_context['cpt_offre'] = Offre.objects.filter(refusee=None, acceptee=None).count()
			extra_context['cpt_rdv'] = RDV.objects.all().count()
			extra_context['cpt_sum'] = sum(extra_context[k]
				for k in ['cpt_biens_ajoutes', 'cpt_biens_modifies', 'cpt_agents_ajoutes', 'cpt_agents_modifies']
			)
			extra_context['offres'] = Offre.objects.filter(refusee=None, acceptee=None)
			extra_context['RDV'] = RDV.objects.all()


		return super(SHAdminSite, self).index(request, extra_context=extra_context)


admin_site = SHAdminSite()

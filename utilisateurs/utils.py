# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.admin.models import LogEntry, CHANGE
from django.contrib.contenttypes.models import ContentType
from django.contrib.gis.db.models.functions import Distance
from os.path import join
from uuid import uuid4
from notifications.signals import notify as notifier
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.urls import reverse
from django.conf import settings
from django.utils.html import strip_tags
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email.Utils import COMMASPACE, formatdate
from email import Encoders
import os

import logging

logger = logging.getLogger(__name__)

def add_logentry_agentavalider(agent_id, agent, changes):
    LogEntry.objects.log_action(
        user_id=agent_id,
        content_type_id=ContentType.objects.get(app_label="utilisateurs", model="agentavalider").pk,
        object_id=agent.id,
        object_repr=unicode(agent),
        action_flag=CHANGE,
        change_message=unicode(changes))

def get_logentry_agentavalider(agent_id):
    return LogEntry.objects.filter(
        object_id=agent_id,
        content_type_id=ContentType.objects.get(app_label="utilisateurs", model="agentavalider").id
        ).order_by('action_time')

def notify(to, message, from_user=None):
    """
    Send a notification to the object "to" from "from_elem" ("to" by default), the content is "message"

    :param User to: The receiver
    :param QuerySet<User> to: The receivers (Queryset of object). If queryset, you need to specify 'from_user'
    :param str message: The message received (in unicode -> u'my message')
    :param from_user: (Optionnal) The sender (by default it's the receiver)
    """
    if from_user is None:
        from_user = to
    notifier.send(from_user, recipient=to, verb=message)

    logger.debug("Notified user : %s / Message : %s" % (to, message))


def notify_email(receiver, subject, template, context_template, sender=settings.DEFAULT_FROM_EMAIL, files=[]):
    msg_html = render_to_string(template, context_template)

    msg = MIMEMultipart('alternative')
    msg['From'] = sender
    msg['To'] = receiver
    msg['Reply-to'] = settings.DEFAULT_REPLY_TO
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subject.encode('utf-8')

    msg_text = MIMEText(strip_tags(msg_html).encode('utf-8'), 'plain', 'utf-8')
    content_html = MIMEText(msg_html.encode('utf-8'), 'html', 'utf-8')


    msg.attach(msg_text)
    msg.attach(content_html)

    for path in files:
        part = MIMEBase('application', "octet-stream")
        with open(path, 'rb') as file:
            part.set_payload(file.read())
        Encoders.encode_base64(part)
        part.add_header('Content-Disposition',
                        'attachment; filename="{}"'.format(os.path.basename(path)))
        msg.attach(part)

    if not isinstance(receiver, list):
        receiver = [receiver]
    try:
        smtp = smtplib.SMTP()
        smtp.connect(getattr(settings, "EMAIL_HOST", None), getattr(settings, "EMAIL_PORT", None))
        smtp.starttls()
        smtp.login(getattr(settings, "EMAIL_HOST_USER", None), getattr(settings, "EMAIL_HOST_PASSWORD", None))
        smtp.sendmail(sender, receiver, msg.as_string())
        smtp.quit()
        logger.debug("Notified user : %s / Message : %s" % (receiver, msg_html))
    except Exception as e:
        logger.error(e)


def send_alert_notification(request, bien, exclude=[]):
    from utilisateurs.models import Agent
    from biens.models import Bien

    if not isinstance(bien, Bien):
        logger.warn('An error occured with method `utilisateurs.utils.send_alert_notifications`')
        return False

    result = list(Agent.objects.exclude(
        valide=False,
    ).annotate(
        distance_agent = Distance(
            'localisation',
            bien.vendeur.localisation
        )
    ).order_by('distance_agent')[:10])

    agents = [agent for agent in result if agent.pk not in exclude]

    if len(agents) > 0:
        for agent in agents:
            notify_email(agent.email, u"Un nouveau bien est disponible !", u"email/pro/alerte_nouveau_bien.html", {
                u"fullname": agent.get_full_name(),
                u"reference": bien.nb_mandat,
                u"titre_annonce": bien,
                u"url_bien": request.build_absolute_uri(reverse('annonce', args=[bien.nb_mandat])).replace('http://', 'https://')
            })

# -*- coding: utf-8 -*-

from django.conf.urls import url
from django.urls import reverse

from django.conf import settings

from django.views.generic import TemplateView
from django.views.generic.edit import DeleteView
from biens.models import Bien

# Inscription Agent / Client
from utilisateurs.views import (
	ClientCreateView, ClientUpdateView, AgentCreateView, AgentUpdateView,
	EmailConfirmView, ClientCreateDisabledView,
)


# Espace client Vendeur
from utilisateurs.views import (
	MesAnnoncesListView, AnnonceDocumentsDetailView, AnnonceVisitesDetailView,AnnonceDispoUpdateView,
	AnnonceOffresDetailView, AnnonceJoursOffDetailView, AnnonceUpdateView, AnnonceAValiderUpdateView,
	AgentOffreDetailView, AnnonceUpdateRelatedDetailView, AnnonceAValiderUpdateRelatedDetailView,
	BienAValiderDeleteView, BienDeleteView, AnnonceDeleteView, AnnonceAValiderDeleteView, AgentAValiderDeleteView
)

# Espace client Acquereur
from utilisateurs.views import (
	MesFavorisListView, MesVisitesListView,

)

# Espace agent fiches clients
from utilisateurs.views import (
	AgentClientCreateView, AgentClientListView,
	AgentClientUpdateView, AgentClientDeleteView
)

# Espace agent listes de favoris
from utilisateurs.views import (
	FavorisListeCreateView, FavorisListeListView,
	FavorisListeDeleteView, FavorisListeDetailView
)

# Espace agent actualites
from utilisateurs.views import AgentActualitesView

# Espace agent visites
from utilisateurs.views import AgentVisiteListView

# Espace agent ventes
from utilisateurs.views import AgentVenteListView, AgentOffreListView

# Notification
from utilisateurs.views import read_notification

app_name = 'utilisateurs'

urlpatterns = [

	# Espace client:
	# 	- Acheteur
	# 	- Acquereur
	url(r'^inscription/client/$',
		ClientCreateView.as_view(),
		name='inscription-client'
	), #if settings.CLIENT_SUBSCRIPTION_OPENED else
	#url(r'^inscription/client/$',
	#	ClientCreateDisabledView.as_view(),
	#	name='inscription-client'
	#),

	url(r'^confirmation/(?P<token>[0-9a-f]{8}(-[0-9a-f]{4}){3}-[0-9a-f]{12})/$',
		EmailConfirmView.as_view(),
		name="confirmation-email"
	),

	url(r'^modification/client/$',
		ClientUpdateView.as_view(),
		name='modification-client'
	),

	url(r'^espace/client/$',
		TemplateView.as_view(
			template_name="utilisateurs/clients/espace.html"
		),
		name='espace-client'
	),

	# Espace Vendeur
	url(r'^espace/client/mes-annonces/$',
		MesAnnoncesListView.as_view(),
		name='mes-annonces'
	),
	url(r'^espace/client/mes-annonces/(?P<nb_mandat>\d+)/documents/$',
		AnnonceDocumentsDetailView.as_view(),
		name='mes-annonces-documents'
	),
	url(r'^espace/client/mes-annonces/(?P<nb_mandat>\d+)/visites/$',
		AnnonceVisitesDetailView.as_view(),
		name='mes-annonces-visites'
	),
	url(r'^espace/client/mes-annonces/(?P<nb_mandat>\d+)/offres/$',
		AnnonceOffresDetailView.as_view(),
		name='mes-annonces-offres'
	),
	url(r'^espace/client/mes-annonces/(?P<nb_mandat>\d+)/jours-off/$',
		AnnonceJoursOffDetailView.as_view(),
		name='mes-annonces-jours-off'
	),
	url(r'^espace/client/mes-annonces/(?P<nb_mandat>\d+)/supprimer/$',
		AnnonceDeleteView.as_view(),
		name='mes-annonces-supprimer'
	),
	url(r'^espace/client/mes-annonces/(?P<nb_mandat>\d+)/modifier/$',
		AnnonceUpdateView.as_view(),
		name='mes-annonces-modifier'
	),
	url(r'^espace/client/mes-annonces-dispos/(?P<nb_mandat>\d+)/modifier/$',
		AnnonceDispoUpdateView.as_view(),
		name='mes-annonces-dispos-modifier'
	),
	url(r'^espace/client/mes-annonces-a-valider/(?P<nb_mandat>\d+)/modifier/$',
		AnnonceAValiderUpdateView.as_view(),
		name='mes-annonces-a-valider-modifier'
	),
	url(r'^espace/client/mes-annonces/(?P<nb_mandat>\d+)/modifier/infos/$',
		AnnonceUpdateRelatedDetailView.as_view(),
		name='mes-annonces-modifier-infos'
	),
	url(r'^espace/client/mes-annonces-a-valider/(?P<nb_mandat>\d+)/modifier/infos/$',
		AnnonceAValiderUpdateRelatedDetailView.as_view(),
		name='mes-annonces-a-valider-modifier-infos'
	),
	url(r'^espace/client/mes-annonces-a-valider/(?P<nb_mandat>\d+)/supprimer/$',
		AnnonceAValiderDeleteView.as_view(),
		name='mes-annonces-a-valider-supprimer'
	),

	# Espace Acquereur
	url(r'^espace/client/mes-favoris/$',
		MesFavorisListView.as_view(),
		name='mes-favoris'
	),
	url(r'^espace/client/mes-visites/$',
		MesVisitesListView.as_view(),
		name='mes-visites'
	),

	# Espace Agent

	url(r'^modification/agent/$',
		AgentUpdateView.as_view(),
		name='modification-agent'
	),

	url(r'^inscription/agent/$',
		AgentCreateView.as_view(),
		name='inscription-agent'
	),

	url(r'^espace/agent/$',
		TemplateView.as_view(
			template_name="utilisateurs/agents/espace.html"
		),
		name='espace-agent'
	),
	url(r'^espace/agent/offres/(?P<nb_mandat>\d+)$',
		AgentOffreDetailView.as_view(),
		name='agent-offre'
	),

	# Fiche clients

	url(r'^espace/agent/fiche-client/nouveau/$',
		AgentClientCreateView.as_view(),
		name='nouveau-agent-client'
	),

	url(r'^espace/agent/actualites/$',
		AgentActualitesView.as_view(),
		name='actualites'
	),

	url(r'^espace/agent/fiche-client/$',
		AgentClientListView.as_view(),
		name='list-agent-client'
	),
	url(r'^espace/agent/fiche-client/(?P<pk>\d+)/$',
		AgentClientUpdateView.as_view(),
		name='agent-client'
	),
	url(r'^espace/agent/fiche-client/(?P<pk>\d+)/supprimer/$',
		AgentClientDeleteView.as_view(),
		name='supprimer-agent-client'
	),


	# Liste de favoris

	url(r'^espace/agent/liste-favoris/nouveau/$',
		FavorisListeCreateView.as_view(),
		name='nouvelle-liste-favoris'
	),
	url(r'^espace/agent/liste-favoris/(?P<pk>\d+)/$',
		FavorisListeDetailView.as_view(),
		name='liste-favoris'
	),
	url(r'^espace/agent/liste-favoris/(?P<pk>\d+)/supprimer/$',
		FavorisListeDeleteView.as_view(),
		name='supprimer-liste-favoris'
	),
	url(r'^espace/agent/liste-favoris/$',
		FavorisListeListView.as_view(),
		name='list-liste-favoris'
	),
	url(r'^espace/agent/mes-favoris/$',
		MesFavorisListView.as_view(),
		name='agent-mes-favoris'
	),

	# Agent visites

	url(r'^espace/agent/visites/$',
		AgentVisiteListView.as_view(),
		name='list-visites'
	),

	# Agent ventes

	url(r'^espace/agent/ventes/$',
		AgentVenteListView.as_view(),
		name='list-ventes'
	),

	# Agent Mes offres Recus
	url(r'^espace/agent/mes-offres/$',
		AgentOffreListView.as_view(),
		name='list-offres'
	),

	# Admin Bien A Valider
	url(r'^bienavalider/(?P<nb_mandat>\d+)/$',
		BienAValiderDeleteView.as_view(),
		name='delete-bav'
	),
	# Admin Bien A Valider
	url(r'^bien/(?P<nb_mandat>\d+)/$',
		BienDeleteView.as_view(),
		name='delete-b'
	),

	# Admin Agent A Valider
	url(r'^agentavalider/(?P<pk>\d+)/$',
		AgentAValiderDeleteView.as_view(),
		name='delete-aav'
	),


	# Notification

	url(r'^notifications/$', # For ajax
		read_notification,
		name='notification-read'
	),

	url(r'^notifications/(?P<pk>\d+)/read/$',
		read_notification
	),

]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractUser, BaseUserManager

from utilisateurs import TYPES_CLIENTS, TYPES_INSCRIPTIONS, TYPE_QUALITE
from django.db.models import AutoField
from utilisateurs.utils import get_logentry_agentavalider
from geo.models import GeoLocalisable
from biens.utils import get_file_path

class UserManager(BaseUserManager):
	"""
	Define a model manager for User model with no username field.
	"""

	use_in_migrations = True

	def _create_user(self, email, password, **extra_fields):
		"""
		Create and save a User with the given email and password.
		"""
		if not email:
			raise ValueError('The given email must be set')

		email = self.normalize_email(email)
		user = self.model(email=email, **extra_fields)

		user.set_password(password)
		user.save(using=self._db)

		return user

	def create_user(self, email, password=None, **extra_fields):
		"""
		Create and save a regular User with the given email and
		password.
		"""
		extra_fields.setdefault('is_staff', False)
		extra_fields.setdefault('is_superuser', False)

		return self._create_user(email, password, **extra_fields)

	def create_superuser(self, email, password, **extra_fields):
		"""
		Create and save a SuperUser with the given email and
		password.
		"""
		extra_fields.setdefault('is_staff', True)
		extra_fields.setdefault('is_superuser', True)

		if extra_fields.get('is_staff') is not True:
			raise ValueError('Superuser must have is_staff=True.')
		if extra_fields.get('is_superuser') is not True:
			raise ValueError('Superuser must have is_superuser=True.')

		return self._create_user(email, password, **extra_fields)


class User(AbstractUser):
	"""
	Definition d'un utilisateur.
	"""
	username = None
	email = models.EmailField(_('email address'), unique=True)

	USERNAME_FIELD = 'email'
	REQUIRED_FIELDS = []

	objects = UserManager()


class FicheClient(GeoLocalisable):

	civilite = models.CharField(
		u"Civilité",
		max_length=8,
		choices=TYPES_CLIENTS,
	)

	prenom = models.CharField(
		u"Prénom",
		max_length=30
	)

	nom = models.CharField(
		"Nom",
		max_length=150
	)

	societe = models.CharField(
		u"Dénomination sociale",
		max_length=50,
		null=True,
		blank=True,
		help_text=u"Les coordonnées que vous indiquez doivent être celles de la carte professionnelle."
	)

	siren = models.CharField(
		max_length=9,
		null=True,
		blank=True,

	)

	mobile = models.CharField(
		u"Téléphone mobile",
		max_length=15,
		# unique=True,
		error_messages={
			'unique': u"Ce numéro de téléphone est déjà utilisé " \
			"Veuillez contacter le support."
		}
	)

	confirm_code = models.CharField(_('code de confirmation'), max_length=64, null=True, blank=True, unique=True)
	confirmed_email = models.BooleanField(_('email confirmé'), default=False)

	notification = models.BooleanField(default=False,
		help_text=u"J'accepte de recevoir des notifications liées à l’actualité")

	creation = models.DateTimeField(auto_now_add=True)
	modification = models.DateField(auto_now=True)

	def save(self, *args, **kwargs):
		if self.pk is None:
			import uuid
			self.confirm_code = uuid.uuid4()
		super(FicheClient, self).save(*args, **kwargs)

	def get_full_name(self):
		return "%s %s" % (self.prenom.encode('utf-8'), self.nom.encode('utf-8'))

	class Meta:
		abstract = True

	def __unicode__(self):
		# if self.societe and self.siren:
		# 	return  u"Société %s (%s)" % (
		# 		self.societe,
		# 		self.siren
		# 	)

		return u"%s %s %s" % (
			self.civilite.title(),
			self.prenom.title(),
			self.nom.upper()
		)


class Client(FicheClient):
	"""
	Les clients du site: vendeurs ou acheteurs
	"""
	user = models.OneToOneField(User, related_name='client')
	favoris = models.ManyToManyField("biens.Bien", blank=True)

	@property
	def email(self):
		return self.user.email

	def is_active(self):
		return self.user.is_active



class Agent(FicheClient):
	"""
	Les Agents
	"""
	user = models.OneToOneField(User, related_name='agent')
	carte = models.CharField(u"Carte professionnelle N°", max_length=20, unique=False)
	valide = models.BooleanField(default=False)
	type_inscription = models.CharField(u"Je m'inscrit avec", max_length=3, choices=TYPES_INSCRIPTIONS)
	attestation = models.CharField(u"Attestation N°", max_length=50, blank=True, default=u"")
	qualite_colaboration = models.CharField(u"Qualité de la collaboration",  max_length=3, blank=True, choices=TYPE_QUALITE)
	siege_adresse = models.CharField(u"Adresse du Siège", max_length=255)
	siege_code_postal = models.CharField(u"Code postal du Siège", max_length=5)
	siege_ville = models.CharField(u"Ville du Siège", max_length=50)
	legal_representant_prenom = models.CharField(u"Prénom du représentant légal", max_length=30)
	legal_representant_nom = models.CharField(u"Nom du représentant légal", max_length=150)
	other_legal_representant_prenom = models.CharField(u"Prénom de l'autre représentant légal", max_length=30, blank=True, default=u"")
	other_legal_representant_nom = models.CharField(u"Nom de l'autre représentant légal", max_length=150, blank=True, default=u"")
	assurance_nom = models.CharField(u"Nom de votre assurance RCP", max_length=50, unique=False)
	assurance_nb = models.CharField(u"N° de la police d'assurance", max_length=50, unique=False)
	file_id1 = models.FileField(u"Face Recto de la carte d'identité (ou Recto et Verso)", upload_to=get_file_path, null=True, blank=True, default=None)

	@property
	def email(self):
		return self.user.email

	@property
	def favoris(self):
		result = set()
		for liste in self.favorislists.all():
			for bien in liste.favoris.all():
				result.add(bien)
		return result

	def is_carte(self):
		return self.type_inscription == 'crt'
	def is_attestation(self):
		return self.type_inscription == 'att'

	def is_active(self):
		return self.user.is_active
	def get_fields(self):
		return dict([(f.name, getattr(self, f.name))
					for f in self._meta.fields
					if not isinstance(f, AutoField) and not f in self._meta.parents.values()])



class AgentAValider(FicheClient):
	"""
	Les Agents à valider, to be validated by the admin before replace the source Agent (if validated in admin), or deleted otherwise
	"""
	source = models.ForeignKey(Agent, related_name="source_origin", null=True, default=None)
	checked = models.BooleanField(default=False)
	modification_a_valider = models.DateTimeField(auto_now=True)
	email = models.EmailField(blank=True)

	# COPY of All fields from Agent (no choice, otherwise a new Agent is created and as to be deleted)
	user = models.OneToOneField(User, related_name='agent_a_valider', null=True)
	carte = models.CharField(u"Carte professionnelle N°", max_length=20,unique=False)
	valide = models.BooleanField(default=False)
	type_inscription = models.CharField(u"Je m'inscrit avec", max_length=3, choices=TYPES_INSCRIPTIONS)
	attestation = models.CharField(u"Attestation N°", max_length=50, blank=True, default=u"",unique=False)
	qualite_colaboration = models.CharField(u"Qualité de la collaboration",  max_length=3, blank=True, choices=TYPE_QUALITE)
	siege_adresse = models.CharField(u"Adresse du siège", max_length=255)
	siege_code_postal = models.CharField(u"Code postal du siège", max_length=5)
	siege_ville = models.CharField(u"Ville du siège", max_length=50)
	legal_representant_prenom = models.CharField(u"Prénom du représentant légal", max_length=30)
	legal_representant_nom = models.CharField(u"Nom du représentant légal", max_length=150)
	other_legal_representant_prenom = models.CharField(u"Prénom de l'autre représentant légal", max_length=30, blank=True, default=u"")
	other_legal_representant_nom = models.CharField(u"Nom de l'autre représentant légal", max_length=150, blank=True, default=u"")
	assurance_nom = models.CharField(u"Nom de votre assurance RCP", max_length=50, unique=False)
	assurance_nb = models.CharField(u"N° de la police d'assurance", max_length=50, unique=False)
	file_id1 = models.FileField(u"Face Recto de la carte d'identité (ou Recto et Verso)", upload_to=get_file_path, null=True, blank=True, default=None)
	file_id2 = models.FileField(u"Face Verso de la carte d'identité", upload_to=get_file_path, null=True, blank=True, default=None)

	def remove_file_ids(self):
		if self.file_id1:
			self.file_id1.delete()
		if self.file_id2:
			self.file_id2.delete()

	def is_new(self):
		return self.source is None

	def is_active(self):
		return self.user.is_active
	def get_fields(self):
		return dict([(f.name, getattr(self, f.name))
					for f in self._meta.fields
					if not isinstance(f, AutoField) and not f in self._meta.parents.values()])
	def get_history(self):
		return get_logentry_agentavalider(self.id)

	def is_carte(self):
		return self.type_inscription == 'crt'
	def is_attestation(self):
		return self.type_inscription == 'att'

from django.db.models.signals import pre_delete
from django.dispatch import receiver

@receiver(pre_delete, sender=AgentAValider)
def aav_delete_signal_handler(sender, **kwargs):
	instance = kwargs['instance']
	instance.remove_file_ids()


class AgentClient(FicheClient):
	agent = models.ForeignKey(Agent, related_name='clients')
	email = models.EmailField(blank=True, null=True)
	commentaire = models.TextField(blank=True, null=True)


class FavorisListe(models.Model):
	agent = models.ForeignKey(Agent, related_name='favorislists')
	favoris = models.ManyToManyField("biens.Bien", related_name='favorislists', blank=True)

	nom = models.CharField(max_length=64)

	def __unicode__(self):
		return u"%s" % self.nom

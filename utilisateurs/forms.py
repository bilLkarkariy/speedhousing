# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms
from django.urls import reverse
from utilisateurs.models import User, Client, Agent, AgentAValider, FicheClient
from agenda.models import JourOff
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import password_validation
from sh import settings



class NotificationsForm(forms.ModelForm):
	error_messages = {
		'flag_notifications': u"Si vous n'acceptez pas de recevoir les notifications d'informations liées à la vente ou à l'achat de biens vous ne pouvez pas utiliser les différentes fonctionnalités du site de votre parcours utilisateur.",
		'flag_cgv': "Vous devez accepter les conditions générales d’utilisations et de ventes afin de pouvoir vous inscrire.",
	}

	flag_cgv = forms.BooleanField(
		required = False
	)
	flag_notifications = forms.BooleanField(
		required = False
	)
	flag_communication = forms.BooleanField(
		required=False
	)

	def __init__(self, *args, **kwargs):
		super(NotificationsForm, self).__init__(*args, **kwargs)

		tooltip = '''
			<a href="%s" target="_blank">
			<span class="glyphicon glyphicon-info-sign" data-toggle="tooltip"
			title="Cliquez ici pour en savoir plus.">
			</span>
			</a>
		''' % (reverse('legal'))

		self.fields['flag_cgv'].label = _(u'J\'accepte les <a href="%s" target="_blank">conditions générales d’utilisations et de ventes</a> du site www.speedhousing.fr') % reverse('legal')

		self.fields['flag_notifications'].label = "%s %s" % (
			_(u"J'accepte de recevoir des notifications liées à la vente et l'achat de biens"),
			tooltip
		)
		self.fields['flag_communication'].label = "%s %s" % (
			_(u"J'accepte de recevoir des notifications liées à l’actualité."),
			tooltip
		)

	def clean_flag_cgv(self):
		value = self.cleaned_data.get("flag_cgv")
		if not value:
			raise forms.ValidationError(
				NotificationsForm.error_messages['flag_cgv'],
				code='required'
			)
		return value

	def clean_flag_notifications(self):
		value = self.cleaned_data.get("flag_notifications")
		if not value:
			raise forms.ValidationError(
				NotificationsForm.error_messages['flag_notifications'],
				code='required'
			)
		return value

	def save(self, commit=True):
		super(NotificationsForm, self).save(commit=False)

		self.instance.notification = self.cleaned_data.get("flag_communication")
		if commit:
			self.instance.save()

		return self.instance


class NotificationsUpdateForm(forms.ModelForm):
	error_messages = {
		'flag_notifications': u"Si vous n'acceptez plus de recevoir les notifications d'informations liées à la vente ou à l'achat de biens vous ne pouvez plus utiliser les différentes fonctionnalités du site et votre compte est suspendu.",
	}

	flag_notifications = forms.BooleanField(
		help_text = error_messages['flag_notifications'],
		initial = True,
		required = False
	)
	flag_communication = forms.BooleanField(
		required=False
	)

	def __init__(self, *args, **kwargs):
		super(NotificationsUpdateForm, self).__init__(*args, **kwargs)

		tooltip = '''
			<a href="%s" target="_blank">
			<span class="glyphicon glyphicon-info-sign" data-toggle="tooltip"
			title="Cliquez ici pour en savoir plus.">
			</span>
			</a>
		''' % (reverse('legal'))

		self.fields['flag_notifications'].label = "%s %s" % (
			_(u"J'accepte de recevoir des notifications liées à la vente et l'achat de biens"),
			tooltip
		)
		self.fields['flag_communication'].label = "%s %s" % (
			_(u"J'accepte de recevoir des notifications liées à l’actualité."),
			tooltip
		)

	def get_initial_for_field(self, field, field_name):
		if field_name == 'flag_communication':
			field.initial = self.instance.notification
			return field.initial
		return super(NotificationsUpdateForm, self).get_initial_for_field(field, field_name)

	def save(self, commit=True):
		super(NotificationsUpdateForm, self).save(commit=False)

		self.instance.notification = self.cleaned_data.get("flag_communication")

		if not self.cleaned_data.get("flag_notifications"):
			self.instance.user.is_active = False
			self.instance.user.save()

		if commit:
			self.instance.save()

		return self.instance


class FicheClientForm(forms.ModelForm):
	class Meta:
		model = FicheClient
		fields = [
			'civilite', 'prenom', 'nom',
			'societe', 'siren', 'mobile',
			'adresse', 'code_postal', 'ville'
		]


class JourOffForm(forms.ModelForm):
	debut = forms.DateField(input_formats=settings.DATE_INPUT_FORMATS)
	fin = forms.DateField(input_formats=settings.DATE_INPUT_FORMATS)

	class Meta:
		model = JourOff
		fields = [
			'debut',
			'fin'
		]

class UserForm(forms.Form):

	error_messages = {
		'password_mismatch':
		_("The two password fields didn't match."),

		'already_exists': "Un compte existe déjà pour cette adresse.",

		'junk_mail': u"Voulez-vous bien utiliser votre "
		u"réelle adresse mail !",
	}

	email = forms.EmailField(label=_("Email"))

	password1 = forms.CharField(
		label=_("Password"),
		widget=forms.PasswordInput,
		strip=False,
		help_text=password_validation.password_validators_help_text_html()
	)
	password2 = forms.CharField(
		label=_("Password confirmation"),
		widget=forms.PasswordInput
	)

	def clean_password2(self):
		password1 = self.cleaned_data.get("password1")
		password2 = self.cleaned_data.get("password2")
		if password1 and password2 and password1 != password2:
			raise forms.ValidationError(
				self.error_messages['password_mismatch'],
				code='password_mismatch',
			)
		return password2

	def _post_clean(self):
		super(UserForm, self)._post_clean()
		# Validate the password after self.instance is updated with form data
		# by super().
		password = self.cleaned_data.get('password2')
		if password:
			try:
				password_validation.validate_password(
					password,
					self.instance
				)
			except forms.ValidationError as error:
				self.add_error('password2', error)

	def clean_email(self):
		email = self.cleaned_data.get("email", None)
		if "yopmail" in email:
			raise forms.ValidationError(
				self.error_messages['junk_mail'],
				code='junk_mail'
			)
		try:
			user = User.objects.get(email = email)
			raise forms.ValidationError(
				self.error_messages['already_exists'],
				code='already_exists'
			)
		except User.DoesNotExist:
			return email

	def save_user(self, commit=True):
		user = User()
		user.email = self.cleaned_data.get('email')
		user.set_password(self.cleaned_data.get('password1'))
		if commit:
			user.save()
		return user


class ClientForm(NotificationsForm):

	class Meta:
		model = Client
		fields = [
			'civilite', 'prenom', 'nom',
			'societe', 'siren',
			'email', 'password1', 'password2',
			'mobile',
			'adresse', 'code_postal', 'ville',
		]
		empty_label={'civilite': 'civil'}

	error_messages = {
		'password_mismatch':
		_("The two password fields didn't match."),

		'already_exists': "Un compte existe déjà pour cette adresse.",

		'junk_mail': u"Voulez-vous bien utiliser votre "
		u"réelle adresse mail !",
	}

	email = forms.EmailField(label=_("Email"))

	password1 = forms.CharField(
		label=_("Password"),
		widget=forms.PasswordInput,
		strip=False,
		help_text=password_validation.password_validators_help_text_html()
	)
	password2 = forms.CharField(
		label=_("Password confirmation"),
		widget=forms.PasswordInput
	)

	def clean_password2(self):
		password1 = self.cleaned_data.get("password1")
		password2 = self.cleaned_data.get("password2")
		if password1 and password2 and password1 != password2:
			raise forms.ValidationError(
				self.error_messages['password_mismatch'],
				code='password_mismatch',
			)
		return password2

	def _post_clean(self):
		super(ClientForm, self)._post_clean()
		# Validate the password after self.instance is updated with form data
		# by super().
		password = self.cleaned_data.get('password2')
		if password:
			try:
				password_validation.validate_password(
					password,
					self.instance
				)
			except forms.ValidationError as error:
				self.add_error('password2', error)

	def clean_email(self):
		email = self.cleaned_data.get("email", None)
		if "yopmail" in email:
			raise forms.ValidationError(
				self.error_messages['junk_mail'],
				code='junk_mail'
			)
		try:
			user = User.objects.get(email = email)
			raise forms.ValidationError(
				self.error_messages['already_exists'],
				code='already_exists'
			)
		except User.DoesNotExist:
			return email

	def save(self, commit=True):
		user = User()
		user.is_active = False
		user.first_name = self.cleaned_data.get('prenom')
		user.last_name = self.cleaned_data.get('nom')
		user.email = self.cleaned_data.get('email')
		user.set_password(self.cleaned_data.get('password1'))
		user.save()

		client = super(ClientForm, self).save(commit=False)
		client.user = user

		if commit:
			client.save()

		return client

class ClientUpdateForm(NotificationsUpdateForm):

	class Meta:
		model = Client
		fields = [
			'civilite', 'prenom', 'nom',
			'mobile',
			'adresse', 'code_postal', 'ville',
			'societe', 'siren'
		]

	def save(self, commit=True):
		client = super(ClientUpdateForm, self).save(commit=False)
		client.user = self.instance.user
		if commit:
			client.save()

		return client

	def clean(self):
		cleaned_data = super(ClientUpdateForm, self).clean()
		nom = cleaned_data.get('nom').upper()
		prenom = cleaned_data.get('prenom').title()



class AgentForm(NotificationsForm):
	class Meta:
		model = AgentAValider
		fields = [
			'civilite', 'prenom', 'nom', 'email',
			'mobile', 'adresse', 'code_postal', 'ville',
			'password1', 'password2', 'type_inscription',
			'attestation', 'qualite_colaboration',
			'carte', 'societe', 'siren',
			'legal_representant_nom', 'legal_representant_prenom',
			'other_legal_representative',
			'other_legal_representant_prenom', 'other_legal_representant_nom',
			'siege_adresse', 'siege_code_postal', 'siege_ville',
			'assurance_nom', 'assurance_nb', 'file_id1', 'file_id2'
		]

		widgets = {
			'siege_code_postal': forms.TextInput(attrs={'placeholder': 'Code postal'}),
			'siege_ville': forms.TextInput(attrs={'placeholder': 'Ville'}),
			'assurance_nom': forms.TextInput(attrs={'placeholder': 'Nom de votre assurance RCP'}),
			'siege_adresse': forms.TextInput(attrs={'placeholder': 'Adresse du siege'}),
			'adresse': forms.TextInput(attrs={'placeholder': 'Adresse (de votre lieu de travail)'}),
                  }

	other_legal_representative = forms.BooleanField(required=False, label=u"Y'a-t-il un autre responsable légal ?")

	error_messages = {
		'password_mismatch':
		_("The two password fields didn't match."),

		'already_exists': "Un compte existe déjà pour cette adresse.",

		'junk_mail': u"Voulez-vous bien utiliser votre "
		u"réelle adresse mail !"
	}

	email = forms.EmailField(label=_("Email"))

	password1 = forms.CharField(
		label=_("Password"),
		widget=forms.PasswordInput,
		strip=False,
		help_text=password_validation.password_validators_help_text_html()
	)
	password2 = forms.CharField(
		label=_("Password confirmation"),
		widget=forms.PasswordInput
	)

	def __init__(self, *args, **kwargs):
		super(AgentForm, self).__init__(*args, **kwargs)
		self.fields['file_id1'].required = True
		self.fields['file_id1'].error_messages['required'] = "La carte d'identité est nécessaire à l'inscription."

	def clean_password2(self):
		password1 = self.cleaned_data.get("password1")
		password2 = self.cleaned_data.get("password2")
		if password1 and password2 and password1 != password2:
			raise forms.ValidationError(
				self.error_messages['password_mismatch'],
				code='password_mismatch',
			)
		return password2

	def _post_clean(self):
		super(AgentForm, self)._post_clean()
		# Validate the password after self.instance is updated with form data
		# by super().
		password = self.cleaned_data.get('password2')
		if password:
			try:
				password_validation.validate_password(
					password,
					self.instance
				)
			except forms.ValidationError as error:
				self.add_error('password2', error)

	def clean_email(self):
		email = self.cleaned_data.get("email", None)
		if "yopmail" in email:
			raise forms.ValidationError(
				self.error_messages['junk_mail'],
				code='junk_mail'
			)
		try:
			user = User.objects.get(email = email)
			raise forms.ValidationError(
				self.error_messages['already_exists'],
				code='already_exists'
			)
		except User.DoesNotExist:
			return email

	def save(self, commit=True):

		user = User()
		user.is_active = False
		user.first_name = self.cleaned_data.get('prenom')
		user.last_name = self.cleaned_data.get('nom')
		user.email = self.cleaned_data.get('email')
		user.set_password(self.cleaned_data.get('password1'))
		user.save()

		agent = super(AgentForm, self).save(commit=False)
		agent.user = user

		if commit:
			agent.save()

		return agent

	def clean(self):
		cleaned_data = super(AgentForm, self).clean()

		nom = cleaned_data.get('nom').upper()
		prenom = cleaned_data.get('prenom').title()
		societe = cleaned_data.get('societe', None)
		siren = cleaned_data.get('siren', None)
		carte = cleaned_data.get('carte').replace(' ', '')

		if societe is None or siren is None:
			raise forms.ValidationError(u"Veuillez saisir"\
				u" votre dénomisation sociale et votre siren.")



class AgentUpdateForm(NotificationsUpdateForm):
	class Meta:
		model = AgentAValider
		fields = [
			'civilite', 'prenom', 'nom', 'email',
			'mobile', 'adresse', 'code_postal', 'ville',
			'type_inscription',
			'attestation', 'qualite_colaboration',
			'carte', 'societe', 'siren', 'legal_representant_nom',
			'legal_representant_prenom',
			'siege_adresse', 'siege_code_postal', 'siege_ville',
			'assurance_nom', 'assurance_nb'
		]

	def save(self, commit=True):
		agent = super(AgentUpdateForm, self).save(commit=False)
		agent.user = self.instance.user
		agent.checked = False
		if commit:
			agent.save()

		return agent

	def clean(self):
		cleaned_data = super(AgentUpdateForm, self).clean()

		nom = cleaned_data.get('nom').upper()
		prenom = cleaned_data.get('prenom').title()
		societe = cleaned_data.get('societe', None)
		siren = cleaned_data.get('siren', None)
		carte = cleaned_data.get('carte').replace(' ', '')

		if societe is None or siren is None:
			raise forms.ValidationError(u"Veuillez saisir"\
				u" votre dénomisation sociale et votre siren.")

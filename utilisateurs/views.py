# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import logging

logger = logging.getLogger(__name__)

from django.shortcuts import render, redirect, get_object_or_404
from django.http import JsonResponse, HttpResponse
from django.views.generic import RedirectView, TemplateView
from django.views.generic.edit import (
	FormView, CreateView, UpdateView, DeleteView
)
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView

from django.conf import settings

from utilisateurs.forms import ClientForm, ClientUpdateForm, UserForm, AgentForm, JourOffForm, AgentUpdateForm
from django.forms import modelform_factory

from django.urls import reverse_lazy, reverse
import os

from django.utils import timezone

from django.contrib import messages

from django.contrib.auth.mixins import (
	LoginRequiredMixin, UserPassesTestMixin
)
from utilisateurs.mixins import AgentTestMixin, UserIsClientMixin, FilterClientBiensMixin, FilterClientBienAValiderMixin
from django.contrib.auth import authenticate, login

from easy_pdf.rendering import render_to_content_file

from utilisateurs.models import Agent, AgentClient, FavorisListe, AgentAValider, Client
from biens.models import Bien, BienAValider, Document, Offre, Photo, Vente, Mandat, Notaire
from agenda.models import JourOff, RDV, Disponibilite
from biens import TYPES_DOCUMENTS, TYPES_BIENS

from biens.views import BienCreateView, PhotoCreateView, DisponibiliteCreateView,BienRelatedDetailView
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.mixins import PermissionRequiredMixin
from biens.utils import add_logentry_bienavalider
from django.db.models import Q
import StringIO
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
from utilisateurs.utils import add_logentry_agentavalider, notify, notify_email, send_alert_notification
from notifications.models import Notification

class UserIsAgentMixin(LoginRequiredMixin, UserPassesTestMixin):
	def test_func(self):
		try:
			self.agent = self.request.user.agent
			return True
		except Agent.DoesNotExist:
			return False


class UserIsAgentOrAgentAValiderMixin(LoginRequiredMixin, UserPassesTestMixin):
	def test_func(self):
		try:
			if hasattr(self.request.user, "agent_a_valider") and self.request.user.agent_a_valider:
				self.agent = self.request.user.agent_a_valider
			else:
				self.agent = self.request.user.agent
			return True
		except Agent.DoesNotExist:
			return False

from sh.settings import ELEMENT_BY_PAGE

def read_notification(request, pk=None):
	Notification.objects.filter(pk=pk, recipient=request.user).mark_all_as_read()
	return HttpResponse('')


class EmailConfirmView(RedirectView):
	pattern_name = "login"

	def get_redirect_url(self, *args, **kwargs):
		if type(self.fiche) == AgentAValider:
			return reverse("home")
		return super(EmailConfirmView, self).get_redirect_url(*args, **kwargs)

	def get(self, request, *args, **kwargs):
		self.token = kwargs.pop('token')

		fiche = Client.objects.filter(confirm_code=self.token).first()
		fiche = fiche if fiche is not None else AgentAValider.objects.filter(confirm_code=self.token).first()
		self.fiche = fiche

		if fiche:
			if fiche.confirmed_email == True:
				messages.warning(request, "Votre email a déjà été confirmé, vous pouvez vous connecter.")
			else:
				fiche.confirmed_email = True
				fiche.confirm_code = None
				fiche.save()

				fiche.user.is_active = True
				fiche.user.save()

				if type(fiche) == AgentAValider:
					messages.info(request, "Votre email a bien été confirmé. Votre compte est désormais en attente de validation par notre service client.")
				else:
					messages.info(request, "Votre email a bien été confirmé, vous pouvez vous connecter.")
		else:
			messages.error(request, "Utilisateur inexistant")

		return super(EmailConfirmView, self).get(request, *args, **kwargs)


class ClientCreateDisabledView(TemplateView):
	template_name = 'utilisateurs/client_form_disabled.html'


class ClientCreateView(FormView):
	template_name = 'utilisateurs/client_form.html'
	form_class = ClientForm

	def get_success_url(self):
		return self.request.GET.get('next', reverse_lazy('home'))

	def form_valid(self, form):
		client = form.save()
		user = client.user

		notify(user, u'Bienvenue %s sur la plateforme SPEEDHOUSING' % (unicode(user)))
		notify_email(user.email, u"Inscription : réception pour validation compte", u"email/confirmation_inscription.html", {
			u"fullname": user.get_full_name(),
			u"url_action": self.request.build_absolute_uri(
				reverse('utilisateurs:confirmation-email', args=[client.confirm_code]).replace('http://', 'https://')

			)
		})

		messages.info(self.request, "Votre compte à été créé avec succès. Vous allez recevoir un email de confirmation pour l'activer.")

		return super(ClientCreateView, self).form_valid(form)


class ClientUpdateView(UserIsClientMixin, UpdateView):
	template_name = 'utilisateurs/client_form.html'
	form_class = ClientUpdateForm
	success_url = reverse_lazy('home')
	model = Client

	def get_object(self, queryset=None):
		return self.client

	def form_valid(self, form):
		# To get changes (also for history)
		form_changed_data = form.changed_data

		if 'flag_notifications' in form_changed_data:
			messages.warning(self.request, "Votre compte a bien été suspendu.")

		if len(form_changed_data) == 0:
			messages.info(self.request, "Aucune modification n'a été faite.")
			return redirect('home')

		messages.info(self.request, "Votre compte a été modifié avec succès.")

		return super(ClientUpdateView, self).form_valid(form)

class AgentCreateView(FormView):
	template_name = 'utilisateurs/agent_form.html'
	form_class = AgentForm
	success_url = reverse_lazy('home')

	def form_valid(self, form):
		agent = form.save()
		user = agent.user

		notify(user, u'Bienvenue %s sur la plateforme SPEEDHOUSING' % (unicode(user)))
		notify_email(user.email, u"Inscription : réception pour validation compte", u"email/pro/confirmation_inscription.html", {
			u"fullname": user.get_full_name(),
			u"url_action": self.request.build_absolute_uri(
				reverse('utilisateurs:confirmation-email', args=[agent.confirm_code]).replace('http://', 'https://')

			)
		})

		messages.info(self.request, "Votre compte à été créé avec succés, il est maintenant en attente de validation. Vous allez recevoir un email de confirmation pour l'activer.")

		return super(AgentCreateView, self).form_valid(form)

	def post(self, *args, **kwargs):
		res = super(AgentCreateView, self).post(*args, **kwargs)
		return res


class AgentUpdateView(UserIsAgentOrAgentAValiderMixin, UpdateView):
	template_name = 'utilisateurs/agent_form.html'
	form_class = AgentUpdateForm
	success_url = reverse_lazy('home')
	model = AgentAValider

	def get_object(self, queryset=None):
		# Check if already a modification waiting
		if isinstance(self.agent, AgentAValider):
			return AgentAValider.objects.get(pk=self.agent.pk)
		agent_a_valider = AgentAValider.objects.filter(source=self.agent)
		if len(agent_a_valider) == 1:
			# Yes so get this one
			return agent_a_valider.get()
		# Or get a Bien (see get_queryset)
		return Agent.objects.get(pk=self.agent.pk)


	def form_valid(self, form):
		if not isinstance(self.agent, AgentAValider):
			# Copie all the value of the Bien (before update)
			old_agent = form.instance
			all_fields = old_agent.get_fields()
			all_fields["source"] = old_agent
			all_fields["email"] = old_agent.user.email
			# Create a BienAValider with the old value
			form.instance = AgentAValider.objects.create(**all_fields)
		form_changed_data = form.changed_data

		if self.agent.user.email == form.instance.email and "email" in form_changed_data:
			form_changed_data.remove("email")

		if 'flag_notifications' in form_changed_data:
			messages.warning(self.request, "Votre compte a bien été suspendu.")

		if len(form_changed_data) == 0:
			messages.info(self.request, "Aucune modification n'a été faite.")
			return redirect('home')

		add_logentry_agentavalider(self.request.user.id, form.instance, form.changed_data)

		messages.info(self.request, "Votre compte a été modifié avec succès. Les modifications sont maintenant en attente de validation.")

		return super(AgentUpdateView, self).form_valid(form)

	def get_context_data(self, *args, **kwargs):
		context = super(AgentUpdateView,
			self).get_context_data(*args, **kwargs)
		if isinstance(context['form'].instance, Agent):
			initial = context['form'].initial
			initial["email"] = self.agent.user.email
			context['form'] = AgentUpdateForm(instance=self.agent, initial=initial)
		return context


@method_decorator(csrf_exempt, name='dispatch')
class AgentAValiderDeleteView(DeleteView):
	permission_required = 'utilisateurs.delete_agentavalider'
	model = AgentAValider
	success_url = reverse_lazy('admin:utilisateurs_agentavalider_changelist')


	def reset_agent_related(self, agent):
		"""
		Reset the dependencie of User in an Agent or an AgentAValider to None

		:param Agent agent: The agent
		"""
		agent.user = None
		agent.save()

	def remove_agent_related(self, user):
		"""
		Remove the user link to the previous Agent

		:param User user: The user to remove
		"""
		user.remove()

	def get_fields_for_agent(self, aav):

		"""
		Retieve the AgentAValider fields and remove the fields which are not in the model Agent

		:param AgentAValider aav: The agent a valider
		:rtype: [{'': Object}]
		:return: The list of fields
		"""
		agent_fields = aav.get_fields()
		agent_fields.pop('file_id1', None)
		agent_fields.pop('file_id2', None)
		agent_fields.pop('modification_a_valider', None)
		agent_fields.pop('checked', None)
		agent_fields.pop('source', None)
		agent_fields.pop('email', None)
		return agent_fields

	def create_agent(self, aav):
		"""
		Create a Agent from a AgentAValider

		:param AgentAValider aav: The agent a valider
		"""
		agent_fields = self.get_fields_for_agent(aav)
		agent_fields["valide"] = True
		if aav.email != agent_fields["user"].email:
			agent_fields["user"].email = aav.email
			agent_fields["user"].save()
		return Agent.objects.create(**agent_fields)

	def update_agent(self, aav):

		"""
		Update a Agent from a AgentAValider

		:param AgentAValider aav: The agent a valider
		"""
		agent_fields = self.get_fields_for_agent(aav)
		agent_fields["valide"] = True
		Agent.objects.filter(pk=aav.source.pk).update(**agent_fields)
		if aav.email != aav.source.user.email:
			aav.source.user.email = aav.email
			aav.source.user.save()
		return Agent.objects.get(pk=aav.source.pk)

	def post(self, *args, **kwargs):
		"""
		Override the post request and don't call the post() of the object parent
		"""
		# Extract Data
		aav = self.get_object()
		action = self.request.GET.get("action", None)
		reason = self.request.GET.get("raison", "")
		if action == "accepter":
			if aav.source is None:
				# New Agent
				# Create a agent from a agent_a_valider
				agent = self.create_agent(aav)
				# Set the AgentAValider user to None (to don't delete it)
				self.reset_agent_related(aav)
				# Delete the AgentAValider (full merged)
				aav.remove_file_ids()
				aav.delete()
				messages.info(self.request, "L'Agent à été créé, celui-ci en est notifié")
				notify(agent.user, u'Votre compte a été validé')
				notify_email(agent.user.email, u"Inscription : confirmation de votre inscription", u"email/pro/validation_inscription.html", {
					u"fullname": agent.get_full_name(),
					u"url_action": self.request.build_absolute_uri(
						reverse('login')
					).replace('http://', 'https://'),
				})

			else:
				# Existing Agent
				# Update a agent_a_valider into a agent
				agent = self.update_agent(aav)
				# Set the AgentAValider user to None (to don't delete it)
				self.reset_agent_related(aav)
				# Delete the AgentAValider (full merged)
				aav.remove_file_ids()
				aav.delete()
				messages.info(self.request, "L'Agent à été mis à jour, celui-ci en est notifié")
				notify(agent.user, u'Votre compte a été mis à jour')
		elif action == "refuser":
			if aav.source is None:
				# New Agent
				aav.checked = True
				aav.save()
				messages.info(self.request, "L'Agent n'a pas été validé, celui-ci en est notifié (il doit le changer)")
				notify(aav.user, u'Votre compte n\'a pas été validé')
			else:
				# Existing Agent
				# Delete the AgentAValider (refused)
				aav.remove_file_ids()
				aav.delete()
				messages.info(self.request, "La mise à jour de l'Agent est refusé, celui-ci en est notifié")
				notify(aav.user, u'La mise à jour de votre compte a été refusé')
		return redirect(self.success_url)



class AgentClientCreateView(AgentTestMixin, CreateView):

	model = AgentClient
	fields = ['civilite', 'prenom', 'nom', 'societe',
		'siren', 'mobile', 'email', #'notification',
		'adresse', 'code_postal', 'ville',
		'commentaire']

	def form_valid(self, form):
		form.instance.agent = self.agent
		return super(AgentClientCreateView, self).form_valid(form)

	def get_success_url(self):
		messages.info(self.request, "La fiche client a bien été ajoutée.")
		next_page = self.request.GET.get('next', None)
		if next_page:
			return next_page
		return self.request.META.get('HTTP_REFERER')


class AgentClientListView(AgentTestMixin, ListView):
	model = AgentClient
	template_name = 'utilisateurs/agents/fiches_clients.html'
	paginate_by = ELEMENT_BY_PAGE

	def get_queryset(self):
		return super(AgentClientListView, self).get_queryset().filter(agent=self.agent)

	def get(self, request, *args, **kwargs):
		if request.is_ajax():
			q = request.GET.get('q', None)
			if q is None:
				return JsonResponse([], safe=False)

			acs = self.get_queryset().filter(
				Q(nom__icontains=q) | \
				Q(prenom__icontains=q) | \
				Q(societe__icontains=q)
			)

			return JsonResponse(
				[{
					'id': a.pk,
					'name': str(a)
				} for a in acs[:5]],
				safe=False
			)

		return super(AgentClientListView, self).get(request, *args,
			**kwargs)


class AgentClientUpdateView(AgentTestMixin, UpdateView):
	model = AgentClient
	fields = AgentClientCreateView.fields

	def get_success_url(self):
		next_page = self.request.GET.get('next', None)
		if next_page:
			return next_page
		return self.request.META.get('HTTP_REFERER')

	def get_queryset(self):
		return super(AgentClientUpdateView, self).get_queryset().filter(agent=self.agent)

class AgentClientDeleteView(AgentTestMixin, DeleteView):
	model = AgentClient

	def get_success_url(self):
		messages.error(self.request, "La fiche client a bien été supprimée.")
		next_page = self.request.GET.get('next', None)
		if next_page:
			return next_page
		return self.request.META.get('HTTP_REFERER')

	def get_queryset(self):
		return super(AgentClientDeleteView, self).get_queryset().filter(agent=self.agent)




class FavorisListeCreateView(AgentTestMixin, CreateView):
	model = FavorisListe
	fields = ['nom']

	def form_valid(self, form):
		form.instance.agent = self.agent
		return super(FavorisListeCreateView, self).form_valid(form)

	def get_success_url(self):
		messages.info(self.request, "La liste de favoris a bien été ajoutée.")
		next_page = self.request.GET.get('next', None)
		if next_page:
			return next_page
		return self.request.META.get('HTTP_REFERER')


class FavorisListeDeleteView(AgentTestMixin, DeleteView):
	model = FavorisListe

	def get_success_url(self):
		messages.error(self.request, "La liste de favoris a bien été supprimée.")
		next_page = self.request.GET.get('next', None)
		if next_page:
			return next_page
		return self.request.META.get('HTTP_REFERER')

	def get_queryset(self):
		return super(FavorisListeDeleteView, self).get_queryset().filter(agent=self.agent)


class FavorisListeListView(AgentTestMixin, ListView):
	model = FavorisListe
	template_name = 'utilisateurs/agents/liste_favoris.html'
	paginate_by = ELEMENT_BY_PAGE

	def get_queryset(self):
		return super(FavorisListeListView, self).get_queryset().filter(agent=self.request.user.agent)


class FavorisListeDetailView(AgentTestMixin, DetailView):
	model = FavorisListe
	template_name = 'biens/bien_list.html'


	def get_context_data(self, **kwargs):
		context = super(FavorisListeDetailView, self).get_context_data(**kwargs)
		context['object_list'] = self.get_biens()
		context['TYPES_BIENS'] = dict(TYPES_BIENS)
		return context

	def get_biens(self):
		queryset = Bien.objects.exclude(valide=None) \
			.exclude(offres__acceptee__isnull=False) \
			.exclude(deleted__isnull=False)
		localisation = Q()
		for loc in self.request.GET.getlist('localisation'):
			if loc:
				localisation = localisation | Q(ville__icontains=loc) | Q(code_postal=loc)
		queryset = queryset.filter(localisation)


		type = Q()
		for t in self.request.GET.getlist('type'):
			if t:
				type = type | Q(type=t)
		queryset = queryset.filter(type)

		min_prix = self.request.GET.get('min_prix', None)
		if min_prix: queryset = queryset.filter(prix__gte=min_prix)
		max_prix = self.request.GET.get('max_prix', None)
		if max_prix: queryset = queryset.filter(prix__lte=max_prix)


		min_surface = self.request.GET.get('min_surface', None)
		if min_surface:
			queryset = queryset.filter(surface__gte=min_surface)

		max_surface = self.request.GET.get('max_surface', None)
		if max_surface:
			queryset = queryset.filter(surface__lte=max_surface)

		return queryset.order_by('-modification', '-creation')

	def get_queryset(self):
		return super(FavorisListeDetailView, self).get_queryset().filter(agent=self.agent)

	def post(self, request, *args, **kwargs):
		supprimer = request.POST.get('supprimer',
			request.GET.get('supprimer', None)
		)
		ajouter = request.POST.get('ajouter',
			request.GET.get('ajouter', None)
		)
		next_page = request.POST.get('next',
			request.GET.get('next',
				reverse('utilisateurs:liste-favoris', kwargs={'pk':self.get_object().pk})
			)
		)
		referer_page = self.request.META.get('HTTP_REFERER')
		liste = self.get_object()

		if supprimer:
			bien = get_object_or_404(Bien, nb_mandat=supprimer)
			liste.favoris.remove(bien)
			messages.error(request, "%s a bien été retiré(e) de la "\
			"liste de favoris %s." % (bien, liste))

			return redirect(referer_page)

		elif ajouter:
			bien = get_object_or_404(Bien, nb_mandat=ajouter)
			liste.favoris.add(bien)
			messages.info(request, "%s a bien été ajouté(e) à la liste %s." % (bien, liste))

			return redirect(next_page)

		else:
			return self.get(self, request, *args, **kwargs)




class AgentVisiteListView(AgentTestMixin, ListView):
	model = RDV
	template_name = 'utilisateurs/agents/visites.html'
	paginate_by = ELEMENT_BY_PAGE


	def get_queryset(self):
		# Get all the Agent RDV from the closest to now
		return RDV.all_objects.filter(agent=self.agent).order_by('crenau')

	def post(self, request, *args, **kwargs):
		annuler = request.POST.get('annuler',
			request.GET.get('annuler', None)
		)
		confirmer = request.POST.get('confirmer',
			request.GET.get('confirmer', None)
		)
		prix = request.POST.get('prix',
			request.GET.get('prix', None)
		)
		rdv_pk = request.POST.get('rdv_pk',
			request.GET.get('rdv_pk', None)
		)

		next_page = request.POST.get('next',
			request.GET.get('next',
				reverse('utilisateurs:list-visites')
			)
		)

		if annuler:
			rdv = get_object_or_404(RDV, agent=self.agent, pk=annuler)
		elif confirmer:
			rdv = get_object_or_404(RDV, agent=self.agent, pk=confirmer)
		elif rdv_pk:
			rdv = get_object_or_404(RDV, agent=self.agent, pk=rdv_pk)

		if rdv.acheteur is not None:
			client_user = rdv.acheteur.user
			client_email = rdv.acheteur.email
			client_full_name = rdv.acheteur.user.get_full_name()
		else:
			client_user = None
			client_email = rdv.agent_client.email
			client_full_name = rdv.agent_client.get_full_name()

		if annuler:
			rdv = get_object_or_404(RDV, agent=self.agent, pk=annuler)
			rdv.delete()

			if client_user:
				notify(client_user, u'Votre rendez-vous a été annulé')
			notify_email(client_email, u"Votre rendez-vous a été annulé", u"email/acheteur/visite_annule.html", {
				u"fullname": client_full_name,
				u"reference": rdv.bien.nb_mandat,
				u"titre_annonce": rdv.bien,
				u"date": rdv.crenau
			})
			pass
		elif confirmer:
			rdv = get_object_or_404(RDV, agent=self.agent, pk=confirmer)
			rdv.visite = timezone.now()
			rdv.save()

			messages.info(request, "Vous venez de confirmer votre visite "\
				": %s. Vous pouvez maintenant effectuer une offre à %s."\
				"" % (rdv.bien, rdv.vendeur))

			return redirect(next_page)
		elif prix and rdv_pk:
			rdv = get_object_or_404(RDV, agent=self.agent, pk=rdv_pk)

			has_offer = rdv.has_offer()
			if rdv.visite and not has_offer and not rdv.is_sold():
				offre = Offre()
				offre.prix = prix
				offre.bien = rdv.bien
				offre.vendeur = rdv.vendeur
				offre.agent = self.agent
				if rdv.acheteur:
					offre.acheteur = rdv.acheteur
				elif rdv.agent_client:
					offre.agent_client = rdv.agent_client
				else:
					raise Exception("Whoopsey !!")
				offre.save()
				messages.info(request, "Votre offre a bien été envoyée.")
				notify(rdv.vendeur.user, u'Nouvelle offre')
				notify_email(rdv.vendeur.email, u"Nouvelle offre", u"email/vendeur/nouvelle_offre.html", {
					u"fullname": rdv.vendeur.get_full_name(),
					u"fullname_agent": self.agent.get_full_name(),
					u"fullname_acheteur": client_full_name,
					u"montant_offre": prix,
					u"titre_annonce": rdv.bien,
					u"reference": rdv.bien.nb_mandat
				})
			elif has_offer:
				messages.error(request, "Vous avez déjà une offre en attente")
			elif rdv.is_sold():
				messages.error(request, "Le bien a déjà été vendu")
			else:
				messages.error(request, "Vous devez confirmer votre visite avant de "\
					"pouvoir faire des offres")
			return redirect(next_page)
		else:
			# je sais pas encore
			return redirect(next_page)


class AgentVenteListView(AgentTestMixin, ListView):
	model = Offre
	template_name = 'utilisateurs/agents/ventes.html'
	paginate_by = ELEMENT_BY_PAGE

	def get_queryset(self):
		countered_offers = Offre.objects.filter(agent=self.agent).exclude(offre=None).values_list('offre_id', flat=True)
		qs = super(AgentVenteListView, self).get_queryset().exclude(id__in=countered_offers)

		return qs.filter(
			agent=self.agent,
			acceptee__isnull=False
		).order_by('-created')

class AgentOffreListView(AgentTestMixin, ListView):
	model = Bien
	template_name = 'utilisateurs/agents/offres.html'
	paginate_by = ELEMENT_BY_PAGE

	def get_queryset(self):
		return super(AgentOffreListView, self).get_queryset().filter(offres__agent=self.agent, offres__acceptee=None, deleted=None).distinct().order_by('-creation')


class AgentOffreDetailView(AgentTestMixin, DetailView):
	template_name = 'utilisateurs/agents/offres.html'
	OffreForm = modelform_factory(Offre, fields=('prix',))
	slug_field = 'nb_mandat'
	slug_url_kwarg = 'nb_mandat'
	model = Bien

	def get_context_data(self, *args, **kwargs):
		bien = self.get_object()
		context = super(AgentOffreDetailView,
			self).get_context_data(*args, **kwargs)
		context['historique'] = Offre.objects.filter(
			Q(bien=bien),
			Q(refusee__isnull=False) | Q(acceptee__isnull=False)
		)
		context['offres'] = Offre.objects.filter(
			bien=bien, initiater=False, refusee=None, acceptee=None
		)
		context['form'] = AgentOffreDetailView.OffreForm()
		return context

	def post(self, request, *args, **kwargs):
		bien = self.get_object()
		accepter = request.GET.get('accepter', None)
		refuser = request.GET.get('refuser', None)
		contre = request.GET.get('contre-offre', None)

		if accepter:
			offre = get_object_or_404(Offre,
				agent=self.agent,
				bien=bien,
				pk=accepter,
			)
			offre.contre_offre_acceptee=True

			new_offre = Offre(prix=offre.prix)
			new_offre.counter(offre)

			messages.info(request, "Vous venez d'accepter une offre "\
				"de %s euros." % str(offre.prix))

			notify(offre.agent.user, u'Offre acceptée')
			notify_email(offre.agent.email, u"Offre acceptée", u"email/pro/offre_acceptee.html", {
				u"fullname": offre.agent.get_full_name(),
				u"fullname_vendeur": offre.vendeur.get_full_name(),
				u"montant_offre": "%s €" % ('{:,}'.format(offre.prix).replace(',', ' ')),
				u"titre_annonce": bien,
				u"reference": bien.nb_mandat
			})

			notify(offre.vendeur.user, u'Offre acceptée')
			notify_email(offre.vendeur.email, u"Offre acceptée", u"email/vendeur/offre_acceptee.html", {
				u"fullname": offre.vendeur.get_full_name(),
				u"fullname_agent": offre.agent.get_full_name(),
				u"montant_offre": "%s €" % ('{:,}'.format(offre.prix).replace(',', ' ')),
				u"titre_annonce": offre.bien,
				u"reference": offre.bien.nb_mandat
			})
			return redirect(reverse('utilisateurs:list-ventes'))

		elif refuser:
			offre = get_object_or_404(Offre,
				agent=self.agent,
				bien=bien,
				pk=refuser
			)

			offre.refuse()

			notify(offre.vendeur.user, u'Offre refusée')
			notify_email(offre.vendeur.email, u"Offre refusée", u"email/vendeur/offre_refusee.html", {
				u"fullname": offre.vendeur.get_full_name(),
				u"fullname_agent": self.agent.get_full_name(),
				u"montant_offre": "%s €" % ('{:,}'.format(offre.prix).replace(',', ' ')),
				u"titre_annonce": bien,
				u"reference": bien.nb_mandat
			})

			messages.info(request, "Vous venez de refuser une offre "\
				"de %s euros." % str(offre.prix)
			)

		elif contre:
			offre = get_object_or_404(Offre,
				agent=self.agent,
				bien=bien,
				pk=contre
			)
			form = AgentOffreDetailView.OffreForm(request.POST)
			if form.is_valid() and offre.prix != form.cleaned_data['prix']:
				co = form.save(commit=False)
				co.counter(offre)

				v_user = offre.vendeur.user
				notify(offre.vendeur.user, u'Nouvelle contre-offre')
				notify_email(offre.vendeur.email, u"Nouvelle contre-offre", u"email/vendeur/nouvelle_contre_offre.html", {
					u"fullname": offre.vendeur.get_full_name(),
					u"fullname_agent": offre.agent.get_full_name(),
					u"fullname_acheteur": offre.acheteur.get_full_name() if offre.acheteur is not None else offre.agent_client.get_full_name(),
					u"montant_contre_offre": "%s €" % ('{:,}'.format(co.prix).replace(',', ' ')),
					u"titre_annonce": offre.bien,
					u"reference": offre.bien.nb_mandat
				})

				messages.info(request,
					"Votre contre-offre a bien été"\
					" envoyée.")
			elif offre.prix == form.cleaned_data['prix']:
				messages.error(request, "La contre-offre doit être différente de l'offre précédente. Veuillez utiliser le bouton accepter pour valider l'offre reçue.")

		return redirect(reverse('utilisateurs:list-offres'))

class AgentActualitesView(AgentTestMixin, ListView):
	model = Bien
	template_name = 'utilisateurs/agents/actualites.html'
	paginate_by = ELEMENT_BY_PAGE

	def get_queryset(self):
		from django.contrib.gis.measure import D
		from datetime import datetime, timedelta

		try:
			distance = int(self.request.GET.get('distance'))
		except:
			distance = 30
		if distance < 0 : distance = 0;
		if distance > 100: distance = 100;

		return super(AgentActualitesView, self).get_queryset() \
			.exclude(valide=None) \
			.exclude(deleted__isnull=False) \
			.exclude(offres__acceptee__isnull=False) \
			.filter(
				valide__gte = datetime.now()-timedelta(days=45),
				localisation__distance_lte=(
					self.agent.localisation,
					D(km = distance)
				)

			).order_by("-creation")


class MesAnnoncesListView(UserIsClientMixin, FilterClientBiensMixin, ListView):

	model = Bien
	template_name = 'utilisateurs/clients/annonces.html'
	paginate_by = 10


	def get_context_data(self, *args, **kwargs):
		from django.contrib.auth.models import User
		from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
		from datetime import datetime, timedelta
		date_del =  datetime.now()- timedelta(days=15)
		date_exp =  datetime.now()- timedelta(days=365)

		context = super(MesAnnoncesListView,
			self).get_context_data(*args, **kwargs)
		object_list = []
		for bien in Bien.objects.exclude(creation__date__gt=date_exp).exclude(deleted__date__gt=date_del).filter(vendeur=self.client):
			object_list.append(bien)
		for bien in Bien.objects.filter(vendeur=self.client,deleted=None):
			object_list.append(bien)

		for bien_a_valider in BienAValider.objects.filter(vendeur=self.client, source=None).all():
			object_list.append(bien_a_valider)

		context["object_list"] = object_list
		return context




class BienOrBienAValider(DetailView):
	def get_queryset(self):
		import datetime
		date_= datetime.datetime.now()
		return super(BienOrBienAValider, self).get_queryset().filter(deleted=None)

	def get_context_data(self, *args, **kwargs):
		context = super(BienOrBienAValider,
			self).get_context_data(*args, **kwargs)
		context['is_bien_a_valider'] = isinstance(self.object, BienAValider)
		return context

class AnnonceDocumentsDetailView(UserIsClientMixin, BienOrBienAValider, FilterClientBiensMixin):
	model = Bien
	slug_field = 'nb_mandat'
	slug_url_kwarg = 'nb_mandat'
	template_name = 'utilisateurs/clients/annonce_documents.html'
	DocumentForm = modelform_factory(Document,
		fields=('type', 'name', 'file')
	)
	form = None

	def get_context_data(self, *args, **kwargs):
		bien = self.get_object()
		context = super(AnnonceDocumentsDetailView,
			self).get_context_data(*args, **kwargs)
		context['documents_types'] = TYPES_DOCUMENTS

		if context['is_bien_a_valider']:
			context["all_documents"] = bien.documents_a_valider
		else:
			context["all_documents"] = bien.documents

		if self.form:
			context['form'] = self.form
		else:
			context['form'] = AnnonceDocumentsDetailView.DocumentForm()
		return context


	def post(self, request, *args, **kwargs):
		bien = self.get_object()
		supprimer = request.GET.get('supprimer', None)
		self.form = AnnonceDocumentsDetailView.DocumentForm(
			request.POST,
			request.FILES
		)
		redirection = reverse(
			'utilisateurs:mes-annonces-documents',
			args=[bien.nb_mandat]
		)

		if supprimer:
			if isinstance(bien, BienAValider):
				document = get_object_or_404(Document,
					bien_a_valider=bien, pk=supprimer)
			else:
				document = get_object_or_404(Document,
					bien=bien, pk=supprimer)
			document.delete()
			messages.info(request,
				'Votre document "%s" a bien été supprimé.' % document.name
			)
			return redirect(redirection)

		if self.form.is_valid():
			document = self.form.save(commit=False)
			if isinstance(bien, BienAValider):
				document.bien_a_valider = bien
			else:
				document.bien = bien
			document.save()
			messages.info(request,
				'Votre document "%s" a bien été ajouté.' % document.name
			)
			return redirect(redirection)

		return self.get(request, *args, **kwargs)


class AnnonceVisitesDetailView(UserIsClientMixin, FilterClientBiensMixin, BienOrBienAValider):
	model = Bien
	slug_field = 'nb_mandat'
	slug_url_kwarg = 'nb_mandat'
	template_name = 'utilisateurs/clients/annonce_visites.html'


class AnnonceOffresDetailView(UserIsClientMixin, FilterClientBiensMixin, BienOrBienAValider):
	template_name = 'utilisateurs/clients/annonce_offres.html'
	OffreForm = modelform_factory(Offre, fields=('prix',))
	model = Bien
	slug_field = 'nb_mandat'
	slug_url_kwarg = 'nb_mandat'

	def get_context_data(self, *args, **kwargs):
		bien = self.get_object()
		context = super(AnnonceOffresDetailView,
			self).get_context_data(*args, **kwargs)
		if not context['is_bien_a_valider']:
			context['historique'] = Offre.objects.filter(
				Q(bien=bien),
				Q(refusee__isnull=False) | Q(acceptee__isnull=False)
				| Q(acheteur=self.request.user.client) | Q(vendeur=self.request.user.client)
			).order_by('-created')
		if not context['is_bien_a_valider']:
			context['offres'] = Offre.objects.filter(
				bien=bien, initiater=False, refusee=None, acceptee=None
			).order_by('-created')
		context['form'] = AnnonceOffresDetailView.OffreForm()
		return context

	def post(self, request, *args, **kwargs):
		bien = self.get_object()
		accepter = request.GET.get('accepter', None)
		refuser = request.GET.get('refuser', None)
		contre = request.GET.get('contre-offre', None)


		if accepter:
			offre = get_object_or_404(Offre,
				vendeur=self.client,
				bien=bien,
				pk=accepter
			)
			vente = offre.accept()

			messages.info(request, "Vous venez d'accepter une offre "\
				"de %s euros." % str(offre.prix))

			notify(self.client.user, u'Offre acceptée et confirmée !')
			notify_email(self.client.email, u"Offre acceptée et confirmée !", u"email/vendeur/offre_acceptee_et_confirmee.html", {
				u"fullname": self.client.get_full_name(),
				u"fullname_agent": offre.agent.get_full_name(),
				u"titre_annonce": offre.bien,
				u"reference": offre.bien.nb_mandat
			})

			acheteur_full_name = (offre.acheteur if offre.acheteur else offre.agent_client).get_full_name()

			notify(offre.agent.user, u'Offre acceptée')
			notify_email(offre.agent.email, u"Offre acceptée", u"email/pro/offre_acceptee_par_vendeur.html", {
				u"fullname": offre.agent.get_full_name(),
				u"fullname_acheteur": acheteur_full_name,
				u"fullname_vendeur": self.client.get_full_name(),
				u"montant_offre": "%s €" % ('{:,}'.format(offre.prix).replace(',', ' ')),
				u"titre_annonce": offre.bien,
				u"reference": offre.bien.nb_mandat
			})

			# render HTML to PDF
			template_mandat_pdf = "biens/pdf/mandat.html"
			template_offre_pdf = "biens/pdf/offre_acceptee.html"
			template_facture_pdf = "biens/pdf/facture_vente.html"
			file_mandat_pdf = render_to_content_file(template_mandat_pdf, {u"bien": bien})
			file_offre_pdf = render_to_content_file(template_offre_pdf, {u"offre": offre})
			file_facture_pdf = render_to_content_file(template_facture_pdf, {u"object": vente})

			# save files
			upload_directory = getattr(settings, "MEDIA_ROOT", '/')
			path_mandat = default_storage.save('documents/biens/%s/mandat.pdf' % bien.nb_mandat, file_mandat_pdf)
			path_offre = default_storage.save('documents/biens/%s/offre_acceptee.pdf' % bien.nb_mandat, file_offre_pdf)
			path_facture = default_storage.save('documents/biens/%s/facture_vente.pdf' % vente.pk, file_facture_pdf)

			# send mail with files
			if settings.NOTIFY_NOTAIRE:
				notify_email(bien.notaire.email, u"Nouvelle vente", u"email/nouvelle_offre_notaire.html", {
					u"fullname": bien.notaire.get_full_name(),
					u"fullname_vendeur": bien.vendeur.get_full_name(),
					u"titre_annonce": bien,
					u"reference": bien.nb_mandat
				}, files=[
					upload_directory + '/' + path_mandat,
					upload_directory + '/' + path_offre,
					upload_directory + '/' + path_facture
				])

		elif refuser:
			offre = get_object_or_404(Offre,
				vendeur=self.client,
				bien=bien,
				pk=refuser
			)
			offre.refuse()

			messages.info(request, "Vous venez de refuser une offre "\
				"de %s euros." % str(offre.prix)
			)

			acheteur_full_name = (offre.acheteur if offre.acheteur else offre.agent_client).get_full_name()

			notify(offre.agent.user, u'Offre refusée')
			notify_email(offre.agent.email, u"Offre refusée", u"email/pro/offre_refusee_par_vendeur.html", {
				u"fullname": offre.agent.get_full_name(),
				u"fullname_acheteur": acheteur_full_name,
				u"fullname_vendeur": self.client.get_full_name(),
				u"montant_offre": "%s €" % ('{:,}'.format(offre.prix).replace(',', ' ')),
				u"titre_annonce": offre.bien,
				u"reference": offre.bien.nb_mandat
			})

		elif contre:
			offre = get_object_or_404(Offre,
				vendeur=self.client,
				bien=bien,
				pk=contre
			)
			form = AnnonceOffresDetailView.OffreForm(request.POST)
			if form.is_valid() and offre.prix != form.cleaned_data['prix']:
				co = form.save(commit=False)
				co.counter(offre, vendeur=True)

				notify(offre.agent.user, u'Nouvelle contre-offre')
				notify_email(offre.agent.email, u"Nouvelle contre-offre", u"email/pro/nouvelle_contre_offre.html", {
					u"fullname": offre.agent.get_full_name(),
					u"fullname_vendeur": self.client.get_full_name(),
					u"montant_contre_offre": "%s €" % ('{:,}'.format(co.prix).replace(',', ' ')),
					u"titre_annonce": offre.bien,
					u"reference": offre.bien.nb_mandat
				})

				messages.info(request,
					"Votre contre-offre a bien été"\
					" envoyée.")
			elif offre.prix == form.cleaned_data['prix']:
				messages.error(request, "La contre-offre doit être différente de l'offre précédente. Veuillez utiliser le bouton accepter pour valider l'offre reçue.")

		return redirect(
			reverse(
				'utilisateurs:mes-annonces-offres',
				args=[bien.nb_mandat]
			)
		)



class AnnonceJoursOffDetailView(UserIsClientMixin, FilterClientBiensMixin, BienOrBienAValider, DetailView):
	model = Bien
	slug_field = 'nb_mandat'
	slug_url_kwarg = 'nb_mandat'
	template_name = 'utilisateurs/clients/annonce_joursoff.html'
	JourOffForm = modelform_factory(JourOff, fields=('debut', 'fin',))
	form = None

	def get_context_data(self, *args, **kwargs):
		bien = self.get_object()
		context = super(AnnonceJoursOffDetailView,
			self).get_context_data(*args, **kwargs)

		if isinstance(bien, BienAValider):
			context['jours_off'] = bien.jours_off_a_valider
		else:
			context['jours_off'] = bien.jours_off
		if self.form:
			context['form'] = self.form
		else:
			context['form'] = AnnonceJoursOffDetailView.JourOffForm()

		return context

	def post(self, request, *args, **kwargs):
		bien = self.get_object()
		annuler = request.GET.get('annuler', None)
		self.form = AnnonceJoursOffDetailView.JourOffForm(request.POST)
		redirection = reverse(
			'utilisateurs:mes-annonces-jours-off',
			args=[bien.nb_mandat]
		)

		if annuler:
			if isinstance(bien, BienAValider):
				jo = get_object_or_404(JourOff, bien_a_valider=bien, pk=annuler)
			else:
				jo = get_object_or_404(JourOff, bien=bien, pk=annuler)
			jo.delete()
			messages.info(request,
					"Votre indisponibilité %s"\
					" a bien été supprimée." % str(jo))
			return redirect(redirection)

		elif self.form.is_valid():
			jo = self.form.save(commit=False)
			if isinstance(bien, BienAValider):
				jo.bien_a_valider = bien
			else:
				jo.bien = bien
			jo.save()
			messages.info(request,
					"Votre indisponibilité %s"\
					" a bien été ajoutée." % str(jo))
			return redirect(redirection)
		else:
			return self.get(request, *args, **kwargs)

class MesFavorisListView(UserIsClientMixin, ListView):
	model = Bien
	template_name = 'utilisateurs/clients/acquereur_favoris.html'
	paginate_by = ELEMENT_BY_PAGE

	def get_queryset(self):
		if hasattr(self, 'client'):
			return self.client.favoris.all()
		ids = self.request.user.agent.favorisliste.all().values_list('favoris', flat=True)
		return super(MesFavorisListView, self).get_queryset().filter(id__in=ids)

	def post(self, request, *args, **kwargs):
		favori = get_object_or_404(Bien,
			nb_mandat=request.POST.get('favori', None)
		)
		next_page = self.request.GET.get('next', None)
		referer_page = self.request.META.get('HTTP_REFERER')

		if hasattr(self, 'client'):
			if favori in self.client.favoris.all():
				self.client.favoris.remove(favori)
				messages.error(request,
					"%s a bien été supprimé(e) de vos favoris." % favori
				)
			else:
				self.client.favoris.add(favori)
				messages.info(request,
					"%s a bien été ajouté(e) a vos favoris." % favori
				)
		else:
			liste = request.POST.get('liste-favoris', None)
			new_liste = request.POST.get('nouvelle-liste', None)
			if new_liste is not None and new_liste != '':
				if FavorisListe.objects.filter(agent=self.request.user.agent, nom=new_liste).exists():
					liste = FavorisListe.objects.get(agent=self.request.user.agent, nom=new_liste)
				else:
					liste = FavorisListe(agent=self.request.user.agent, nom=new_liste)
					liste.save()
			elif liste is not None and liste != '':
				liste = get_object_or_404(FavorisListe, pk=liste, agent=self.request.user.agent)
			else:
				liste = favori.favorislists.filter(agent=self.request.user.agent).first()

			if liste:
				if favori in liste.favoris.all():
					liste.favoris.remove(favori)
					messages.error(request,
						"%s a bien été supprimé(e) de vos favoris." % favori
					)
				else:
					liste.favoris.add(favori)
					messages.info(request,
						"%s a bien été ajouté(e) à vos favoris." % favori
					)
			else:
				messages.error(request,
					"Veuillez sélectionner une liste de favoris existante ou nommer la liste à créer"
				)

		if next_page:
			return redirect(next_page)
		return redirect(referer_page)

class MesVisitesListView(UserIsClientMixin, ListView):
	model = RDV
	template_name = 'utilisateurs/clients/acquereur_visites.html'
	paginate_by = 6

	def get_queryset(self):
		# Get all the Agent RDV from the closest to now
		return RDV.all_objects.filter(Q(acheteur=self.client)&~Q(vendeur=self.client)).order_by('crenau')

class AnnonceDeleteView(UserIsClientMixin, FilterClientBiensMixin, DeleteView):
	model = Bien
	slug_field = 'nb_mandat'
	slug_url_kwarg = 'nb_mandat'
	success_url = reverse_lazy('utilisateurs:mes-annonces')

	def post(self, request, *args, **kwargs):
		bien = self.get_object()
		bien.deleted = timezone.now()
		bien.save()
		messages.info(request, "Votre annonce reste en ligne pour une durée de quinze jours, passé ce délai elle sera supprimée.")
		return redirect(self.success_url)

class AnnonceAValiderDeleteView(UserIsClientMixin, FilterClientBienAValiderMixin, DeleteView):
	model = BienAValider
	slug_field = 'nb_mandat'
	slug_url_kwarg = 'nb_mandat'
	success_url = reverse_lazy('utilisateurs:mes-annonces')
	template_name = 'biens/bien_confirm_delete.html'

	def post(self, request, *args, **kwargs):
		super(AnnonceAValiderDeleteView, self).post(request, *args, **kwargs)
		messages.info(request, "Votre annonce a été supprimée avec succés.")
		return redirect(self.success_url)

class AnnonceUpdateView(UserIsClientMixin, FilterClientBiensMixin, UpdateView):
	model = Bien
	slug_field = 'nb_mandat'
	slug_url_kwarg = 'nb_mandat'
	fields = BienCreateView.fields
	template_name = 'biens/bien_form.html'
	old_prix = None

	def get_context_data(self, *args, **kwargs):
		context = super(AnnonceUpdateView,
			self).get_context_data(*args, **kwargs)
		context['update_info'] = True

		return context
	def get(self, request, **kwargs):
		self.bien = Bien.objects.get(nb_mandat=self.kwargs['nb_mandat'])
		bienAV = BienAValider.objects.filter(nb_mandat=self.kwargs['nb_mandat']).exists()
		if bienAV:
			messages.info(self.request, "Ce bien est déja en attente de modification, "+
				"vous ne pouvez pas le modifier tant que la dernière modification n'a pas été validée.")
			return redirect(reverse_lazy('utilisateurs:mes-annonces'))

		if(self.bien.get_vendu()):
			messages.info(self.request, "Ce bien a déjà été vendu, vous ne pouvez plus modifier son annonce.")
			return redirect(reverse_lazy('utilisateurs:mes-annonces'))

		return super(AnnonceUpdateView, self).get(self, request, **kwargs)

	def get_success_url(self):
		return reverse_lazy('utilisateurs:mes-annonces-a-valider-modifier-infos', args=[self.object.nb_mandat])

	def form_valid(self, form):

		if not isinstance(form.instance, BienAValider):
			# Copie all the value of the Bien (before update)
			old_bien = form.instance
			all_fields = old_bien.get_fields()
			all_fields.pop("deleted")
			all_fields["source"] = old_bien
			# Create a BienAValider with the old value
			form.instance = BienAValider.objects.create(**all_fields)

			for photo in Photo.objects.filter(bien=old_bien):
				# Copy the image (dplicate the file)
				# Note: Attempt to copy only the instance but on the delete the file is really deleted
				# TODO: Optimisation, copy only the instance of the image: Photo.objects.create(bien_a_valider=form.instance, image=photo.image)
				# Note: Be aware of the deletion of the Photo instance
				p = Photo.objects.create(bien_a_valider=form.instance, image=None)
				p.image = ContentFile(StringIO.StringIO(photo.image.read()).getvalue())
				p.image.name = "copy.png"
				p.save()
			for dispo in Disponibilite.objects.filter(bien=old_bien):
				Disponibilite.objects.create(bien_a_valider=form.instance, jour=dispo.jour, debut=dispo.debut, fin=dispo.fin)
			add_logentry_bienavalider(self.request.user.id, form.instance, form.changed_data)
			form.instance.prix = int(form.instance.prix*1.03 + 0.5)

		return super(AnnonceUpdateView, self).form_valid(form)

class AnnonceDispoUpdateView(UserIsClientMixin, FilterClientBiensMixin, UpdateView):
	model = Bien
	slug_field = 'nb_mandat'
	slug_url_kwarg = 'nb_mandat'
	fields = BienCreateView.fields
	template_name = 'biens/bien_dispos_form.html'
	dispo_form = None
	photo_form = None

	def get_photo_form(self):
		return PhotoCreateView(request=self.request).get_form()

	def get_dispo_form(self):
		return DisponibiliteCreateView(request=self.request).get_form()

	def get_context_data(self, *args, **kwargs):
		context = super(AnnonceDispoUpdateView,
			self).get_context_data(*args, **kwargs)

		context['dispo_form'] = self.dispo_form or self.get_dispo_form()
		context['photo_form'] = self.photo_form or self.get_photo_form()
		context['disponibilites'] = self.object.disponibilites
		context['photos'] = self.object.photos
		context['update_info'] = True

		return context

	def post(self, request, *args, **kwargs):
		bien = self.get_object()
		self.request.session['nb_mandat'] = bien.nb_mandat
		if request.POST.get('add_disponibilite'):

			self.dispo_form = self.get_dispo_form()
			if self.dispo_form.is_valid():

				self.dispo_form.instance.bien = bien
				self.dispo_form.save()
				add_logentry_bienavalider(self.request.user.id, bien, [u"disponibilites"])
			else:
				return self.get(request, nb_mandat=bien.nb_mandat, *args, **kwargs)

		elif request.POST.get('add_photo'):

			self.photo_form = self.get_photo_form()
			if self.photo_form.is_valid():
				self.photo_form.instance.bien = bien
				self.photo_form.save()
				add_logentry_bienavalider(self.request.user.id, bien, [u"photos"])
			else:
				return self.get(request, nb_mandat=bien.nb_mandat, *args, **kwargs)

		elif request.POST.get('set_notaire'):
			notaire_id = request.POST.get('set_notaire')
			notaire = get_object_or_404(Notaire, pk=notaire_id)
			bien_a_valider.notaire = notaire
			bien_a_valider.save()

		elif request.POST.get('validate'):
			return redirect(reverse_lazy('validation'))

		elif request.POST.get('next'):
			messages.info(self.request, "Votre annonce à bien été mise à jours. "\
			"Celle-ci est desormais en attente de validation.")
			bien.checked = True
			bien.save()
			return redirect(reverse_lazy('utilisateurs:mes-annonces'))

		return redirect(reverse_lazy('utilisateurs:mes-annonces-dispos-modifier', args=[bien.nb_mandat]))

class AnnonceAValiderUpdateView(UserIsClientMixin, FilterClientBienAValiderMixin, UpdateView):
	model = BienAValider
	slug_field = 'nb_mandat'
	slug_url_kwarg = 'nb_mandat'
	fields = BienCreateView.fields
	template_name = 'biens/bien_form.html'

	def get(self, request, **kwargs):
		self.bien = BienAValider.objects.get(nb_mandat=self.kwargs['nb_mandat'])
		return super(AnnonceAValiderUpdateView, self).get(self, request, **kwargs)

	def get_success_url(self):
		return reverse_lazy('utilisateurs:mes-annonces-modifier-infos', args=[self.object.nb_mandat])

	def form_valid(self, form):

		add_logentry_bienavalider(self.request.user.id, form.instance, form.changed_data)
		return super(AnnonceAValiderUpdateView, self).form_valid(form)

class AnnonceUpdateRelatedDetailView(UserIsClientMixin, FilterClientBiensMixin, UpdateView):
	model = Bien
	slug_field = 'nb_mandat'
	slug_url_kwarg = 'nb_mandat'
	fields = BienCreateView.fields
	template_name = "biens/related_forms.html"
	dispo_form = None
	photo_form = None

	def get_photo_form(self):
		return PhotoCreateView(request=self.request).get_form()

	def get_dispo_form(self):
		return DisponibiliteCreateView(request=self.request).get_form()

	def get_context_data(self, *args, **kwargs):
		context = super(AnnonceUpdateRelatedDetailView,
			self).get_context_data(*args, **kwargs)

		context['dispo_form'] = self.dispo_form or self.get_dispo_form()
		context['photo_form'] = self.photo_form or self.get_photo_form()
		context['disponibilites'] = self.object.disponibilites
		context['photos'] = self.object.photos_a_valider
		context['update_info'] = True


		return context

	def post(self, request, *args, **kwargs):
		bien_a_valider = self.get_object()
		self.request.session['nb_mandat'] = bien_a_valider.nb_mandat
 		if request.POST.get('add_photo'):
			self.photo_form = self.get_photo_form()
			if self.photo_form.is_valid():
				self.photo_form.instance.bien = bien_a_valider
				self.photo_form.save()
				add_logentry_bienavalider(self.request.user.id, bien_a_valider, [u"photos"])
			else:
				return self.get(request, nb_mandat=bien_a_valider.nb_mandat, *args, **kwargs)

		# elif request.POST.get('set_notaire'):
		# 	notaire_id = request.POST.get('set_notaire')
		# 	notaire = get_object_or_404(Notaire, pk=notaire_id)
		# 	bien_a_valider.notaire = notaire
		# 	bien_a_valider.save()

		elif request.POST.get('validate'):
			return redirect(reverse_lazy('validation'))

		elif request.POST.get('next'):
			messages.info(self.request, "Votre annonce à bien été mise à jours. "\
			"Celle-ci est desormais en attente de validation.")
			bien_a_valider.checked = False
			bien_a_valider.save()
			return redirect(reverse_lazy('validation-modification'))

		return redirect(reverse_lazy('utilisateurs:mes-annonces-modifier-infos', args=[bien.nb_mandat]))

class AnnonceAValiderUpdateRelatedDetailView(UserIsClientMixin, FilterClientBienAValiderMixin, UpdateView):
	import sys

	reload(sys)
	sys.setdefaultencoding('utf8')
	model = BienAValider
	slug_field = 'nb_mandat'
	slug_url_kwarg = 'nb_mandat'
	fields = BienCreateView.fields
	template_name = "biens/related_forms.html"
	dispo_form = None
	photo_form = None

	def get_photo_form(self):
		return PhotoCreateView(request=self.request).get_form()

	def get_dispo_form(self):
		return DisponibiliteCreateView(request=self.request).get_form()

	def get_context_data(self, *args, **kwargs):
		context = super(AnnonceAValiderUpdateRelatedDetailView,
			self).get_context_data(*args, **kwargs)

		context['dispo_form'] = self.dispo_form or self.get_dispo_form()
		context['photo_form'] = self.photo_form or self.get_photo_form()
		context['disponibilites'] = self.object.disponibilites_a_valider
		context['photos'] = self.object.photos_a_valider
		context['update_info'] = True

		return context

	def post(self, request, *args, **kwargs):
		bien_a_valider = self.get_object()
		self.request.session['nb_mandat'] = bien_a_valider.nb_mandat
		if request.POST.get('add_disponibilite'):

			self.dispo_form = self.get_dispo_form()
			if self.dispo_form.is_valid():
				self.dispo_form.instance.bien_a_valider = bien_a_valider
				self.dispo_form.save()
			else:
				return self.get(request, nb_mandat=bien_a_valider.nb_mandat, *args, **kwargs)
		elif request.POST.get('add_photo'):

			self.photo_form = self.get_photo_form()
			if self.photo_form.is_valid():

				self.photo_form.instance.bien_a_valider = bien_a_valider
				self.photo_form.save()

			else:
				return self.get(request, nb_mandat=bien_a_valider.nb_mandat, *args, **kwargs)

		elif request.POST.get('set_notaire'):
			notaire_id = request.POST.get('set_notaire')
			notaire = get_object_or_404(Notaire, pk=notaire_id)
			bien_a_valider.notaire = notaire
			bien_a_valider.save()

		elif request.POST.get('next'):

			bien_a_valider.checked = False
			bien_a_valider.save()

			vendeur = bien_a_valider.vendeur

			return redirect(reverse_lazy('validation-modification'),args=[bien_a_valider.nb_mandat])

		return redirect(reverse_lazy('utilisateurs:mes-annonces-a-valider-modifier-infos', args=[bien_a_valider.nb_mandat]))

@method_decorator(csrf_exempt, name='dispatch')
class BienDeleteView(DeleteView):

	model = Bien
	slug_field = 'nb_mandat'
	slug_url_kwarg = 'nb_mandat'
	success_url=reverse_lazy('admin:biens_bien_changelist')
	def post(self, request, *args, **kwargs):
		from datetime import datetime, timedelta
		bien = self.get_object()

		action = self.request.GET.get("action", None)

		if action == "supprimer":
			bien.deleted =  datetime.now() - timedelta(days=16)
			bien.save()
		if action == "repost":
			bien.deleted = None
			bien.save()


		return redirect(self.success_url)



@method_decorator(csrf_exempt, name='dispatch')
class BienAValiderDeleteView(DeleteView):
	permission_required = 'biens.delete_bienavalider'
	model = BienAValider
	slug_field = 'nb_mandat'
	slug_url_kwarg = 'nb_mandat'
	success_url = reverse_lazy('admin:biens_bienavalider_changelist')

	def move_bien_related_to_bien(self, bien, photos, dispos, documents, days_off):
		"""
		Change the dependencie of Photo and Disponibilite from a BienAValider to a Bien

		:param Bien bien: The bien to link
		:param [Photo] photos: The photo list
		:param [Disponibilite] dispos: The disponibilite list
		:param [Documents] dispos: The docuement list
		:param [JourOff] dispos: The day off list
		"""
		for new_img in photos:
			# Move all the new photo to the bien
			Photo.objects.filter(pk=new_img.pk).update(bien=bien, bien_a_valider=None)
		for new_dispo in dispos:
			# Move all the new dispo to the bien
			Disponibilite.objects.filter(pk=new_dispo.pk).update(bien=bien, bien_a_valider=None)
		for document in documents:
			# Move all the document to the bien (which was attach to the bien_a_valider before the bien exist)
			Document.objects.filter(pk=document.pk).update(bien=bien, bien_a_valider=None)
		for day_off in days_off:
			# Move all the day_off to the bien (which was attach to the bien_a_valider before the bien exist)
			JourOff.objects.filter(pk=day_off.pk).update(bien=bien, bien_a_valider=None)


	def remove_bien_related(self, bien_photos, bien_dispos):
		"""
		Remove the Photos and the Disponibilites of a bien.
		.

		:param [Photo] bien_photos: The photos to remove
		:param [Disponibilite] bien_dispos: The disponibilites to remove
		"""
		for img in bien_photos:
			# Remove all photos
			img.delete()
		for dispo in bien_dispos:
			# Remove all dispos
			dispo.delete()

	def get_fields_for_bien(self, bav):
		"""
		Retieve the BienAValider fields and remove the fields which are not in the model Bien

		:param BienAValider bav: The bien a valider
		:rtype: [{'': Object}]
		:return: The list of fields
		"""
		bien_fields = bav.get_fields()
		bien_fields.pop('modification_a_valider', None)
		bien_fields.pop('checked', None)
		bien_fields.pop('source', None)
		bien_fields.pop('creation')
		return bien_fields

	def create_bien(self, bav):
		"""
		Create a Bien from a BienAValider

		:param BienAValider bav: The bien a valider
		"""
		bien_fields = self.get_fields_for_bien(bav)
		bien_fields["valide"] = timezone.now()
		bien = Bien.objects.create(**bien_fields)
		Mandat(bien=bien).save()

		vendeur = bien.vendeur
		notify_email(vendeur.user.email, u"Dépôt d'annonce : validation de l'annonce", u"email/vendeur/validation_annonce.html", {
			u"fullname": vendeur.get_full_name(),
			u"annonce": bien
		})

		return bien

	def update_bien(self, bav):
		"""
		Update a Bien from a BienAValider

		:param BienAValider bav: The bien a valider
		"""
		bien_fields = self.get_fields_for_bien(bav)
		# Do not update this two next fields
		bien_fields.pop('notaire', None)
		bien_fields.pop('vendeur', None)
		bien_fields["valide"] = timezone.now()
		Bien.objects.filter(nb_mandat=bav.source.nb_mandat, deleted=None).update(**bien_fields)

		bien = Bien.objects.get(nb_mandat=bav.source.nb_mandat)
		vendeur = bien.vendeur
		notify_email(vendeur.user.email, u"Mise à jour d'annonce : validation de vos modifications", u"email/vendeur/validation_annonce_modifiee.html", {
			u"fullname": vendeur.get_full_name(),
			u"annonce": bien
		})

	def post(self, request, *args, **kwargs):
		"""
		Override the post request and don't call the post() of the object parent
		"""
		# Extract Data
		bav = self.get_object()
		action = self.request.GET.get("action", None)
		reason = self.request.GET.get("raison", "")
		if action == "accepter":
			bav_photos = bav.photos_a_valider.all()
			bav_dispos = bav.disponibilites_a_valider.all()
			bav_documents = bav.documents_a_valider.all()
			bav_days_off = bav.jours_off_a_valider.all()
			if bav.source is None:
				# New Bien
				# Create a bien from a bien_a_valider
				bien = self.create_bien(bav)
				self.move_bien_related_to_bien(bien, bav_photos, bav_dispos, bav_documents, bav_days_off)
				# Delete the BienAValider (full merged)
				bav.delete()
				messages.info(self.request, "Le Bien à été créé, le vendeur en est notifié")
				notify(bien.vendeur.user, u'Votre annonce a été validée')
				send_alert_notification(request, bien)
			else:
				# Existing Bien
				# Update a bien_a_valider into a bien
				self.update_bien(bav)
				# Remove the bien related (dispo/photo...) of the current Bien
				self.remove_bien_related(bav.source.photos.all(), bav.source.disponibilites.all())
				# Set The new Photo+Dispo to the bien
				self.move_bien_related_to_bien(bav.source, bav_photos, bav_dispos, bav_documents, bav_days_off)
				# Notify
				notify(bav.vendeur.user, u'Votre annonce a été mise à jour')
				# Delete the BienAValider (full merged)
				bav.delete()
				messages.info(self.request, "Le Bien à été mis à jour, le vendeur en est notifié")
		elif action == "refuser":
			if bav.source is None:
				# New Bien
				bav.checked = True
				bav.save()
				messages.info(self.request, "Le Bien n'a pas été validé, le vendeur en est notifié (il doit le changer)")
				notify(bav.vendeur.user, u'Votre bien n\'a pas été validé')
			else:
				# Existing Bien
				# Delete all related bien
				self.remove_bien_related(bav.photos_a_valider.all(), bav.disponibilites_a_valider.all())
				# Notify
				notify(bav.vendeur.user, u'La mise à jour de votre bien a été refusée')
				# Delete the BienAValider (refused)
				bav.delete()
				messages.info(self.request, "La mise à jour du Bien est annulé, le vendeur en est notifié")
		return redirect(self.success_url)

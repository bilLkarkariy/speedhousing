# -*- coding: utf-8 -*-

TYPES_CLIENTS = (
	( '', u"Civilité"),
	('mme', u"Madame"),
	('mr', u"Monsieur")
)


TYPES_INSCRIPTIONS = (
	( '', u"Je m'inscris avec ..."),
	('crt', u"Je m'inscris avec ma carte professionnelle"),
	('att', u"Je m'inscris avec mon attestation de collaborateur")
)

TYPE_QUALITE = (
	( '', u"Qualité de la collaboration"),
	('sal' , u"Salarié(e)"),
	('agc' , u"Agent commercial(e)")
)

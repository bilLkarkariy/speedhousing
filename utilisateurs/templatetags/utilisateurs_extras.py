#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django import template

register = template.Library()


@register.filter
def get_at_index(list,index):
    return list[index]

@register.filter
def bien_inFavoris(pk, lists):
    #for liste in lists:
    result = set()
    for liste in lists.favoris.all()[:len(lists.favoris.all()[:])]:
        result.add(list)

    return result

@register.filter
def documents_by_type(documents, type):
    return documents.filter(type=type).all()

@register.filter
def reverse_list(l):
    # Reverse a list
    return list(reversed(l))

@register.filter
def get_waiting_offers(offers, user):
	if user.agent is not None:
		return offers.filter(acceptee=None, refusee=None, agent=user.agent).order_by('-created')
	return offers.filter(acceptee=None, refusee=None).order_by('-created')

@register.filter
def get_past_offers(offers, user):
	if user.agent is not None:
		return offers.filter(agent=user.agent).order_by('-created')
	return offers.order_by('-created')

@register.filter
def is_bienavalider(obj):
    from biens.models import BienAValider
    return isinstance(obj, BienAValider)

@register.filter()
def get_changes(agent_a_valider):
    from ast import literal_eval
    from json import dumps
    changes = []
    for logentry in agent_a_valider.get_history():
        changes += literal_eval(logentry.change_message)
    return dumps(changes)

@register.filter
def get_type(index_type):
    if index_type == 1 :
        return  "c’est l’acte authentique qui prouve votre qualité de propriétaire et définit précisément le bien."

    elif index_type == 2 :
        return "le diagnostic surface carrez est obligatoire pour vendre un logement en copropriété (un appartement par exemple), il calcule la surface de plancher d'un bien sans prendre en compte l'espace occupé par les ouvertures, cloisons, marches, cages d'escaliers, espace dont la hauteur de plafond est inférieure à 1,80m, caves, garages, parkings. Ce diagnostic doit être obligatoirement réalisé par un diagnostiqueur certifié.           \n \nLe diagnostic surface habitable n’est pas obligatoire mais conseillé, il concerne les biens qui ne sont pas en copropriété (une maison par exemple).  "

    elif index_type == 3 :
        return "Selon la situation de votre bien votre diagnostiqueur pourra réaliser plusieurs de ces diagnostics :\n \n -  l’état de l’installation intérieure d’électricité ;\n -  l’état de l’installation intérieure de gaz « naturel » ;\n -  le diagnostic de performance énergétique du bâtiment (DPE);\n -  le constat de risque d’exposition au plomb (CREP) ;\n -  l’état mentionnant la présence ou l’absence de matériaux ou produits contenant de l’amiante ;\n -  l’état relatif à la présence de termites dans le bâtiment ;\n -  l’état des risques naturels et technologiques ;\n -  l’état des installations d’assainissement non collectif ;\n -  le diagnostic de performance énergétique du bâtiment (DPE)."

    elif index_type == 4 :
        return "il s’agit du procès-verbal de l’assemblée générale annuelle du syndic de copropriété, de l’année précédant celle en cours. Les trois dernières années sont obligatoires à transmettre pour l’information de l’acquéreur. "

    elif index_type == 5 :
        return "il s’agit du procès-verbal de l’assemblée générale annuelle du syndic de copropriété, datant de deux ans avant l’année en cours. Les trois dernières années sont obligatoires à transmettre pour l’information de l’acquéreur. "

    elif index_type == 6 :
        return "il s’agit du procès-verbal d’assemblée générale annuelle du syndic de copropriété, datant de trois ans avant l’année en cours. Les trois dernières années sont obligatoires à transmettre pour l’information de l’acquéreur. "

    elif index_type == 7 :
        return "tout immeuble en copropriété doit posséder un carnet d'entretien. Ce document répertorie des informations techniques sur la maintenance et les travaux effectués dans l'immeuble."

    elif index_type == 8 :
        return "autre document"

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.db import models
from django.urls import reverse
from sh.admin import admin_site

from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin
from django.utils.translation import ugettext_lazy as _
from django.utils.html import format_html

from support.forms import NewWindowAdminFileWidget

from utilisateurs.models import User, Client, Agent, AgentAValider, AgentClient
from utilisateurs.views import AgentAValiderDeleteView


from biens.admin import ValidationTypeListFilter

from django.shortcuts import redirect
from django.http import HttpResponseRedirect
from django.conf.urls import url

from django.http import HttpResponse


@admin.register(User, site=admin_site)
class UserAdmin(DjangoUserAdmin):
	"""
	Define admin model for custom User model with no email field.
	"""
	fieldsets = (
		(None, {'fields': ('email', 'password')}),
		(_('Personal info'), {'fields': ('first_name', 'last_name')}),
		(_('Permissions'), {'fields': ('is_active', 'is_staff',
			'is_superuser', 'groups', 'user_permissions')}),
		(_('Important dates'), {'fields': (
			'last_login', 'date_joined')}),
	)
	add_fieldsets = (
		(None, {
			'classes': ('wide',),
			'fields': ('email', 'password1', 'password2'),
		}),
	)
	list_display = ('email', 'first_name', 'last_name', 'is_staff')
	search_fields = ('email', 'first_name', 'last_name')
	ordering = ('last_login',)




class FicheClientAdmin(admin.ModelAdmin):
	list_display = ('civilite', 'prenom', 'nom',
		'societe', 'adresse_mail','confirmed_email', 'mobile', 'adresse',
		'code_postal', 'ville',)
	list_display_links = ('prenom', 'nom')
	search_fields = ('societe', 'siren', 'prenom', 'nom')


@admin.register(Client, site=admin_site)
class ClientAdmin(FicheClientAdmin):
	list_display = ('civilite', 'prenom', 'nom',
		'societe','email', 'confirmed_email', 'mobile', 'adresse',
		'code_postal', 'ville')

	def email(self, obj):
		url = reverse('admin:utilisateurs_user_change', args=[obj.user.pk])
		return format_html('<a href="{url}">{email}</a>',
			url=url, email=obj.user.email
		)

class AgentClientInline(admin.StackedInline):
	model = AgentClient

@admin.register(Agent, site=admin_site)
class AgentAdmin(admin.ModelAdmin):
	list_display = ('civilite', 'prenom', 'nom', 'email', 'confirmed_email',
		'adresse','code_postal', 'ville','societe', 'mobile', 'siege_adresse',
		 'siege_code_postal','siege_ville')
	list_display_links = ('prenom', 'nom', 'societe')
	search_fields = ('nom', 'prenom', 'code_postal', 'ville',)
	read_only_fields = ('email',)
	inlines = (AgentClientInline,)
	formfield_overrides = {
		models.FileField: {'widget': NewWindowAdminFileWidget}
	}

	def get_fields(self, request, obj=None):
		return (
			('notification', 'confirmed_email', 'confirm_code'),
			('civilite', 'prenom', 'nom'),
			('adresse','code_postal', 'ville'),
			('societe', 'siren'),
			'mobile',
			'type_inscription',
			('carte', 'attestation', 'qualite_colaboration') if obj is None or obj.is_attestation()
			else ('carte'),
			'siege_adresse',
			('siege_code_postal', 'siege_ville'),
			('legal_representant_prenom', 'legal_representant_nom'),
			('other_legal_representant_prenom', 'other_legal_representant_nom'),
			('assurance_nom', 'assurance_nb'),
		)

	def email(self, obj):
		url = reverse('admin:utilisateurs_user_change', args=[obj.user.pk])
		return format_html('<a href="{url}">{email}</a>',
			url=url, email=obj.user.email
		)


@admin.register(AgentAValider, site=admin_site)
class AgentAValiderAdmin(admin.ModelAdmin):
	change_form_template = 'utilisateurs/admin/change_agent_a_valider.html'
	list_display = ('checked', '_status', '_nom', 'societe', 'email', 'confirmed_email','adresse',
	 	'code_postal','ville','siege_adresse',
		 'siege_code_postal','siege_ville')
	list_display_links = ('_nom',)
	list_filter = (ValidationTypeListFilter,)
	search_fields = ('nom', 'prenom', 'code_postal', 'ville',)
	formfield_overrides = {
		models.FileField: {'widget': NewWindowAdminFileWidget}
	}

	def apply_validation(modeladmin, request, queryset):
		for agentAvalider in queryset:
			return redirect ('/admin/utilisateurs/agentavalider/'+ str(agentAvalider.id))

	apply_validation.short_description="Valider les agents sélectionnés"
	actions = [apply_validation, ]


	def get_fields(self, request, obj=None):
		return (
			('notification', 'confirmed_email', 'confirm_code'),
			('civilite', 'prenom', 'nom'),
			('societe', 'siren'),
			'mobile',
			'email',
			'type_inscription',
			('carte', 'attestation', 'qualite_colaboration') if obj is None or obj.is_attestation()
			else ('carte'),
			'adresse',
			('code_postal','ville'),
			'siege_adresse',
			('siege_code_postal', 'siege_ville'),
			('legal_representant_prenom', 'legal_representant_nom'),
			('other_legal_representant_prenom', 'other_legal_representant_nom'),
			('assurance_nom', 'assurance_nb'),
			'file_id1',
			'file_id2',
		)

	def _nom(self, obj):
		return u"%s %s" % (obj.prenom, obj.nom.upper())
	_nom.short_description = "Nom Complet"

	def _status(self, obj):
		return "Nouveau" if obj.is_new() else "Modifié"
	_status.short_description = "Status"

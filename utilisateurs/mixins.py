# -*- coding: utf-8 -*-

from django.contrib.auth.mixins import (
	LoginRequiredMixin, UserPassesTestMixin
)


class AgentTestMixin(LoginRequiredMixin, UserPassesTestMixin):
	def test_func(self):
		from utilisateurs.models import Agent
		try:
			self.agent = self.request.user.agent
			return True
		except Agent.DoesNotExist:
			return False


class UserIsClientMixin(LoginRequiredMixin, UserPassesTestMixin):
	def test_func(self):
		if hasattr(self.request.user, 'client') and self.request.user.client is not None:
			self.client = self.request.user.client
		return True

class FilterClientBiensMixin(object):
	def get_queryset(self):
		return super(FilterClientBiensMixin, self).get_queryset().filter(vendeur=self.client, deleted=None)

class FilterClientBienAValiderMixin(object):
	def get_queryset(self):
		return super(FilterClientBienAValiderMixin, self).get_queryset()

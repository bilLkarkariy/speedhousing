# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.gis.db import models
from django.contrib.gis.geos import Point

from geopy.geocoders import IGNFrance
from sh.settings import (
	IGN_API_KEY, IGN_USERNAME,
	IGN_PASSWORD, IGN_TIMEOUT
)

from django.contrib.gis.measure import D

class GeoLocalisable(models.Model):
	PARIS = 'POINT(2.294694 48.858093)'

	adresse = models.CharField(max_length=255)
	code_postal = models.CharField(max_length=5)
	ville = models.CharField(max_length=50)

	localisation = models.PointField(
		default=PARIS
	)

	geolocateur = IGNFrance(
		IGN_API_KEY,
		username=IGN_USERNAME,
		password=IGN_PASSWORD,
		timeout=IGN_TIMEOUT
	)

	objects = models.GeoManager()

	class Meta:
		abstract = True

	def get_adresse(self):
		adresse = None

		if self.code_postal in self.adresse:
			adresse = u"%s" % self.adresse

		else:
			adresse = u"%s %s %s" % (
				self.adresse,
				self.code_postal,
				self.ville
			)

		return adresse.encode('utf-8')

	def __unicode__(self):
		return self.get_adresse()

	def __init__(self, *args, **kwargs):
		super(GeoLocalisable, self).__init__(*args, **kwargs)
		self._current_address = self.get_adresse()

	def save(self, *args, **kwargs):

		if self.pk is None or \
		self._current_address != self.get_adresse():
			try:
				l = self.geolocateur.geocode(self.get_adresse())
				self.localisation = Point(l.longitude, l.latitude)
			except:
				pass

		return super(GeoLocalisable, self).save(*args, **kwargs)

	

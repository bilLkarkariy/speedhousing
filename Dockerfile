FROM ubuntu:18.04

ENV DEBIAN_FRONTEND=noninteractive

RUN apt -y upgrade
RUN apt update && apt -y install python
RUN apt -y install curl
RUN apt -y install libsqlite3-mod-spatialite
RUN apt -y install git
RUN apt -y install sudo


RUN curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py \
		&& python get-pip.py \
		&& pip install virtualenv


RUN ln -s /usr/bin/nodejs /usr/bin/node

RUN apt -y install libgdal-dev


COPY ./ /home/app/
WORKDIR /home/app/

COPY ./requirements.txt /home/app/requirements.txt
RUN pip install -r requirements.txt
RUN apt -y install npm
RUN apt -y install libcairo2-dev
RUN apt -y install libpango1.0-dev
RUN apt -y install python-requests

RUN npm install -g bower
RUN sudo python manage.py bower_install --allow-root
RUN python manage.py collectstatic --noinput
RUN find . -name '*pyc' -exec rm {} \;
RUN find . -path "*/migrations/*.py" -not -name "__init__.py" -delete
RUN python manage.py makemigrations
RUN python manage.py migrate
#RUN python manage.py flush

RUN python manage.py loaddata clean.json
#RUN python manage.py crontab add

CMD python manage.py runserver 0.0.0.0:8000

EXPOSE 8000

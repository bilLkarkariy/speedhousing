# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.staticfiles.finders import find

from django.contrib.staticfiles.templatetags.staticfiles import static
from easy_pdf.rendering import render_to_content_file
from django.core.files.storage import default_storage
from os.path import join

from django.shortcuts import render, redirect, get_object_or_404
from django.core.files.storage import FileSystemStorage
from django.http import JsonResponse, HttpResponse
from django.contrib import messages
from django.conf import settings
from django.db import IntegrityError
from datetime import *
from time import *
from django.views.generic.edit import (
	CreateView, DeleteView, FormView
)
from django.contrib.auth.mixins import (
	LoginRequiredMixin,
	UserPassesTestMixin
)

from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic import TemplateView
from django.utils import timezone
from extra_views import ModelFormSetView
from utilisateurs.models import Client
from biens.models import (
	Bien, Photo, Notaire, Offre, BienAValider, Vente, Mandat
)
from agenda.models import Disponibilite, RDV

from biens.forms import NotaireForm, PlusDeCritereForm

from django.urls import reverse, reverse_lazy

from django import forms

from easy_pdf.views import PDFTemplateResponseMixin, PDFTemplateView

from django.contrib.gis.db.models.functions import Distance
from django.db.models import Q

from biens import TYPES_BIENS, TYPES_DOCUMENTS
from sh.settings import DATETIME_INPUT_FORMATS

from sh.settings import ELEMENT_BY_PAGE

from biens.utils import add_logentry_bienavalider
from utilisateurs.utils import notify, notify_email

import logging, pyexcel, requests, json
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email.Utils import COMMASPACE, formatdate
from email import Encoders
import os
logger = logging.getLogger(__name__)


class UserIsAdminMixin(LoginRequiredMixin, UserPassesTestMixin):
	def test_func(self):
		return self.request.user.is_superuser

class BienAnnonceView(DetailView):
	model = Bien
	slug_field = 'nb_mandat'
	slug_url_kwarg = 'nb_mandat'
	queryset = Bien.objects.exclude(valide=None)



	def get_context_data(self, **kwargs):
		context = super(BienAnnonceView, self).get_context_data(**kwargs)
		if hasattr(self.request.user, 'client'):
			context['visit_planned'] = RDV.objects.filter(acheteur=self.request.user.client, bien=self.get_object(), visite=None, deleted=None)
		if hasattr(self.request.user, 'agent'):
			context['documents_types'] = TYPES_DOCUMENTS
			context["all_documents"] = context['object'].documents
			if RDV.objects.filter(agent=self.request.user.agent, bien=self.get_object(), deleted=None).exclude(visite=None).count() > 0:
				context['display_document'] = True
		return context


class BienAnnonceListView(ListView):
	model = Bien
	paginate_by = 6

	def get_context_data(self, **kwargs):
		context = super(BienAnnonceListView, self).get_context_data(**kwargs)
		context['TYPES_BIENS'] = dict(TYPES_BIENS)
		context['sorting'] = 'asc' if self.request.GET.get('sorting', 'desc') == 'desc' else 'desc'
		return context

	def get_queryset(self):
		from itertools import chain
		from datetime import datetime, timedelta
		date_del =  datetime.now()- timedelta(days=15)

		queryset_tmp = super(BienAnnonceListView, self).get_queryset().exclude(valide=None).exclude(offres__acceptee__isnull=False).filter(deleted__date__gt=date_del)
		queryset1 = super(BienAnnonceListView, self).get_queryset().exclude(valide=None).exclude(offres__acceptee__isnull=False).filter(deleted=None)

		queryset = queryset_tmp | queryset1
		# Localisation géographique
		localisation = Q()
		for loc in self.request.GET.getlist('localisation'):
			if loc:
				localisation = localisation | Q(ville__icontains=loc) | Q(code_postal=loc)
		queryset = queryset.filter(localisation)

		# Type
		type = Q()
		for t in self.request.GET.getlist('type'):
			if t:
				type = type | Q(type=t)
		queryset = queryset.filter(type)

		# Prix (min<max)
		try:
			min_prix = self.request.GET.get('min_prix', None)
			if min_prix: queryset = queryset.filter(prix__gte=int(min_prix))
		except ValueError as verr:
			pass

		try:
			max_prix = self.request.GET.get('max_prix', None)
			if max_prix: queryset = queryset.filter(prix__lte=int(max_prix))
		except ValueError as verr:
			pass

		# Surface minimum (m²)
		try:
			min_surface = self.request.GET.get('min_surface', None)
			if min_surface: queryset = queryset.filter(surface__gte=int(min_surface))
		except ValueError as verr:
			pass

		# Surface maximum (m²)
		try:
			max_surface = self.request.GET.get('max_surface', None)
			if max_surface:	queryset = queryset.filter(surface__lte=int(max_surface))
		except ValueError as verr:
			pass

		# Nombre de piece (1,2,3,4,5+)
		try:
			piece = self.request.GET.get('pieces', None)
			if piece and int(piece) <= 4:
				queryset = queryset.filter(pieces=int(piece))
			elif piece:
				# 5+
				queryset = queryset.filter(pieces__gte=5)
		except ValueError as verr:
			pass

		# Nombre de chambre (1,2,3,4,5+)
		try:
			chambres = self.request.GET.get('chambres', None)
			if chambres and int(chambres) <= 4:
				queryset = queryset.filter(chambres=int(chambres))
			elif chambres:
				# 5+
				queryset = queryset.filter(chambres__gte=5)
		except ValueError as verr:
			pass

		# Etage (min<max)
		try:
			min_etage = self.request.GET.get('min_etage', None)
			if min_etage: queryset = queryset.filter(etage__gte=int(min_etage))
		except ValueError as verr:
			pass

		try:
			max_etage = self.request.GET.get('max_etage', None)
			if max_etage: queryset = queryset.filter(etage__lte=int(max_etage))
		except ValueError as verr:
			pass




		# Surface annexes
		surface_terrasse = self.request.GET.get('surface_terrasse', None)
		if surface_terrasse: queryset = queryset.exclude(Q(surface_terrasse__isnull=True))
		surface_balcon = self.request.GET.get('surface_balcon', None)
		if surface_balcon: queryset = queryset.exclude(Q(surface_balcon__isnull=True))
		surface_jardin = self.request.GET.get('surface_jardin', None)
		if surface_jardin: queryset = queryset.exclude(Q(surface_jardin__isnull=True))

		# Type de Cuisine
		cuisine_americaine = self.request.GET.get('cuisine_americaine', None)
		if cuisine_americaine: queryset = queryset.filter(cuisine_americaine=True)
		cuisine_separee = self.request.GET.get('cuisine_separee', None)
		if cuisine_separee: queryset = queryset.filter(cuisine_separee=True)

		# Comodités
		gardien = self.request.GET.get('gardien', None)
		if gardien: queryset = queryset.filter(gardien=True)
		digicode = self.request.GET.get('digicode', None)
		if digicode: queryset = queryset.filter(digicode=True)
		interphone = self.request.GET.get('interphone', None)
		if interphone: queryset = queryset.filter(interphone=True)
		cave = self.request.GET.get('cave', None)
		if cave: queryset = queryset.filter(cave=True)
		parking = self.request.GET.get('parking', None)
		if parking: queryset = queryset.filter(parking=True)
		refait_a_neuf = self.request.GET.get('refait_a_neuf', None)
		if refait_a_neuf: queryset = queryset.filter(refait_a_neuf=True)
		a_renover = self.request.GET.get('a_renover', None)
		if a_renover: queryset = queryset.filter(a_renover=True)
		piscine = self.request.GET.get('piscine', None)
		if piscine: queryset = queryset.filter(piscine=True)
		belle_vue = self.request.GET.get('belle_vue', None)
		if belle_vue: queryset = queryset.filter(belle_vue=True)
		ascenseur = self.request.GET.get('ascenseur', None)
		if ascenseur: queryset = queryset.filter(ascenseur=True)
		dernier_etage = self.request.GET.get('dernier_etage', None)
		if dernier_etage: queryset = queryset.filter(dernier_etage=True)
		sans_vis_a_vis = self.request.GET.get('sans_vis_a_vis', None)
		if sans_vis_a_vis: queryset = queryset.filter(sans_vis_a_vis=True)
		alarme = self.request.GET.get('alarme', None)
		if alarme: queryset = queryset.filter(alarme=True)
		access_handicape = self.request.GET.get('access_handicape', None)
		if access_handicape: queryset = queryset.filter(access_handicape=True)
		cheminee = self.request.GET.get('cheminee', None)
		if cheminee: queryset = queryset.filter(cheminee=True)
		box = self.request.GET.get('box', None)
		if box: queryset = queryset.filter(box=True)
		rangements = self.request.GET.get('rangements', None)
		if rangements: queryset = queryset.filter(rangements=True)
		dressing = self.request.GET.get('dressing', None)
		if dressing: queryset = queryset.filter(dressing=True)
		placards = self.request.GET.get('placards', None)
		if placards: queryset = queryset.filter(placards=True)

		# Seulement avec photo
		with_photo = self.request.GET.get('with_photo', None)
		if with_photo:
			all_photo_id_with_bien = Photo.objects.exclude(bien=None).values_list('bien_id', flat=True)
			queryset = queryset.filter(id__in=all_photo_id_with_bien)

		order_by = self.request.GET.get('order_by', None)
		sorting = '-' if self.request.GET.get('sorting', 'desc') == 'desc' else ''
		if order_by and order_by == 'date':
			# let the possibility to sort by any field
			#queryset.order_by(sorting + order_by)
			queryset = queryset.order_by(sorting + 'modification', sorting + 'creation')
		else:
			queryset = queryset.order_by(sorting + 'modification', sorting + 'creation')


		return queryset

def recherche_plus(request):
	return render(request, 'biens/bien_recherche_plus.html', {"form": PlusDeCritereForm(request.GET or None)})

class BienWizardMixin(LoginRequiredMixin, UserPassesTestMixin):


	def test_func(self):
		self.nb_mandat = self.kwargs.get('nb_mandat', self.request.session.get('nb_mandat', None))
		self.bien = Bien.objects.get(nb_mandat=self.nb_mandat)
		return True

	def end_session(self):
		del self.request.session['nb_mandat']

	def get_context_data(self, *args, **kwargs):
		context = super(BienWizardMixin, self).get_context_data(*args, **kwargs)
		context['bien'] = self.bien
		return context

	def form_valid(self, form):
		if isinstance(self.bien, Bien):
			form.instance.bien_a_valider = self.bien
		else:
			form.instance.bien = self.bien
		return super(BienWizardMixin, self).form_valid(form)


class BienAValiderWizardMixin(LoginRequiredMixin, UserPassesTestMixin):


	def test_func(self):
		self.nb_mandat = self.kwargs.get('nb_mandat', self.request.session.get('nb_mandat', None))
		self.bien = BienAValider.objects.get(nb_mandat=self.nb_mandat)
		return True

	def end_session(self):
		del self.request.session['nb_mandat']

	def get_context_data(self, *args, **kwargs):
		context = super(BienAValiderWizardMixin, self).get_context_data(*args, **kwargs)
		context['bien'] = self.bien
		return context

	def form_valid(self, form):
		if isinstance(self.bien, BienAValider):
			form.instance.bien_a_valider = self.bien
		else:
			form.instance.bien = self.bien
		return super(BienAValiderWizardMixin, self).form_valid(form)

class PhotoDeleteView(BienAValiderWizardMixin, DeleteView):
	model = Photo

	def get_success_url(self, *args, **kwargs):
		# Check if it's an update info (not the same url)
		bien_a_valider = self.request.POST.get('update_info', None)

		if bien_a_valider is not None:
			# It is
			add_logentry_bienavalider(self.request.user.id, self.bien, [u"photos"])
			return reverse_lazy('utilisateurs:mes-annonces-a-valider-modifier-infos', args=[self.nb_mandat])
		# It's not (bien creation)
		return reverse_lazy('infos')

class PhotoCreateView(BienAValiderWizardMixin, CreateView):
	model = Photo
	fields = ['image']
	success_url = reverse_lazy('photos')

	# def get_success_url(self, *args, **kwargs):
	# 	if self.request.POST.get('add'):
	# 		return reverse_lazy('photos')
	# 	return reverse_lazy('disponibilites')


class DisponibiliteDeleteView(BienAValiderWizardMixin, DeleteView):
	model = Disponibilite

	def get_success_url(self, *args, **kwargs):
		# Check if it's an update info (not the same url)
		bien_a_valider = self.request.POST.get('update_info', None)
		if bien_a_valider is not None:
			# It is
			add_logentry_bienavalider(self.request.user.id, self.bien, [u"disponibilites"])
			return reverse_lazy('utilisateurs:mes-annonces-modifier-infos', args=[self.nb_mandat])
		# It's not (bien creation)
		return reverse_lazy('infos')

class DisponibiliteBienDeleteView(BienWizardMixin, DeleteView):
	model = Disponibilite

	def get_success_url(self, *args, **kwargs):
		# Check if it's an update info (not the same url)
		bien_a_valider = self.request.POST.get('update_info', None)
		if bien_a_valider is not None:
			# It is
			add_logentry_bienavalider(self.request.user.id, self.bien, [u"disponibilites"])
			return reverse_lazy('utilisateurs:mes-annonces-dispos-modifier', args=[self.nb_mandat])
		# It's not (bien creation)
		return reverse_lazy('infos')

class DisponibiliteCreateView(BienAValiderWizardMixin, CreateView):
	model = Disponibilite
	fields = ['jour', 'debut', 'fin']

	def get_success_url(self, *args, **kwargs):
		if self.request.POST.get('add'):
			return reverse_lazy('disponibilites')
		return reverse_lazy('notaire')

	# def get_form(self, *args, **kwargs):
	# 	form = super(DisponibiliteCreateView,
	# 		self).get_form(*args, **kwargs)
	# 	debut_dispo = forms.Select(
	# 		choices=[(None, u"Début")] + [
	# 			(
	# 				"%d:%s" % (h, m),
	# 				"%d:%s" % (h, m)
	# 			)
	# 			for h in range(7, 22)
	# 			for m in '00', '30'
	# 		]
	# 	)
	# 	fin_dispo = forms.Select(
	# 		choices=[(None, u"Fin")] + [
	# 			(
	# 				"%d:%s" % (h, m),
	# 				"%d:%s" % (h, m)
	# 			)
	# 			for h in range(7, 22)
	# 			for m in '00', '30'
	# 		]
	# 	)
	# 	form.fields['debut'].widget = debut_dispo
	# 	form.fields['fin'].widget = fin_dispo
	# 	return form


class NotaireFormView(BienAValiderWizardMixin, FormView):
	form_class = NotaireForm
	template_name = "biens/notaire_form.html"
	success_url = reverse_lazy('notaire')


	def clean(self):
		cleaned_data = super(NotaireFormView, self).clean()
		notaire = cleaned_data.get("notaire")

		if notaire is None:
			msg = "Le choix du notaire est nécessaire."
			self.add_error('notaire', msg)

		return cleaned_data


	def form_valid(self, form):
		notaire_id = form.cleaned_data.get('notaire', None)
		notaire = get_object_or_404(Notaire, pk=notaire_id)

		self.bien.notaire = notaire
		self.bien.save()

		messages.info(self.request, "Votre annonce a bien été deposée.")
		self.end_session()

		super(FormView, self).form_valid(form)
		return redirect('home')


	def get_form_kwargs(self):
		kwargs = super(BienAValiderWizardMixin, self).get_form_kwargs()
		kwargs['bien'] = self.bien
		kwargs['client'] = self.request.user.client
		return kwargs

	def get(self, request, *args, **kwargs):
		if request.is_ajax():
			q = request.GET.get('q', None)
			form = self.form_class(**self.get_form_kwargs())
			return JsonResponse(
				[{
					'id': n.pk,
					'name': n.get_nom(),
					'label': form.get_labels(n)
				} for n in form.get_notaires_queryset(query=q)],
				safe=False
			)
		return super(NotaireFormView, self).get(
			request, *args, **kwargs)


# Create a BienAValider instead of a Bien
class BienCreateView(LoginRequiredMixin, CreateView):
	login_url = reverse_lazy('utilisateurs:inscription-client')
	template_name = "biens/bien_form.html"
	model = BienAValider
	fields = ['type', 'energie', 'prix', 'sdb', 'exposition',
			'chauffage', 'type_chauffage', 'sde', 'construction',
			'surface_terrasse', 'surface_balcon', 'surface_jardin',
			'cuisine_separee', 'cuisine_americaine', 'cuisine_equipee',
			'lot', 'charges','taxe','surface', 'pieces', 'chambres',
			'etage', 'adresse', 'code_postal',
			'gardien', 'digicode', 'interphone', 'cave',
			'parking', 'refait_a_neuf', 'a_renover', 'piscine',
			'belle_vue', 'ascenseur', 'dernier_etage',
			'sans_vis_a_vis', 'alarme', 'access_handicape',
			'cheminee', 'box', 'rangements', 'dressing',
			'placards', 'ville', 'description', 'transports',
			'commerces', 'ecoles', 'loisirs']



	def get_success_url(self):
		self.request.session['nb_mandat'] = self.object.nb_mandat

		return reverse_lazy('infos')

	def form_valid(self, form):
		try:
			form.instance.vendeur = self.request.user.client
			form.instance.prix = int(form.instance.prix * 1.03 + 0.5)

		except Client.DoesNotExist:
			messages.error(self.request,
				u"Oups ! il semblerait que vous n'aillez "\
				u"pas le droit de faire cela !"
			)
			return redirect('home')



		return super(BienCreateView, self).form_valid(form)


class BienRelatedDetailView(NotaireFormView):
	template_name = "biens/related_forms.html"
	dispo_form = None
	photo_form = None
	notaire_form = None




	def get_photo_form(self):
		return PhotoCreateView(request=self.request).get_form()

	def get_dispo_form(self):
		return DisponibiliteCreateView(request=self.request).get_form()

	def get_context_data(self, *args, **kwargs):
		context = super(BienRelatedDetailView,
			self).get_context_data(*args, **kwargs)

		context['dispo_form'] = self.dispo_form or self.get_dispo_form()
		context['photo_form'] = self.photo_form or self.get_photo_form()
		context['disponibilites'] = self.bien.disponibilites_a_valider
		context['photos'] = self.bien.photos_a_valider
		context['object'] = self.bien

		return context

	def post(self, request, *args, **kwargs):
		self.bien.delete_on =  datetime.now() + timedelta(days=365)
		self.bien.save()

		if request.POST.get('add_disponibilite'):
			self.dispo_form = self.get_dispo_form()
			if self.dispo_form.is_valid():
				self.dispo_form.instance.bien_a_valider = self.bien
				self.dispo_form.save()
			else:
				return self.get(request, *args, **kwargs)
		elif request.POST.get('add_photo'):
			self.photo_form = self.get_photo_form()
			if self.photo_form.is_valid():
				self.photo_form.instance.bien_a_valider = self.bien
				self.photo_form.save()
			else:
				return self.get(request, *args, **kwargs)

		elif request.POST.get('set_notaire'):
			notaire_id = request.POST.get('set_notaire')
			notaire = get_object_or_404(Notaire, pk=notaire_id)
			self.bien.notaire = notaire
			self.bien.save()
		else:
			valid_notaire = self.bien.notaire is not None
			valid_dispos = self.bien.disponibilites_a_valider.exists()

			if not valid_notaire:
				messages.error(self.request, "Le choix d'un notaire est requis.")
			if not valid_dispos:
				messages.error(self.request, "Vous n'avez indiqué aucune disponibilité.")

			if valid_notaire and valid_dispos:

				vendeur = self.bien.vendeur
				notify_email(vendeur.user.email, u"Dépôt d'annonce : attente de vérification", u"email/vendeur/confirmation_annonce.html", {
					u"fullname": vendeur.get_full_name(),
					u"annonce": self.bien
				})


				self.bien.checked = False
				self.bien.save()

				return redirect(reverse('validation'))

		return redirect(reverse_lazy('infos'))

class BienAValiderValidateView(BienAValiderWizardMixin, TemplateView):

	template_name = "biens/validation_modification_form.html"

	def get_bien(self):
		return self.bien

	def get_context_data(self, *args, **kwargs):
		context = super(BienAValiderValidateView,
			self).get_context_data(*args, **kwargs)
		context['update_info'] = True
		context['nb_mandat'] = self.bien.nb_mandat
		context['bien'] = self.bien
		context['photos'] = self.bien.photos_a_valider
		return context

	def post(self, request, *args, **kwargs):
		next_page = request.POST.get('next', None)
		if next_page:
			messages.info(self.request, "Votre annonce à bien été mise à jours. "\
			"Celle-ci est desormais en attente de validation.")

			notify_email(self.bien.vendeur.user.email, u"Mise à jour d'annonce : vérification de vos modifications", u"email/vendeur/confirmation_annonce_modifiee.html", {
				u"fullname": self.bien.vendeur.get_full_name(),
				u"annonce": self.bien
			})
			return redirect(reverse_lazy('utilisateurs:mes-annonces'))
		elif 'prev' in request.POST:
			return redirect(reverse_lazy('infos'))
		return redirect(reverse_lazy('home'))


class BienValidateView(BienAValiderWizardMixin, TemplateView):

	template_name = "biens/validation_form.html"

	def post(self, request, *args, **kwargs):
		next_page = request.POST.get('next', None)
		if next_page:
			messages.info(self.request, "Signez pour déposer votre annonce. "\
			"Celle-ci est desormais en attente de validation.")
			return redirect(reverse_lazy('signature'))
		elif 'prev' in request.POST:
			return redirect(reverse_lazy('infos'))
		return redirect(reverse_lazy('home'))

class BienSignatureValideView(BienAValiderWizardMixin, TemplateView):
	template_name = "biens/bien_sign.html"

	def get_context_data(self, *args, **kwargs):
		context = super(BienSignatureValideView,
			self).get_context_data(*args, **kwargs)
		context['bien'] = self.bien
		return context

	def get(self, request, *args, **kwargs):
		self.bien.signature = True
		self.bien.save()
		messages.info(self.request, "Votre annonce a bien été signée, celle-ci est en cours de validation.")

		return redirect(reverse('utilisateurs:mes-annonces'))



class BienSignatureView(BienAValiderWizardMixin, TemplateView):
	template_name = "biens/bien_sign.html"
	procedure_id ="";

	def yousign_pdf(self):
		template_mandat_pdf = "biens/pdf/sign_mandat.html"
		file_mandat_pdf = render_to_content_file(template_mandat_pdf, {u"bien": self.bien})

		# save files
		upload_directory = getattr(settings, "MEDIA_ROOT", '/')
		path_mandat = default_storage.save('documents/biens/%s/sign_mandat.pdf' % self.bien.nb_mandat, file_mandat_pdf)
		file_mandat = upload_directory + '/' + path_mandat
		#file_mandat = upload_directory + '/' +"documents/biens/1/pdf-test.pdf" #+ path_mandat

		part = MIMEBase('application', "pdf")
		with open(file_mandat, 'rb') as file:
			part.set_payload(file.read())
		Encoders.encode_base64(part)
		part.add_header('Content-Disposition',
                        'attachment; filename="{}"'.format(os.path.basename(file_mandat)))
		base642=str(part)
		base64 =  base642.split('.pdf\"')[1].replace('\n\n','').replace('\n','')
		return base64

	def yousign_logo(self):
		import base64
		encoded_string = ""
		format = "png"
		logo_file = static('img/logo_pdf.png')
		with open(logo_file,'rb') as img_f:
			encoded_string=base64.b64encode(img_f.read())
		return 'data:image/%s;base64,%s' % (format, encoded_string)



	def get_mandat(self):
		return self.bien.nb_mandat;
	def get_bien(self):
		return self.bien
	def get_context_data(self, *args, **kwargs):
		context = super(BienSignatureView,
			self).get_context_data(*args, **kwargs)
		context['bien'] = self.bien

		#create procedure
		body_procedure = {
		  "name" : "Procedure",
		  "description" : "New test yosuign",
		  "start" : False
		}
		#Post procedure
		r = requests.post('https://api.yousign.com/procedures',json=body_procedure, headers=settings.HEADER_AUTH)

		#Get procedure_id
		json_data = json.loads(r.content)
		self.procedure_id = json_data['id']

		#post file assign to procedure
		mandat_sign = "Mandat signature" + str(self.bien.nb_mandat)
		body_files = {
		  "name" : mandat_sign,
		  "content" : self.yousign_pdf(),
		  "procedure" : self.procedure_id
		}
		r = requests.post('https://api.yousign.com/files',json=body_files, headers=settings.HEADER_AUTH)


		file_id = json.loads(r.content)['id']
		mobile = "+33"+self.bien.vendeur.mobile[1:]
		body_members = {
		  "firstname" : self.bien.vendeur.prenom,
		  "lastname" : self.bien.vendeur.nom,
		  "email" : self.bien.vendeur.email,
		  "phone" : mobile,
		  "procedure" : self.procedure_id
		}

		r = requests.post('https://api.yousign.com/members',json=body_members, headers=settings.HEADER_AUTH)
		member_id = json.loads(r.content)['id']
		signedBy = "Signé par "+ self.bien.vendeur.prenom+" " +self.bien.vendeur.nom

		context['member_id'] = member_id


		body_object = {
			"file" : file_id,
			"member" : member_id,
			"position" : "450,205,549,245",
			"page" : 6,
			"mention" : "Lu et approuvé"
		}
		r = requests.post('https://api.yousign.com/file_objects',json=body_object, headers=settings.HEADER_AUTH)

		SWAGGER_UI = {
		        "name": "string",
		        "description": "string",
		        "defaultZoom": 100,
		        "logo": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQUAAAEFCAYAAADqlvKRAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAEtdJREFUeNrsnd1x2zoWgEGO36NbwWUqsFJB5NkCrFQQeR/vS+wKYleg+GUfr+UK7BRwx3IFUSoI3YG2gl0iOoppR5b5A4D4+b4ZjpxMYkkg+PGcAxBQCgAAAAAAoBEZTRA/f/3z96T2x1F1jFv+inV1rOp//s+//r2iZZEC+HnBj+VC31747+V1+/e2KeXQ4vheE0hZiaPkDCEFsHfxF3Khj+XCL+TwnaVI40F+XlWyWHNGkQK0E8A2xJ+IAFzd9V1RSjRxL5JYctaRAvwuga0AJh1y/hhYiiRuqVcghZRrAVoAx7V6AGxYiyS+iiRIN5BC1CL4WB3TQOoBvqAjh2sRRElzIAVEAAgCKQQvgkIk8AkRWOVWpxiVHBY0BVLwVQbTWlQA7liLIC4pUiIFH0SgRw5ORQZEBcOz1OkF0QNSGCpF+FwdM1rDS0q1qT18YfQCKdiWwURkMKE1gkktFpJalDQHUkAGUEfL4QI5IAVkAMgBKRiRgZ5fMEcGUXOhqDkghQYyKBQFxJTQQtD1hnOaAik8l8F2aFFPOBrRIsmhU4mzSg63SAG2k450qlDQGsmzrI6TlOsNWeIy0BK4om4AO0i23pAlLIRzUgVokFKcpLYITJagDMYSHYzp89CQL2ozhLlGCnFGB5/p40DUkLgUpHZwQ3QARA1IQQthpjYjC9QOwBQriRpWSCEsGYxEBjP6MFhARwpnMT6inUUqBIqJ4IqFyGGNFEgXAKJMJ7LIhKBlcEofhYHSiZMYpklnkchARwV6dGFC34SBuQj94aosAiFQPwDfWKiA6wxZBEK4o34AHqLrC0chiiEPWAiz6uUbQgBP+XnDkhsXkYIjIVzR7yAA1hIxBDMykQcohDlCgIAYhRYx5IEJQcuAIUcIUQzfJMIlfTAshBn9CwLnxPep0RlCAEAMQUkBIQBiQAoIARADUkAIkCwffHteIvNUCDzYBKng3TyGzEMh6OiAeQiAGJACQoDkxfDWh2clMo+EwMNNkDpePESVeyKEAiEA/HyI6mboD5F7IITtAikIAUCpiYy8JR0psEAKwFNmQz4nMWhNgR2bAPbybogRiWxAIUx9yJ8APGaQEYl8ICEUiqFHgNcYDXHjzAcQAoVFgOZMJM2OOlLQNQQKiwAtrplKDBNXb+a0pkAdAaAzzuoLuUMhjKgjAHTGWX3BZfpAHQGgH7q+YP3pYSfpg3yROecUwEgaoecvlMFGCjL8yAQlAHNphNU03EX6cEXaABBOGmE1fYg4bVhJGLdljPg4T7GkEQcWhTCKLG3QHeyyOm53DQvJehAf1WZdSQQxHMvquN5znvSw+LEKf/3PbRpxFEykENHCq7pjnTRdXFNkOFcsOuv7eSrkopoE/r2NL/yaWRKCbui7SKKDTivhyKOvc6IGZ9HBh47n6TzwiLaUNMLYpCZbhcZ5ykLQyHr+R89yWjDPomrrPudJS+Ek4O+vIx6jRUfjkUIkxUVja+WxGK3d81Sdo3eG+m3oEcNbU0XH3LAQYigubnNTI3d4iRi+cP1aOU8fTP0yiRiWAbeHsRux6fThNIIc+tLCajcXpBHGubAwHBdyGjE19SSlMSlEMnNxbeOuLlHHBdexufNUtamN86Qlswi4XT57JQUVx5yEhcVHUxdcy0G05deA22Ui8zCGl4JECbMIOpu1DiGyueV69v48hX6O5l5IIZIoQXeIpeW3+M71bATbKxwvA26bou/y8L2lEFGU4GIp7SXXs7GoCyzdpPOhP4BH0NEgFnpFC72kEFGUAEC0YChSYPEUAH+jhYlTKcjsRaIEgMiihT6RwiltDuA1E1nnw5kUPtHmAN7T+jrtJAWpbLJOAID/zCTVtx4pECUABCQGq1KQHIW9IAEiTSFy228AAINTtHlQqosUprQxQHAcW5ECBUaAYGlccGwbKRzTtgDB0ijKbywFec6B1AEgXBrVA9tECggBIGzGcnM3JoWPtClA/ClEIymIXZibABA+r97cm0YKpA5mYCEX8D6FaCqF97RlfyzsJwFgPIV4VQoytkmkABAP7/tGChPa0ChECxB2pKCYsIQUIDr2PQtBpOCee5qgd4dmqr3FFCJ/pfH1MGSRSCM56WiyCzWjEP1geLw/k66RwoSOZgU2m+0HKa2B/v7S0ORrUmAo0k60oHdMXtASnZk1ma4L3aIFIoXf0yVXYjhBDL1SvRtqC705bCUFMXFqje707iNiOKPG0Dnd+9Z3M1UihXaRwiTBRnKeLkkq8a46viCHThK/qsTwozpOSSk61RVGbaRwiDmdiaGsjrPq+KP64wdJK5jP0E4O8+rQcrjpul1awhHXE7I96cNdotHCHz5tdS4dfFQ7ee+fnVDy6t2U1XEhQ8Bt2zylvq/b6Lz+FwekD78xVR4VAKsTtpQfb1tIpM5ox93gT/V7/SQ2wWxTi89d5ZAIv2UEBy90rJQnh3xSAY8K1CRS57bL75J+MGogj8Mdf1coPya+beWg1xE40akaHuiQPkhF9yrhhjp64eKCfqlQXRS7UiLb0alOC89eixpSS52r9siapA9F4v1Xh5xIwXznKyXXfzGCqa3yNRZZmLw4RxI1vJfhYJCUs34TfEkKqc9k1Ft4T6uGuqXLDCaO23qnVZupzVNDNyw9I1Ihht1BQE6k8CJXzJjzRhRLGbJ9qx7ndJQGxHBF6yKF1qEmzeCdIFY1QXzomeYhhh2ZQb4jv+Cx1EemdBqvBXFbHUfVj0c95LBLDKldA08i4mxX0aF6uaPLPeFMpiODx0jfveoY6S7U5jmUaYoRYn0EYpcUTtVmyig86zQUpoKRw7najCBBc37N5M1fCyXgSZh5R/ExiLueloIuSPL8SHPGL9YU1GYKLOxGh6c/9i16Cd6IYSW1hgWt0Y5dUiholr1sF/i44VFd78WwlpSP5e+a3fBelAI0YypRwxVyCCKdoB7UI1KY0Cztag3q8Tl+0gp/xbBADHt5Q6RgJ3K4kVWA5sz38FYMpBK7+dVfdw1J/o/2MUapNpNqvupXnxZvSRmZrDSjJZ6wlIlgTx+IIjc2TiGdbybtqyvi+viuX3k8ezDO1OOTmPCMgx2dGOyGaOOahJVIohRRlHKsiCqsphHrqu11feEbrfGL0c70gSnO/oV08qrF8d+aNBTiMJJGnCtmPtZlmSGFeNDSqAvi+Sa2dZk8+X+pi0UXhomQn0rhgKaIJi2pM2lxUbz2T14SSlvWkiKtRWK+CEnXF27oQo8QKcDQEY5Oka5lWvJQ0UKq2xnsjBSYpwBDRzj6qVy9/duQW8Axd6EGUgCfBHElcnA6VChDwyWnACmAv3L4Jut6EC0gBYBfzB0vhadXj2aIFymA5zhbWFVGQljSHylAIGJwlUp8pbmRAoSTSlgvPrL5D1KAwMTg6H2SFwNSgFCYyOQ629wn2r7lS1Kg+go+4+LhpVRXgN4thSGnmgI0jBYKm2/AGhekDxAen4gWkALAk2gBKThMH7AkBMDYwS5dDwm268M+KVBshNSjhSXpA0Bg0QJNYC9l2iWFe9oHPOfQ5i9PdARiTaQAIcPO3+Yp90lhSfsA6UNaVNFRSaQARAr7SenmWNb/kJNPAZA67JWCwLAkQDqsmkiBCUwA6fCAFACgdaTwQDsBIAUiBYD0WD/fvm+nFBiBAN9x8FBUklHCvkiBaAG8JvXdsg1yjxQAoHOk8J32Ak8pHbxHkaoUDvb842VCHay03LkKrmOk4GM71p95eFUKehHXv/75O4UOdl1913Nbv7xqQ/27P3MdByeFJKOE19KHlKIF5w0PvbCa2tpeMdoj7rtIgQVX+kOVPDzRpiKFJZHCbljFJzActGkKUli/tM9L3qDxY7/TuZgEQw5sDhd7PRYpt2OTRVZiv9O56ABEC+ZwsV38YQLteN9HCrHXFYohTwAQKfhUT2gqhei35v7rn7/HEXTkFFg4mt4c+xqQq13zExpLQf5z7MNqVu8M0pEXXNO9uXBwg5iknoI1Xbg19pzYxZ3hmmu6d5RQEiXYj1xzOvRP3tt+AxnJWXJtd0JHWmex9IWBKV8aimwlBfklZcQN5SpkPFFMZurUbg4flZ6mHCW0iRQa/bKQcZFLSvh7xjXeWgi3jvrANIH2vDYphdhTiGMXb1J18IVEDNBMCIvY+oDPqUMrKSSQQji7SyCGRjWEI8dCIHXoECloLiNusMLBfIXnYjhSTIF+zpfqeOv6mZHq3M9U/BvXXtqQQuyTcD66fDPp+O/UZvw95QJkKW3wR9UmZwOtv/gx8jZeNR3SzToY9SbiMGstd6lBLlC5Wx0nEMb+7KRqM0R73STPtdzuOkL8Fnl7N67PHHT45dcRd1odPs4khHWOnLSFLF+u21iPmY9V+BNq1iKB+60MPFuN+VPkQli3ifKzLu9QddofKt6HRnSI9c63JcRlyLSQYzvBZuLRR1zW2u9BPa59ufJ5OfZEogQ9G7RxYfug45voaCHWdQf1RXdaHec+fah9hTeJLMbPvoNpaW/v9o0+U0DME0jVWj0z0jVS0B3uR+Th1jtHc+1huChBp2g3kX9NnaodtfkPeZd3kYtlEXFD6jvvFZdN1EJI5Ry3nnSY93izy8gbc1J1nFMun2i5UvHPSyi7TADrLAUZRlpG3qhzlxOawFmUcK7SGPbttP5EPsSbBsYdYohKCDOVxuY8rYYhjUkhkTUCRoghKiGkUiu67DoUnBt48xRWFEIMCCG0KKHzBLzeUpBCRpmQGKZcYkHWEFIaTbrsM2EsN/QhUlk4RIvhpupkcy61IGQwkmd1Utrgt1eUoMkMnoA75de0W9vo0ZcztoXzOl2Yq/iHHZ9z0XcX9QOTHyYxKYwlnVjIiSi5FL2QwUQig0mCX79UBh7mywyfkLtET4YGOSCDoTGyfJ1pKRQq7mcimqDHhq9dLTaaes1AbSYhfVJp7NewN0qo+txbE78os3CidJV3RpdV212hvlJ3MN7HtAiO6WdPODLVz2xIYSTRwojz9EQQOnLQi4wsSTE6RaATlc6qVG1p/SSkUynISdQPEjFstyfUU5uZoN/lhK5okif9ZywSOJTXglbZy1uTN5rM4on9Rp7XipUcDyKMdeyykAigkH5yKD9P6Aqt6D0E6VIK+uTecc6MRBXb42EbLm7TEp/FUdt1a7sy1Bt5HXHDMNY3jC8dmFnuFDqFYE0Ch7nljk7z0FA6uyheCd3f7Li4udO748hGEfvA8ofWE5qm5ITO4IJMh4WtUa3c5qeWsIbt0QDMoq8ra88b5bY/vdjsC+cRwBgnNpfNzx19iQvFnokAptIGq7NlnUiBNALACKVysEyBq0iBNALA87RhS+b6WzGpCaBbCm56ktLgkUKNDyrtbdcB2rJ0JYRBpCBztKkvADRjLTdSZwwRKSipnlJfAGgQWbvetTsb8tsmvlITwGvoNUCd3zzzgb+0DotKzj3AbyyGEMLgUpCwiMIjwFN+rhQ+1JtnPrRAYrv3AOxD3yDfuq4j+JQ+bCOGhUpnQxmAfUI4GlII3khBxKDzpwX9AhLmxIdFc3KfWqRqkBPEAAkLwYttAXIPG0enESxkCilxZmITF1NkPraQLBOv5zDwjATEzkIiZIUUEAOAd0LwWgqIARACUkAMgBCQAmIAhIAUEAMgBIUUEANAEELQ5CG1qkz/1LvrLuljEBAnoQghuEjhWdSgH6Ca0d8gACEsQvrAWcitzV6V4DE/lwWwtbUbUtgvBh0t8Ng1+EQpQghyun4WwxmoxKALj7oAOaI/wsBoEQz++HPyUhAxFNXLjWJkAoZjEVJB8SXyWM6GLB2vRyYW9E0YgJMYhBBVpPAsatDFxzn9FKgfIIW6GMaSThT0W7DErXK0vyNSMCcGXXjUIxNT+i8Y5myoJdiRghk5zCSdYHQC+rJSnqylaIs8hbMoM8reKaZHQz90ZHAUsxCSiRSeRQ26CPmZqAFaUEp0kMRNJUvxDMucBl1rmNDfoUF0cBFbMREpEDUAtQOk0EIMWgi6CDnjOgC1eZDpItaRBaTQTg4TkQPTpNNloTZDjUlveIwUfpfDTFKKgtZIhqVEB0uaAinsSyl0veGTot4QM6VEBrc0BVJADsjgIrQVkZCCX3IoJKWYIgdkgBSAyAEZIAVoLIePioKkzyyr4xoZIAXXgphJ5MBQpj8sRAZLmgIpDCmHsciBusNwKcJ1dXxJfZ4BUvAztZgSPTiNCr4yrIgUQhFEoTbTp6k9WKgVVMctUQFSCD29+ChRBILoJoKvIoKS5kAKMQpiIpIgxdjN+pkIiAiQQjKCGIkgjuU15ShiJRJYMnKAFOBREoXI4VBeY44k9IV/L68rogGkAM1FsZXDobyGJoq1RAH6+C4CWHFmkQKYFcVYUg39+qf8XAycfqxEAPc1EZQUBpECDC+MUS2a0K/byVRvekQZa7nLbynlUOT/AAAAAKnwfwEGADXIwU3i6foIAAAAAElFTkSuQmCC",
		        "languages": [
		          "fr",
		          "en"
		        ],
		        "defaultLanguage": "fr",
		        "signImageTypesAvailable": [
		          "name",
		          "draw"
		        ],
		        "enableHeaderBar": True,
		        "enableHeaderBarSignAs": True,
		        "enableSidebar": True,
		        "enableMemberList": True,
		        "enableDocumentList": True,
		        "enableDocumentDownload": True,
		        "enableActivities": True,
		        "authenticationPopup": False,
		        "labels": [
		          {
		            "name": "NAME OF THE LABEL",
		            "languages": {
		              "en": "Label en",
		              "fr": "Label fr"
		            }
		          }
		        ],
		        "fonts": ["Roboto", "Lato"],
		        "style": "Just a CSS string for customize all of our iframe.",
				"redirectSuccess": {
			     	"url": "https://www.speedhousing.fr/biens/nouveau/signature-valide/"+str(self.bien.nb_mandat),
			     	"target": "_top",
			     	"auto": True
			    }


		      }

		r = requests.post('https://api.yousign.com/signature_uis',json=SWAGGER_UI , headers=settings.HEADER_AUTH)

		swagger_id = json.loads(r.content)['id']
		context['swagger_id'] = swagger_id

		body_config = {
			"start": True,

		}

		r = requests.put('https://api.yousign.com'+self.procedure_id,json=body_config, headers=settings.HEADER_AUTH)
		return context


	def post(self, request, *args, **kwargs):


		next_page = request.POST.get('next', None)
		if next_page:




			#r = requests.put('https://staging-api.yousign.com'+procedure_id',json=body_config, headers=headers)

			return redirect(reverse('utilisateurs:mes-annonces'))
			# return redirect(reverse('bien-sign-mandat', args=[self.bien.nb_mandat]))
		return redirect(reverse_lazy('home'))


# https://django-easy-pdf.readthedocs.io/en/v0.2.0-dev1/usage.html

class OffreAccepteePDFView(LoginRequiredMixin, PDFTemplateResponseMixin, DetailView):
	model = Offre
	template_name = "biens/pdf/offre_acceptee.html"

	def get_queryset(self):
		qs = super(OffreAccepteePDFView, self).get_queryset()

		if self.request.user.is_superuser:
			return qs
		if hasattr(self.request.user, 'agent') and self.request.user.agent is not None:
			return qs.filter(acceptee__isnull=False, agent=self.request.user.agent)
		return qs.filter(acceptee__isnull=False, vendeur=self.request.user.client)


	def get_context_data(self, *args, **kwargs):
		offre = self.get_object()
		context = super(OffreAccepteePDFView,
			self).get_context_data(pagesize="A4", *args, **kwargs)
		context['title'] = "Offre acceptée %s au prix %d €" % (offre.bien, offre.prix)
		return context


class FactureVentePDFView(UserIsAdminMixin, PDFTemplateResponseMixin, DetailView):
	model = Vente
	template_name = "biens/pdf/facture_vente.html"

	def get_context_data(self, *args, **kwargs):
		return super(FactureVentePDFView,
			self).get_context_data(pagesize="A4", *args, **kwargs)

class BienSignMandatPDFView(LoginRequiredMixin, PDFTemplateResponseMixin, DetailView):
	model = BienAValider
	slug_field = 'nb_mandat'
	slug_url_kwarg = 'nb_mandat'
	template_name = "biens/pdf/sign_mandat.html"

	def get_bien(self):
		return self.get_object()

	def get_context_data(self, *args, **kwargs):
		offre = self.get_object()
		context = super(BienSignMandatPDFView,
			self).get_context_data(pagesize="A4", *args, **kwargs)
		context['bien'] = self.get_object()
		return context

	def get_queryset(self):
		qs = super(BienSignMandatPDFView, self).get_queryset()

		if self.request.user.is_superuser:
			return qs
		if self.request.user.client is not None:
			return qs.filter(vendeur=self.request.user.client)

class BienMandatPDFView(LoginRequiredMixin, PDFTemplateResponseMixin, DetailView):
	model = Bien
	slug_field = 'nb_mandat'
	slug_url_kwarg = 'nb_mandat'
	template_name = "biens/pdf/mandat.html"


def get_context_data(self, *args, **kwargs):
	offre = self.get_object()
	context = super(BienMandatPDFView,
		self).get_context_data(pagesize="A4", *args, **kwargs)
	context['bien'] = self.get_object()
	return context

	def get_queryset(self):
		qs = super(BienMandatPDFView, self).get_queryset()

		if self.request.user.is_superuser:
			return qs
		if self.request.user.client is not None:
			return qs.filter(vendeur=self.request.user.client)

class RegistreDesMandatsPDFView(LoginRequiredMixin, PDFTemplateView):
	template_name = "biens/pdf/registre_des_mandats.html"

	def get_context_data(self, **kwargs):
		if self.request.user.is_superuser:
			return super(RegistreDesMandatsPDFView, self).get_context_data(
				pagesize='A4',
				title='Registre des Mandats',
				biens=Bien.objects.exclude(valide=None).order_by("nb_mandat").all(),
				**kwargs
			)
		raise Bien.DoesNotExist()


def registre_des_mandats_to_xlsx_view(request):
	if request.user.is_authenticated and request.user.is_superuser:
		data = []
		data.append(["REGISTRE DES MANDATS (transactions sur immeubles et fonds de commerce)"])
		data.append(["TITULAIRE DU REGISTRE : SPEEDHOUSING, société par actions simplifiée, au capital de 15 000 euros, immatriculée au registre du commerce et des sociétés de Bobigny sous le numéro 823 244 512, dont le siège social est situé au 17 bis rue Emile Augier 93310 Le Pré-Saint-Gervais, représenté par Madame Neda TABAN, en qualité de présidente."])
		data.append(["TITULAIRE DE LA CARTE PROFESSIONNELLE « transactions sur immeubles et fonds de commerce » N° : CPI 9301 2017 000 016 755 - sans perception de fonds, délivrée le 17/02/2017 par la CCI Paris Ile-de-France - le vice-président Gérard LISSORGUES, valable jusqu’au 16/02/2020."])
		data.append([""])
		data.append([""])
		data.append([
				"N° du Mandat",
				"Status",
				"Date du Mandat",
				"Nom du Mandant",
				"Adresse du Mandant",
				"Objet du Mandant",
				"Adresse des Biens à vendre",
				"Nature des biens",
				"Situations des biens"
		])
		for bien in Bien.objects.exclude(valide=None).filter(deleted=None).order_by("nb_mandat").all():
			data.append([
				bien.nb_mandat,
				bien.get_status(),
				bien.creation.strftime(DATETIME_INPUT_FORMATS[0]),
				unicode(bien.vendeur),
				unicode(bien.vendeur.adresse),
				"publicité, intermédiation, délégation, négociation",
				unicode(bien.adresse),
				bien.get_type_display(),
				u"%s, %dm², %d Pièce(s), %d Chambre(s)" % (bien.get_type_display(), bien.surface, bien.pieces, bien.chambres)
			])
		pyexcel.save_as(array=data, dest_file_name="/tmp/registre_des_mandats.xlsx")
		fs = FileSystemStorage('/tmp')
		with fs.open('registre_des_mandats.xlsx') as f:
			response = HttpResponse(f, content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
			response['Content-Disposition'] = 'attachment; filename="registre_des_mandats.xlsx"'
			return response
	raise Bien.DoesNotExist()


class GestionFinancierePDFView(LoginRequiredMixin, PDFTemplateView):
	template_name = "biens/pdf/gestion_financiere.html"

	def get_context_data(self, **kwargs):
		if self.request.user.is_superuser:
			if bool(self.request.GET.get('passe_notaire__isnull', None)):
				ventes = Vente.objects.filter(passe_notaire=None)
			else:
				filter = self.request.GET.get('passe_notaire__exact', None)
				if filter in [0, '0']:
					ventes = Vente.objects.filter(passe_notaire=False)
				elif filter in [1, '1']:
					ventes = Vente.objects.filter(passe_notaire=True)
				else:
					ventes = Vente.objects
			ventes = ventes.order_by("bien__nb_mandat").all()
			return super(GestionFinancierePDFView, self).get_context_data(
				pagesize='A4',
				title='Gestion financiere',
				ventes=ventes,
				date=timezone.now(),
				**kwargs
			)
		raise Vente.DoesNotExist()


def gestion_financiere_to_xlsx_view(request):
	if request.user.is_authenticated and request.user.is_superuser:
		data = []
		data.append([
				"Reference",
				"Nom du Vendeur",
				"Adresse du Bien",
				"Typologie",
				"Prix mandat",
				"Prix offre finale vendu",
				"Com SH 0.4% TTC",
				"Com SH 0.4% HT",
				"Com délégué 2.6% TTC",
				"Coordonnées Vendeur",
				"Coordonnées Notaire affilié",
				"Passé Notaire"
		])
		if bool(request.GET.get('passe_notaire__isnull', None)):
				ventes = Vente.objects.filter(passe_notaire=None)
		else:
			filter = request.GET.get('passe_notaire__exact', None)
			if filter in [0, '0']:
				ventes = Vente.objects.filter(passe_notaire=False)
			elif filter in [1, '1']:
				ventes = Vente.objects.filter(passe_notaire=True)
			else:
				ventes = Vente.objects
		for vente in ventes.order_by("bien__nb_mandat"):
			data.append([
				vente.bien.nb_mandat,
				unicode(vente.offre.vendeur),
				unicode(vente.bien.adresse),
				vente.bien.get_type_display(),
				vente.bien.prix,
				vente.offre.prix,
				vente.offre.get_com_sh_ttc(),
				vente.offre.get_com_sh_ht(),
				vente.offre.get_com_delegue(),
				unicode(vente.bien.vendeur.adresse),
				vente.bien.notaire.get_coordonees(),
				u"Offre acceptée en attente de signature" if vente.passe_notaire is None else u"Vente Annulée" if vente.passe_notaire is False else u"Vente à eu lieu"
			])
		pyexcel.save_as(array=data, dest_file_name="/tmp/gestion_financiere.xlsx")
		fs = FileSystemStorage('/tmp')
		with fs.open('gestion_financiere.xlsx') as f:
			response = HttpResponse(f, content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
			response['Content-Disposition'] = 'attachment; filename="gestion_financiere.xlsx"'
			return response
	raise Vente.DoesNotExist()

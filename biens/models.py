# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os
import json

from django.db import models

from biens import (
	CLASSES_ENERGIE, TYPES_BIENS, TYPES_DOCUMENTS,
	EXPOSITIONS, CHAUFFAGES, TYPES_CHAUFFAGES, ETAGES,

)

from django.utils import timezone
from geo.models import GeoLocalisable
from trash.models import Trashable
from utilisateurs.models import Client, Agent, AgentClient
from sh.settings import DATETIME_INPUT_FORMATS
from django.db.models import AutoField

from .utils import get_new_nb_mandat, rename_image, get_logentry_bienavalider, get_file_path
from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver


from django.utils.text import slugify
import dateutil, datetime
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.shortcuts import redirect


class Notaire(GeoLocalisable):

	nom = models.CharField(u"Nom", max_length=255)
	prenom = models.CharField(u"Prénom", max_length=50)

	email = models.EmailField()
	telephone = models.CharField(u"Téléphone", max_length=15)
	#fax = models.CharField(max_length=15)

	valide = models.BooleanField(default=True)

	def __unicode__(self):
		return u"%s %s à %s" % (self.prenom.title(),
			self.nom.upper(), self.ville)

	def get_full_name(self):
		return "%s %s" % (self.prenom, self.nom)

	def get_coordonees(self):
		return u"%s %s %s %s" % (self.get_nom(), self.telephone, self.email, self.adresse)

	def get_nom(self):
		return u"%s %s" % (self.prenom.title(), self.nom.upper())

	class Meta:
		unique_together = ('nom', 'prenom', 'email')



# class Propriete(models.Model):
# 	TYPES = (
# 		('c', u"Commodités"),
# 		('d', u"Détails du bien"),
# 		('p', u"À Proximité")
# 	)
# 	nom = models.CharField(max_length=50)
# 	type = models.CharField(max_length=1,
# 		choices=TYPES)
# 	valeurs = models.CharField(max_length=255,
# 		blank=True,
# 		help_text="Les valeurs de la liste deroulante"
# 		" separées par ';'")
# 	description = models.TextField(blank=True,
# 		help_text="Le texte affiché à l'utilisateur")

# 	def __unicode__(self):
# 		return u"%s" % self.nom


class Bien(GeoLocalisable):
	vendeur = models.ForeignKey(Client, related_name='biens')
	nb_mandat = models.PositiveIntegerField(unique=True, null=True)
	description = models.TextField(blank=True, null=True)
	notaire = models.ForeignKey(Notaire, related_name='biens', blank=True, null=True)
	valide = models.DateTimeField(blank=True, null=True, default=None)

	# Global
	type = models.CharField(max_length=1, choices=TYPES_BIENS)
	energie = models.CharField(max_length=1, choices=CLASSES_ENERGIE)
	exposition = models.CharField(max_length=2, choices=EXPOSITIONS)
	chauffage = models.CharField(max_length=1, choices=CHAUFFAGES)
	type_chauffage = models.CharField(max_length=1,
		choices=TYPES_CHAUFFAGES)
	prix = models.PositiveIntegerField()
	taxe = models.PositiveIntegerField(blank=True, null=True)
	charges = models.PositiveIntegerField(blank=True, null=True)
	lot = models.CharField(max_length=50,blank=True, null=True,
		help_text='Numéro du lot de copropriété')
	surface = models.PositiveIntegerField()
	pieces = models.PositiveIntegerField()
	sdb = models.PositiveIntegerField()
	sde = models.PositiveIntegerField()
	construction = models.PositiveIntegerField()
	chambres = models.PositiveIntegerField()
	etage = models.PositiveIntegerField(choices=ETAGES)

	# Surfaces annexes
	surface_terrasse = models.PositiveIntegerField(blank=True, null=True)
	surface_balcon = models.PositiveIntegerField(blank=True, null=True)
	surface_jardin = models.PositiveIntegerField(blank=True, null=True)

	# Cuisine
	cuisine_americaine = models.BooleanField(default=False)
	cuisine_separee = models.BooleanField(default=False)
	cuisine_equipee = models.BooleanField(default=False)

	# commodites
	gardien = models.BooleanField(default=False)
	digicode = models.BooleanField(default=False)
	interphone = models.BooleanField(default=False)
	cave = models.BooleanField(default=False)
	parking = models.BooleanField(default=False)
	refait_a_neuf = models.BooleanField(default=False)
	a_renover = models.BooleanField(default=False)
	piscine = models.BooleanField(default=False)
	belle_vue = models.BooleanField(default=False)
	ascenseur = models.BooleanField(default=False)
	dernier_etage = models.BooleanField(default=False)
	sans_vis_a_vis = models.BooleanField(default=False)
	alarme = models.BooleanField(default=False)
	access_handicape = models.BooleanField(default=False)
	cheminee = models.BooleanField(default=False)
	box = models.BooleanField(default=False)
	rangements = models.BooleanField(default=False)
	dressing = models.BooleanField(default=False)
	placards = models.BooleanField(default=False)

	# proximité
	transports = models.BooleanField(default=False)
	commerces = models.BooleanField(default=False)
	ecoles = models.BooleanField(default=False)
	loisirs = models.BooleanField(default=False)

	deleted = models.DateTimeField(null=True, default=None,blank=True)
	creation = models.DateTimeField(auto_now_add=True)

	modification = models.DateTimeField(auto_now=True,editable=True)

	delete_on = models.DateTimeField(auto_now=False,editable=True,null=True,blank=True)


	a_Afficher = models.BooleanField(default=False)
	signature =  models.BooleanField(default=False)



	def get_designation(self):
		str = ""
		if self.type == 't' or self.type == 'p' or self.type == 'b' or self.type == 'i':
			return u"%s %dm²" % (self.get_type_display(), self.surface)

		str += u"%s ; %d m², %d pièces ; %d chambres" % (self.get_type_display(), self.surface, self.pieces, self.chambres)
		if self.cuisine_equipee:
			str+= " ; Cuisine équipée"
		if self.cuisine_americaine:
			str+= " ; Cuisine américaine"
		if self.cuisine_separee:
			str+= " ; Cuisine séparée"

		if 	self.sdb > 1:
			str+= u" ; %s Salles de bains" % (self.sdb)
		elif self.sdb == 1:
			str+= u" ; %s Salle de bain" % (self.sdb)
		if 	self.sde > 1:
			str+= u" ; %s Salles d'eau" % (self.sde)
		elif self.sde == 1:
			str+= u" ; %s Salle d'eau" % (self.sde)

		return str

	def get_type(self):
		return dict(TYPES_BIENS)[self.type]
	def get_etage(self):
		return dict(ETAGES)[self.etage]
	def get_energie(self):
		return dict(CLASSES_ENERGIE)[self.energie]
	def get_exposition(self):
		return dict(EXPOSITIONS)[self.exposition]
	def get_chauffage(self):
		return dict(CHAUFFAGES)[self.chauffage]
	def get_type_chauffage(self):
		return dict(TYPES_CHAUFFAGES)[self.type_chauffage]
	def get_offre_acceptee(self):
		return self.offres.filter(acceptee__isnull=False).first()
	def get_situation(self):
		if self.type == 't' or self.type == 'p' or self.type == 'b' or self.type == 'i':
			return u"%s %dm²" % (self.get_type_display(), self.surface)
		else:
			return u"%s %dm² | %d pièces | %d chambres" % (self.get_type_display(), self.surface, self.pieces, self.chambres)

	def __unicode__(self):
		return u"%s de %d m², %s %s" % (
			self.get_type(),
			self.surface, self.code_postal, self.ville)

	def save(self, *args, **kwargs):
		if self.nb_mandat is None:
	 		self.nb_mandat = get_new_nb_mandat()
	 	return super(Bien, self).save(*args, **kwargs)


	def get_absolute_url(self):
		from django.urls import reverse
		return reverse('annonce', args=[str(selfnb_mandat)])

	def get_photo_url(self):
		p = self.photos.first()
		if p:
			return p.image.url
		return static('img/default4.png')

	def get_en_ligne(self):
		return self.valide is not None
	get_en_ligne.boolean = True
	get_en_ligne.short_description = "En ligne"

	def get_vendu(self):
		return self.offres.filter(acceptee__isnull=False).count() > 0
	get_vendu.boolean = True
	get_vendu.short_description = "Vendu"

	def get_passe_notaire(self):
		return self.get_vendu() and self.vente.filter(passe_notaire=True).count() > 0

	def get_status(self):
		if self.deleted is not None :
			if datetime.datetime.now() > self.deleted + datetime.timedelta(days=15):
				return "Supprimé"
			else:
				return "Suppression dans " + str(( self.deleted-datetime.datetime.now() + datetime.timedelta(days=15)  ).days) +" jours"
		elif timezone.now() > self.creation + timezone.timedelta(days=365):
			return "Expiré"
		elif self.get_vendu():
			return "Vendu"
		elif self.source_origin.count() > 0:
			return "En Ligne & Modifié"
		elif self.get_en_ligne():
			return "En Ligne"
		else:
			return "Inconnu"

	def get_rruleset(self, start=None, end=None):


		set = dateutil.rrule.rruleset()

		for dispo in self.disponibilites.all():
			for date in dispo.get_weekly_rrule(start, end):
				set.rrule(dispo.get_minutely_rrule(date))

		for joff in self.jours_off.all():
			set.exrule(joff.get_rrule())

		for rdv in self.rdvs.all():
			set.exdate(rdv.crenau)

		return set


	def get_1er_rdv(self):
		return next(
			iter(
				self.get_rruleset(
					start = datetime.datetime.now()
				)
			),
			None
		)

	def get_fields(self):
		return dict([(f.name, getattr(self, f.name))
					for f in self._meta.fields
					if not isinstance(f, AutoField) and not f in self._meta.parents.values()])

	def get_all_rdvs(self):
		from agenda.models import RDV
		return RDV.all_objects.filter(bien=self)


class BienAValider(GeoLocalisable):
	"""
	Class to copie a Bien, to be validated by the admin before replace the source Bien (if validated in admin), or deleted otherwise
	"""
	source = models.ForeignKey(Bien, related_name="source_origin", null=True, default=None)
	checked = models.BooleanField(default=False)
	modification_a_valider = models.DateTimeField(auto_now=True)

	# COPY of All fields from Bien (no choice, otherwise a new Bien is created and as to be deleted)
	vendeur = models.ForeignKey(Client, related_name='biens_a_valider')
	nb_mandat = models.PositiveIntegerField(unique=True, null=True) # TODO: Remove the null=True
	description = models.TextField(blank=True, null=True)
	notaire = models.ForeignKey(Notaire, related_name='biens_a_valider', blank=True, null=True)
	valide = models.DateTimeField(blank=True, null=True, default=None)

	# Global
	type = models.CharField(max_length=1, choices=TYPES_BIENS)
	energie = models.CharField(max_length=1, choices=CLASSES_ENERGIE)
	exposition = models.CharField(max_length=2, choices=EXPOSITIONS)
	chauffage = models.CharField(max_length=1, choices=CHAUFFAGES)
	type_chauffage = models.CharField(max_length=1,
		choices=TYPES_CHAUFFAGES)
	prix = models.PositiveIntegerField()
	taxe = models.PositiveIntegerField(blank=True, null=True)
	charges = models.PositiveIntegerField(blank=True, null=True)
	lot = models.CharField(max_length=50, blank=True, null=True,
		help_text='Numéro lot de copropriété')
	surface = models.PositiveIntegerField()
	pieces = models.PositiveIntegerField()
	sdb = models.PositiveIntegerField()
	sde = models.PositiveIntegerField()
	construction = models.PositiveIntegerField()
	chambres = models.PositiveIntegerField()
	etage = models.PositiveIntegerField(choices=ETAGES)

	# Surfaces annexes
	surface_terrasse = models.PositiveIntegerField(blank=True, null=True)
	surface_balcon = models.PositiveIntegerField(blank=True, null=True)
	surface_jardin = models.PositiveIntegerField(blank=True, null=True)

	# Cuisine
	cuisine_americaine = models.BooleanField(default=False)
	cuisine_separee = models.BooleanField(default=False)
	cuisine_equipee = models.BooleanField(default=False)

	# commodites
	gardien = models.BooleanField(default=False)
	digicode = models.BooleanField(default=False)
	interphone = models.BooleanField(default=False)
	cave = models.BooleanField(default=False)
	parking = models.BooleanField(default=False)
	refait_a_neuf = models.BooleanField(default=False)
	a_renover = models.BooleanField(default=False)
	piscine = models.BooleanField(default=False)
	belle_vue = models.BooleanField(default=False)
	ascenseur = models.BooleanField(default=False)
	dernier_etage = models.BooleanField(default=False)
	sans_vis_a_vis = models.BooleanField(default=False)
	alarme = models.BooleanField(default=False)
	access_handicape = models.BooleanField(default=False)
	cheminee = models.BooleanField(default=False)
	box = models.BooleanField(default=False)
	rangements = models.BooleanField(default=False)
	dressing = models.BooleanField(default=False)
	placards = models.BooleanField(default=False)

	# proximité
	transports = models.BooleanField(default=False)
	commerces = models.BooleanField(default=False)
	ecoles = models.BooleanField(default=False)
	loisirs = models.BooleanField(default=False)

	creation = models.DateTimeField(auto_now=True)
	modification = models.DateTimeField(auto_now=True,editable=True)
	delete_on = models.DateTimeField(auto_now=False,editable=True,null=True,blank=True)


	a_Afficher = models.BooleanField(default=False)
	signature =  models.BooleanField(default=False)


	def get_type(self):
		return dict(TYPES_BIENS)[self.type]
	def get_etage(self):
		return dict(ETAGES)[self.etage]
	def get_energie(self):
		return dict(CLASSES_ENERGIE)[self.energie]
	def get_exposition(self):
		return dict(EXPOSITIONS)[self.exposition]
	def get_chauffage(self):
		return dict(CHAUFFAGES)[self.chauffage]
	def get_type_chauffage(self):
		return dict(TYPES_CHAUFFAGES)[self.type_chauffage]
	def get_offre_acceptee(self):
		if self.source:
			return self.get_offre_acceptee()
		return None
	def get_fields(self):
		return dict([(f.name, getattr(self, f.name))
					for f in self._meta.fields
					if not isinstance(f, AutoField) and not f in self._meta.parents.values()])
	def get_history(self):
		return get_logentry_bienavalider(self.id)

	def get_photo_url(self):
		if self.source:
			return self.source.get_photo_url()
		p = self.photos_a_valider.first()
		if p:
			return p.image.url
		return "http://via.placeholder.com/350x150"

	def __unicode__(self):
		return u"%s de %d m², %s %s" % (
			self.get_type(),
			self.surface, self.code_postal, self.ville)

	def save(self, *args, **kwargs):
		if self.nb_mandat is None:
			# If nb_mandat is not set
			self.nb_mandat = get_new_nb_mandat()
		super(BienAValider, self).save(*args, **kwargs)

class CustomImageField(models.ImageField):
	def delete(self, *args, **kwargs):
		return super(CustomImageField, self).delete()


class Photo(models.Model):
	bien = models.ForeignKey(Bien, related_name='photos', default=None, null=True)
	bien_a_valider = models.ForeignKey(BienAValider, related_name='photos_a_valider', default=None, null=True)
	image = CustomImageField(upload_to=rename_image, null=True)

	def __unicode__(self):
		return self.image.name

	def get_thumbnail(self):
		return u'<img src="%s" width="100px" />' % self.image.url
	get_thumbnail.allow_tags = True
	get_thumbnail.short_description = 'Image'


class Document(models.Model):
	bien = models.ForeignKey(Bien, related_name='documents', null=True, default=None)
	bien_a_valider = models.ForeignKey(BienAValider, related_name='documents_a_valider', null=True, default=None)
	name = models.CharField(u"Libellé", max_length=64)
	type = models.PositiveIntegerField(u"Type du document",
		choices=TYPES_DOCUMENTS)
	file = models.FileField(u"Fichier", upload_to=get_file_path)

	class Meta:
		ordering = ['type']

	def get_type(self):
		return dict(TYPES_DOCUMENTS)[self.type]

	def get_filename(self):
		return os.path.basename(self.file.name)


	def __unicode__(self):
		return self.get_type()



class Offre(Trashable):

	offre = models.OneToOneField('Offre', verbose_name='Offre initiale',
		null=True, blank=True, default=None,
		related_name='contre_offre')


	vendeur = models.ForeignKey(Client, related_name='vendeur_offres')
	agent = models.ForeignKey(Agent, related_name='offres')

	acheteur = models.ForeignKey(Client,
		null=True, blank=True, default=None, related_name='acheteur_offres')


	agent_client = models.ForeignKey(AgentClient,
		null=True, blank=True, default=None, related_name='offres')

	bien = models.ForeignKey(Bien, related_name='offres')
	prix = models.PositiveIntegerField()


	initiater = models.BooleanField(default=False, help_text="True (offre provenant du vendeur) / False (sinon)")
	contre_offre_acceptee = models.BooleanField(default=False, help_text="Contre-offre acceptee")

	# True if vendeur -> agent
	# False if agent -> vendeur

	acceptee = models.DateTimeField(null=True, blank=True, default=None)
	refusee = models.DateTimeField(null=True, blank=True, default=None)

	class Meta(Trashable.Meta):
		ordering = ['-created', '-prix']

	def __unicode__(self):
		if self.offre:
			return u"%s -> %s €" % (self.offre.prix, self.prix)
		return u"%s €" % self.prix

	@property
	def acquereur(self):
		if self.acheteur:
			return self.acheteur
		return self.agent_client

	@property
	def commission(self):
		return int(round(0.03 * self.prix))

	def accept(self):
		self.acceptee = timezone.now()
		self.save()
		self.bien.offres.filter(acceptee=None, refusee=None).update(refusee=timezone.now())
		vente = Vente(offre=self, bien=self.bien)
		vente.save()
		return vente

	def refuse(self):
		self.refusee = timezone.now()
		self.save()

	def counter(self, offre, vendeur=False):
		"""
			Les offres sont contrees jusqu'a parvenir a une offre identique de
			part et d'autre.

			Le dernier mot est laisse au vendeur. Il valide en faisant une
			contre-offre du montant propose par l'agent, ce qui marque
			automatiquement l'offre acceptee et cree la vente.

			Chaque fois qu'une contre-offre differente est faite, l'offre
			initiale est automatiquement refusee.
		"""
		self.offre = offre
		self.vendeur = offre.vendeur
		self.agent = offre.agent
		self.acheteur  = offre.acheteur
		self.agent_client = offre.agent_client
		self.bien = offre.bien
		self.initiater = vendeur
		self.contre_offre_acceptee = offre.contre_offre_acceptee;
		self.save()

		offre.refuse()

		if vendeur and self.prix == offre.prix :
			if 	offre.contre_offre_acceptee and self.prix == offre.prix:
				self.accept()

	def get_com_sh_ttc(self):
		return round(self.prix * 0.004, 2)

	def get_com_sh_ht(self):
		return round((self.prix * 0.004) / 1.2, 2)

	def get_com_delegue(self):
		return round(self.prix * 0.026, 2)

class Vente(models.Model):
	class Meta:
		verbose_name = 'Vente'
		verbose_name_plural = 'Gestion Financiere'

	bien = models.ForeignKey(Bien, related_name="vente")
	offre = models.ForeignKey(Offre, related_name="vente")
	passe_notaire = models.NullBooleanField(null=True, default=None)


@receiver(post_save, sender=Vente)
def save_vente(sender, instance, **kwargs):
	"""
	Check if passe_notaire is set to False, to cancel the vente
	"""
	if instance.passe_notaire is False:
		# The bien must be publish again
		# Refusing the accepted offer
		instance.offre.refusee = instance.offre.acceptee
		instance.offre.acceptee = None
		instance.offre.save()
		# Delete the current vente
		instance.delete()


@receiver(pre_delete, sender=Vente)
def remove_vente(sender, instance, **kwargs):
	"""
	If someone delete an instance of vente without pass the passe_notaire to False
	"""
	if instance.offre.acceptee != None:
		# The bien must be publish again
		# Refusing the accepted offer
		instance.offre.refusee = instance.offre.acceptee
		instance.offre.acceptee = None
		instance.offre.save()

class Mandat(models.Model):
	"""
	This class is made to display a specific Legal page in the admin "Le registre des mandats"
	"""
	class Meta:
		verbose_name = 'Mandat'
		verbose_name_plural = 'Registre des Mandats'

	bien = models.ForeignKey(Bien, related_name="registre")


# class MandatSign(models.Model):
# 	"""
# 	This class is made to display a specific Legal page in the admin "Le registre des mandats"
# 	"""
# 	class Meta:
# 		verbose_name = 'Mandat'
# 		verbose_name_plural = 'Registre des Mandats'
#
# 	bien = models.ForeignKey(BienAValider, related_name="registre")

from django.contrib.admin.models import LogEntry, CHANGE
from django.contrib.contenttypes.models import ContentType
from os.path import join
from uuid import uuid4

from sh.settings import MEDIA_ROOT

def get_new_nb_mandat():
    """
    Generate a new unique mandat number

    :rtype: int
    :return: The new available mandat number
    """
    from biens.models import BienAValider, Bien
    # Return a new value for the nb_mandat
    last_bien = Bien.objects.order_by("-nb_mandat").first()
    last_bien_a_valider = BienAValider.objects.order_by("-nb_mandat").first()
    if last_bien_a_valider is None:
        if last_bien is None:
            return 1
        return last_bien.nb_mandat + 1
    elif last_bien is None or last_bien.nb_mandat <= last_bien_a_valider.nb_mandat:
        return last_bien_a_valider.nb_mandat + 1
    return last_bien.nb_mandat + 1


def rename_image(instance, filename):
    """
    Rename the image into a uuid and return the path
    """
    path = "images/"
    # Get extension
    ext = filename.split('.')[-1]
    # Generate random name
    filename = '{}.{}'.format(uuid4().hex, ext.lower())
    # Return path+filename
    return join(path, filename)

def add_logentry_bienavalider(client_id, bien, changes):
    LogEntry.objects.log_action(
        user_id=client_id,
        content_type_id=ContentType.objects.get(app_label="biens", model="bienavalider").pk,
        object_id=bien.id,
        object_repr=unicode(bien),
        action_flag=CHANGE,
        change_message=unicode(changes))

def get_logentry_bienavalider(bien_id):
    return LogEntry.objects.filter(
        object_id=bien_id,
        content_type_id=ContentType.objects.get(app_label="biens", model="bienavalider").id
        ).order_by('action_time')

def get_file_path(instance, filename):
    import uuid, os
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    path = "documents/%s/" % uuid.uuid4()
    return os.path.join(path, filename)

# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext as _

from django.contrib import admin
from sh.admin import admin_site

from biens.models import Bien, Photo, Notaire, Offre, BienAValider, Vente, Mandat
from biens.forms import PhotoForm

from agenda.models import Disponibilite, RDV

from django import forms
from django.db import models
from django.utils import timezone
import datetime
from django.utils.html import mark_safe

from support.forms import NewWindowAdminFileWidget

from django.urls import reverse, reverse_lazy
from sh.settings import DATETIME_INPUT_FORMATS
from django.shortcuts import redirect

import logging

logger = logging.getLogger(__name__)


class ValidationTypeListFilter(admin.SimpleListFilter):
    title = _('statut')
    parameter_name = 'is_new'

    def lookups(self, request, model_admin):
        return (
            (1, _('Nouveau')),
            (0, _('Modifié')),
        )

    def queryset(self, request, queryset):
    	logger.warn("%s : %s" % (type(self.value()), self.value()))
        if self.value() == '1':
            return queryset.filter(source=None)
        if self.value() == '0':
            return queryset.exclude(source=None)
        return queryset


class AdminImageWidget(NewWindowAdminFileWidget):
    def render(self, name, value, attrs=None):
        output = []
        if value:
            output.append(u'<div>{0}</div>'.format(value))
        output.append(super(AdminImageWidget, self).render(name, value, attrs))
        if value and getattr(value, "url", None):
            #l'image mais on pourrait aussi mettre un lien
            img = u'<div><img src="{0}" height="128px"/></div>'.format(value.url)
            #output.append(img)
        return mark_safe(u''.join(output))
	# def render(self, name, value, attrs=None):
	# 	output = []
	# 	if value and getattr(value, "url", None):
	# 		image_url = value.url
	# 		file_name=str(value)
	# 		#output.append(u' <a href="%s" target="_blank"><img src="%s" width="150" height="150"  style="object-fit: cover;"/></a> %s ' % \
	# 		#	(image_url, image_url, file_name ))
	# 		output.append(super(NewWindowAdminFileWidget, self).render(name, value, attrs))
	# 		return mark_safe(u''.join(output))

# class CommoditeInline(admin.TabularInline):
# 	model = Commodite
# 	extra = 1

class PhotoInline(admin.TabularInline):
	model = Photo
	fields = ('image',)
	extra = 0
	formfield_overrides = {
		models.ImageField: {'widget': AdminImageWidget}
	}


class DisponibiliteInline(admin.TabularInline):
	fields = ('jour', 'debut', 'fin')
	model = Disponibilite
	extra = 0

class RDVInline(admin.TabularInline):
	model = RDV
	extra = 0

class OffreInline(admin.TabularInline):
	model = Offre
	extra = 0
	fields = ["offre", "_offre", "vendeur", "initiater", "agent", "agent_client", "prix", "acceptee", "refusee"]
	readonly_fields = ["_offre"]

	def _offre(self, obj):
		return unicode(obj)
	_offre.short_description = "contre-offre"


@admin.register(Bien, site=admin_site)
class BienAdmin(admin.ModelAdmin):
	change_form_template = 'biens/admin/change_bien.html'

	fieldsets = (
		("Detail du bien", {
			'fields': (
				('prix'),
				('type', 'surface'),
				('pieces' , 'chambres'),
				('sdb', 'sde'),
				('energie', 'construction'),
				('exposition', 'etage'),
				('chauffage', 'type_chauffage'),
				('surface_terrasse', 'surface_balcon', 'surface_jardin'),
				('cuisine_americaine', 'cuisine_separee', 'cuisine_equipee'),
				('taxe','charges','lot'),
				('valide'),
				('creation'),
                ('deleted'),
                ('delete_on'),
                ('a_Afficher')

			)}
		),
		('Commodités', {
			'fields': (
				('gardien','digicode', 'interphone', 'cave',
					'parking','refait_a_neuf','a_renover','piscine',
					'belle_vue','ascenseur','dernier_etage'
				),
				('sans_vis_a_vis', 'alarme','access_handicape',
					'cheminee','box', 'rangements','dressing',
					'placards'
				),
			)
		}),
		('Adresse', {
			'fields': (
				'adresse',
				('code_postal', 'ville'),
				('transports', 'commerces', 'ecoles', 'loisirs'),
				'localisation',
				'description'
			)
		}),
		('Vendeur', {
			'fields': ('vendeur',)
		}),
		('Notaire', {
			'fields': ('notaire',)
		}),
	)
	readonly_fields =('creation','deleted',)


	inlines = [DisponibiliteInline, PhotoInline, RDVInline, OffreInline]
	list_display = ('_nb_mandat', '_status', 'type', '_prix', 'vendeur', 'adresse', 'code_postal', 'ville', 'html_mandat')
	list_display_links = ('_nb_mandat',)
	list_filter = ['type','ville',]
	search_fields = ['nb_mandat', 'vendeur__nom', 'vendeur__prenom',
		'lot', 'adresse', 'code_postal', 'ville',]
	ordering = ['nb_mandat']

	def has_delete_permission(self, request,obj=None):
		return False

	def _status(self, obj):

		if obj.deleted is not None :
			if datetime.datetime.now() > obj.deleted + datetime.timedelta(days=15):
				return "Supprimé"
			else:
				return "Suppression dans " + str(( obj.deleted-datetime.datetime.now() + datetime.timedelta(days=15)  ).days) +" jours"
		elif timezone.now() > obj.creation + timezone.timedelta(days=365):
			return "Expiré"
		elif obj.get_vendu():
			return "Vendu"
		elif obj.source_origin.count() > 0:
			return "En Ligne & Modifié"
		elif obj.get_en_ligne():
			return "En Ligne"
		else:
			return "Inconnu"
	_status.short_description = "Status"

	def nb_photos(self, obj):
		return obj.photos.count()
	nb_photos.short_description = "Nombre de photos"

	def _nb_mandat(self, obj):
		return obj.nb_mandat
	_nb_mandat.short_description = "N° Mandat"

	def _prix(self, obj):
		return u"%d €" % obj.prix
	_prix.short_description = "Prix"

	def _id(self, obj):
		return obj.id
	_id.short_description = "ID"

	def html_mandat(self, obj):
		return u"<a href='%s' target='_blank'>Télécharger</a>" % reverse(
			'bien-mandat', args=[obj.nb_mandat])
	html_mandat.short_description = "Mandat"
	html_mandat.allow_tags = True


@admin.register(Mandat, site=admin_site)
class RegistreDesMandatsAdmin(admin.ModelAdmin):
	change_list_template = 'biens/admin/list_registre_des_mandats.html'
	list_display = ('_nb_mandat','_status', '_date', '_vendeur', '_vendeur_adresse', '_reason', '_adresse', '_type', '_situation', '_mandat')
	list_display_links = ('_nb_mandat',)
	list_filter = ("bien__type","bien__vendeur__nom",)
	search_fields = ["bien__type","bien__vendeur__nom"]
	ordering = ['bien__nb_mandat']

	def _nb_mandat(self, obj):
		return obj.bien.nb_mandat
	_nb_mandat.short_description = "Numéro du mandat"

	def _status(self,obj):
		return obj.bien.get_status()
	_status.short_description = "Status du mandat"

	def _date(self, obj):
		return obj.bien.creation.strftime("%Y-%m-%d")
	_date.short_description = "Date du mandat"

	def _vendeur(self, obj):
		return obj.bien.vendeur
	_vendeur.short_description = "Nom du mandant"

	def _vendeur_adresse(self, obj):
		return obj.bien.vendeur.adresse
	_vendeur_adresse.short_description = "Adresse du MANDANT"

	def _reason(self, obj):
		return "publicité, intermediation, délégation, négociation"
	_reason.short_description = "Object du mandat"

	def _adresse(self, obj):
		return obj.bien.adresse
	_adresse.short_description = "Adresse des biens à vendre"

	def _type(self, obj):
		return obj.bien.get_type_display()
	_type.short_description = "Nature des biens"

	def _situation(self, obj):
		return obj.bien.get_situation()
	_situation.short_description = "Situation des biens"

	def _mandat(self, obj):
		return u"<a href='%s' target='_blank'>Télécharger</a>" % reverse(
			'bien-mandat', args=[obj.bien.nb_mandat])
	_mandat.short_description = "Mandat"
	_mandat.allow_tags = True

class BAVPhotoInline(admin.TabularInline):
	model = Photo
	form = PhotoForm
	fields = ('image',)
	fk_name = "bien_a_valider"
	extra = 0
	formfield_overrides = {
		models.ImageField: {'widget': AdminImageWidget}
	}


class BAVDisponibiliteInline(admin.StackedInline):
	fields = ('jour', 'debut', 'fin')
	model = Disponibilite
	fk_name = "bien_a_valider"
	extra = 0

@admin.register(BienAValider, site=admin_site)
class BienAValiderAdmin(admin.ModelAdmin):
	change_form_template = 'biens/admin/change_bien_a_valider.html'
	fields = (
				('type', 'surface'),
				('pieces' , 'chambres'),
				('sdb', 'sde'),
				('energie', 'construction'),
				('exposition', 'etage'),
				('chauffage', 'type_chauffage'),
				('surface_terrasse', 'surface_balcon', 'surface_jardin'),
				('cuisine_americaine', 'cuisine_separee', 'cuisine_equipee'),
				('taxe','charges','lot'),
				('gardien','digicode', 'interphone', 'cave',
					'parking','refait_a_neuf','a_renover','piscine',
					'belle_vue','ascenseur','dernier_etage'
				),
				('sans_vis_a_vis', 'alarme','access_handicape',
					'cheminee','box', 'rangements','dressing',
					'placards'
				),
				('transports', 'commerces', 'ecoles', 'loisirs'),
				'localisation',
				'adresse',
				('code_postal', 'ville'),
				'description',
				'prix',
				'notaire',
                'creation',
                'delete_on'
			)


	readonly_fields =('creation',)


	inlines = [BAVDisponibiliteInline, BAVPhotoInline]
	list_display = ('nb_mandat','signature', '_checked', 'type', '_prix', 'vendeur', 'adresse', 'code_postal', 'ville', 'html_mandat')
	list_display_links = ('nb_mandat','type','vendeur','adresse',)
	list_filter = [ValidationTypeListFilter,'type','ville']
	search_fields = ['nb_mandat', 'vendeur__nom', 'vendeur__prenom',
		'lot', 'adresse', 'code_postal', 'ville', 'type']
	ordering = ['nb_mandat']

	def apply_validation(modeladmin, request, queryset):
		for bienAValider in queryset:
			return redirect ('/admin/biens/bienavalider/'+ str(bienAValider.id))

	apply_validation.short_description="Valider les bien sélectionnés"
	actions = [apply_validation, ]
	def _checked(self, obj):
		return obj.checked
	_checked.short_description = "Déja vérifié"
	_checked.boolean = True

	def nb_photos(self, obj):
		return obj.photos.count()
	nb_photos.short_description = "Nombre de photos"

	def _nb_mandat(self, obj):
		return obj.nb_mandat
	_nb_mandat.short_description = "N° Mandat"

	def _prix(self, obj):
		return u"%d €" % obj.prix
	_prix.short_description = "Prix"

	def _id(self, obj):
		return obj.id
	_id.short_description = "ID"

	def html_mandat(self, obj):
		return u"<a href='%s' target='_blank'>Télécharger</a>" % reverse(
			'bien-mandat', args=[obj.nb_mandat])
	html_mandat.short_description = "Mandat"
	html_mandat.allow_tags = True

@admin.register(Offre, site=admin_site)
class BienOffreAdmin(admin.ModelAdmin):
	list_display = ('html_mandat','_prix', 'vendeur', 'agent','_initiater','_offre','_status','_date','_heure')
	list_display_links = ('vendeur',)
	def get_query_set(self):
		return super(BienOffreAdmin, self).get_query_set().order_by('id');
	def _offre(self, obj):
		return str(obj.prix) + " €"
	def _status(self, obj):
		if obj.acceptee:
			return "Acceptée"
		elif obj.refusee:
			return "Refusée"
		else :
			return ""
	def _date(self, obj):
		if obj.acceptee:
			return obj.acceptee.strftime("%d/%m/%Y")
		elif obj.refusee:
			return obj.refusee.strftime("%d/%m/%Y")
		else:
			return ""
	def _prix(self, obj):
		return str(obj.bien.prix) + " €"
	def _heure(self, obj):
		if obj.acceptee:
			return obj.acceptee.strftime("%Hh%M")
		elif obj.refusee:
			return obj.refusee.strftime("%Hh%M")
		else:
			return ""
	def _initiater(self, obj):
		if obj.initiater:
			return "Contre-offre vendeur"
		else:
			if '->' not in str(obj.offre):
				return "Offre initiale agent"
		return "Contre-offre agent"

	def html_mandat(self, obj):
		return u"<a href='https://speedhousing.fr/admin/biens/bien/%s'>%s</a>" % (obj.bien.id,obj.bien.nb_mandat)
	html_mandat.short_description = "N° Mandat"
	_status.short_description = "Statut"
	_prix.short_description = "Prix du bien"
	_initiater.short_description = "Acteur offre"
	_offre.short_description = "Montant offre"
	html_mandat.allow_tags = True


# @admin.register(Propriete, site=admin_site)
# class ProprieteAdmin(admin.ModelAdmin):
# 	list_display = ('nom', 'type',
# 		'valeurs', 'description')
# 	list_filter = ('type',)
# 	search_fields = ('nom', 'valeurs', 'description')

@admin.register(Notaire, site=admin_site)
class NotaireAdmin(admin.ModelAdmin):
	list_display = ('valide', 'nom', 'prenom', 'email',
		'telephone', 'code_postal', 'ville')
	search_fields = ('nom', 'prenom',
		'ville', 'email', 'telephone')
	list_display_links = ('nom', 'prenom')
	list_filter = ('valide',)
	fieldsets = (
		(None, {
			'fields': ('valide','nom', 'prenom',
				'email', 'telephone'
			)}
		),
		('Adresse', {
			'fields': (
				'adresse',
				('code_postal', 'ville')
			)}
		)
	)

def cancel_vente(modeladmin, request, queryset):
	for vente in queryset:
		vente.passe_notaire = False
		vente.save()
cancel_vente.short_description = "Annuler la/les vente(s)"

@admin.register(Vente, site=admin_site)
class GestionFinanciere(admin.ModelAdmin):
	actions = [cancel_vente]
	change_list_template = 'biens/admin/list_vente.html'
	list_display = ('_reference', '_vendeur', '_bien_address', '_typo', '_price_mandat',
		'_price_final', '_com_SH_ttc', '_com_SH_ht', '_com_delegue', '_coord_vendeur',
		'_coord_notaire', '_passe_notaire', '_link_offre', '_link_facture')

	list_filter = ('passe_notaire',)
	search_fields = ('_reference', '_vendeur', '_bien_address', '_typo', '_price_mandat',
	 	'_price_final', '_com_SH_ttc', '_com_SH_ht', '_com_delegue', '_coord_vendeur',
		 '_coord_notaire', '_passe_notaire', '_link_offre', '_link_facture')


	def _reference(self, obj):
		return obj.bien.nb_mandat
	_reference.short_description = "Reference"

	def _vendeur(self, obj):
		return unicode(obj.offre.vendeur)
	_vendeur.short_description = "Nom du Vendeur"

	def _bien_address(self, obj):
		return obj.bien.adresse
	_bien_address.short_description = "Adresse du Bien"

	def _typo(self, obj):
		return obj.bien.get_type_display()
	_typo.short_description = "Typologie"

	def _price_mandat(self, obj):
		return obj.bien.prix # TODO: Wrong, this one can Change, Add a field fixed on the bien creation
	_price_mandat.short_description = "Prix mandat"

	def _price_final(self, obj):
		return obj.offre.prix
	_price_final.short_description = "Prix offre finale vendu"

	def _com_SH_ttc(self, obj):
		return obj.offre.get_com_sh_ttc()
	_com_SH_ttc.short_description = "Com SH 0.4% TTC"

	def _com_SH_ht(self, obj):
		return obj.offre.get_com_sh_ht()
	_com_SH_ht.short_description = "Com SH 0.4% HT"

	def _com_delegue(self, obj):
		return obj.offre.get_com_delegue()
	_com_delegue.short_description = "Com délégué 2.6% TTC"

	def _coord_vendeur(self, obj):
		return obj.bien.vendeur.adresse
	_coord_vendeur.short_description = "Coordonnées Vendeur"

	def _coord_notaire(self, obj):
		return obj.bien.notaire.get_coordonees()
	_coord_notaire.short_description = "Coordonnées Notaire affilié"

	def _passe_notaire(self, obj):
		if obj.passe_notaire is None:
			return "Offre accepte en attente de signature"
		elif obj.passe_notaire is False:
			return "Vente Annulé"
		else:
			return "Vente à eu lieu"
	_passe_notaire.short_description = "Statut de la signature notairiale"

	def _link_offre(self, obj):
		return u"<a href='%s' target='_blank'>Télécharger</a>" % reverse(
				'offre-acceptee', args=[obj.offre.pk])
	_link_offre.short_description = "L'offre d'achat accepté"
	_link_offre.allow_tags = True

	def _link_facture(self, obj):
		return u"<a href='%s' target='_blank'>Télécharger</a>" % reverse(
				'facture-vente', args=[obj.pk])
	_link_facture.short_description = "Facture"
	_link_facture.allow_tags = True

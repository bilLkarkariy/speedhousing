import logging
import datetime

from django import template
from num2words import num2words


logger = logging.getLogger(__name__)

register = template.Library()

@register.filter
def mult(a,b):
    return a*b

@register.filter
def add_date(value, days):
    return value + datetime.timedelta(days=days)

@register.filter
def in_words(number):
    return num2words(number, lang='fr')

@register.filter
def getlist(query_dict, key):
    return query_dict.getlist(key)

@register.filter
def hash(h, key):
    return h[key]

@register.filter
def replace_commas(s):
    return str(s).replace(',', '.')

@register.filter
def price_ht(price):
    # Compute by removing 3%
    return int(price /1.03)

@register.filter
def price_format(price):
    # Split "11000000" into "11 000 000"
    price = str(price)[::-1]  # 00000011
    splited_prices = [price[i:i+3] for i in range(0, len(price), 3)]  # ["000", "000", "11"]
    return " ".join([m[::-1] for m in reversed(splited_prices)])  # "11 000 000"

@register.filter()
def get_changes(bien_a_valider):
    from ast import literal_eval
    from json import dumps
    changes = []
    for logentry in bien_a_valider.get_history():
        if logentry.change_message:
            changes += literal_eval(logentry.change_message)
        else:
            logger.info("Skipping logentry %s from changes (message : %s)" % (logentry.pk, logentry.change_message))
    return dumps(changes)

@register.filter
def substract(value, arg):
    return value - arg

@register.filter()
def field_type(field):
    return field.field.widget.__class__.__name__

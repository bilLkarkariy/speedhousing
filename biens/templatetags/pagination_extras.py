from django import template
import re

register = template.Library()


@register.filter
def get_page(request, page):
    """
    Add the page parameter into the current url.
    Deal if it's already present or if there is already other parameters.

    :param WSGIRequest request: The current request
    :param integer page: The number of the new page 
    :rtype: string
    :return: The final url with the page parameter
    """
	# Get the current url
    url = request.build_absolute_uri()
    # Find if a parameter "page" is already inside the url
    regex = r"^(.*\?.*)(page=\d+&?)(.*)$"
    if re.search(regex, url):
        # Found
        match = re.search(regex, url)
        # Remove the param page in the url
        url = match.group(1) + match.group(3)
    # Case when no parameter
    if url.find('?') == -1:
        url += "?"
    # If no '&' was already there at the end
    if url[-1] not in ['&', '?']:
        url += "&" 
    # The final url + the new page parameter
    return url + "page=" + str(page)

# -*- coding: utf-8 -*-
from django import forms

from django.contrib.gis.db.models.functions import Distance
from django.db.models import Q

from biens.models import Bien, Notaire, Photo
from agenda import JOURS

# class AnnonceForm(forms.ModelForm):
# 	class Meta:
# 		model = Bien
# 		fields = (
# 			'type', 'energie', 'prix',
# 			'lot', 'surface', 'pieces',
# 			'chambres', 'etage'
# 		)

# class BienRelatedForm(forms.Form):
# 	notaire = forms.IntegerField(widget=forms.HiddenInput())
# 	jour = forms.ChoiceField(choices=JOURS)
# 	debut = forms.ChoiceField(choices=[
# 		(None, u"Début")] + [
# 				(
# 					"%d:%s" % (h, m),
# 					"%d:%s" % (h, m)
# 				)
# 				for h in range(7, 22)
# 				for m in '00', '30'
# 			]
# 		)
# 	fin = forms.ChoiceField(choices=[
# 		(None, u"Fin")] + [
# 				(
# 					"%d:%s" % (h, m),
# 					"%d:%s" % (h, m)
# 				)
# 				for h in range(7, 22)
# 				for m in '00', '30'
# 			]
# 		)
# 	photo = forms.ImageField()


class NotaireForm(forms.ModelForm):

	notaire = forms.IntegerField(
		widget = forms.HiddenInput()
	)

	class Meta:
		model = Notaire
		fields = [
			'notaire', 'prenom', 'nom',
			'email', 'telephone', 'adresse',
			'code_postal', 'ville'
		]

		# error_messages = {
		# 	NON_FIELD_ERRORS: {
		# 		'unique_together': "%(model_name)s's %(field_labels)s are not unique.",
		# 	}
		# }


	def get_notaires_queryset(self, query=None):

		queryset = Notaire.objects.exclude(valide=None).annotate(
			distance_bien = Distance(
				'localisation',
				self.bien.localisation
			),
			distance_client = Distance(
				'localisation',
				self.client.localisation
			)
		)

		if query:
			queryset = queryset.filter(
				Q(nom__icontains=query) | Q(prenom__icontains=query)
			)

		return queryset.order_by(
			'distance_bien',
			'distance_client'
		)[:5]



	def get_notaires(self, query=None):
		notaires = self.get_notaires_queryset(query=query)
		return [(n.pk, self.get_labels(n)) for n in notaires]



	def __init__(self, *args, **kwargs):

		self.client = kwargs.pop('client')
		self.bien = kwargs.pop('bien')

		if self.bien.notaire and not self.bien.notaire.valide:
			super(NotaireForm, self).__init__(
				instance = self.bien.notaire,
				*args, **kwargs
			)
		else:
			super(NotaireForm, self).__init__(*args, **kwargs)
			self.fields['notaire'].initial = self.bien.notaire_id

		for f in self.fields:
			self.fields[f].required = False




	# notaire = forms.ModelChoiceField(
	# 	queryset=Notaire.objects.none(),
	# 	empty_label=u"Sélection du notaire"
	# )

	def get_labels(slef, notaire):
		return "%s  %s - "\
		"%d km de l'adresse du bien" \
		" " % (
			notaire.get_nom(),
			notaire.ville +" "+ notaire.code_postal[:2],
			int(notaire.distance_bien.km)
		)

	# def get_query_set(self):
	# 	return Notaire.objects.annotate(
	# 		distance_bien = Distance(
	# 			'localisation',
	# 			self.bien.localisation
	# 		),
	# 		distance_client = Distance(
	# 			'localisation',
	# 			self.client.localisation
	# 		)
	# 	).order_by(
	# 		'distance_bien',
	# 		'distance_client'
	# 	)

	# def __init__(self, *args, **kwargs):
	# 	self.client = kwargs.pop('client')
	# 	self.bien = kwargs.pop('bien')

	# 	super(NotaireForm, self).__init__(*args, **kwargs)

	# 	self.fields['notaire'].queryset = self.get_query_set()
	# 	self.fields['notaire'].label_from_instance = self.get_labels
	# 	self.fields['notaire'].initial = self.bien.notaire

	# def save(self, commit=True):
	# 	notaire = super(NotaireForm, self).save(commit=False)
	# 	self.bien.notaire_id = self.cleaned_data.get('notaire')
	# 	if commit:
	# 		notaire.save()
	# 		self.bien.save()
	# 	return self.bien

class PhotoForm(forms.ModelForm):

    class Meta(object):
        model = Photo
        fields='__all__'

from biens import TYPES_BIENS
class PlusDeCritereForm(forms.Form):
	localisation = forms.CharField(required=False, help_text="Saisissez une ville, un quartier, un code postal...")
	type = forms.MultipleChoiceField(choices=TYPES_BIENS[1:], required=False)
	min_prix = forms.IntegerField(u"Prix minimum", required=False)
	max_prix = forms.IntegerField(u"Prix maximum", required=False)
	min_surface = forms.IntegerField(u"Surface minimum", required=False)
	max_surface = forms.IntegerField(u"Surface maximum", required=False)
	piece = forms.ChoiceField(choices=[('', u''),(1, u'1'),(2, u'2'), (3, u'3'), (4, u'4'), (5, u'5+')], label=u"Nombre de pièce", required=False)
	chambres = forms.ChoiceField(choices=[('', u''),(1, u'1'),(2, u'2'), (3, u'3'), (4, u'4'), (5, u'5+')], label=u"Nombre de chambre", required=False)
	min_etage = forms.IntegerField(u"Etage minimum", required=False)
	max_etage = forms.IntegerField(u"Etage maximum", required=False)
	cuisine_americaine = forms.BooleanField(required=False)
	cuisine_separee = forms.BooleanField(required=False)
	gardien = forms.BooleanField(required=False)
	digicode = forms.BooleanField(required=False)
	interphone = forms.BooleanField(required=False)
	cave = forms.BooleanField(required=False)
	parking = forms.BooleanField(required=False)
	refait_a_neuf = forms.BooleanField(required=False)
	a_renover = forms.BooleanField(required=False)
	piscine = forms.BooleanField(required=False)
	belle_vue = forms.BooleanField(required=False)
	ascenseur = forms.BooleanField(required=False)
	dernier_etage = forms.BooleanField(required=False)
	sans_vis_a_vis = forms.BooleanField(required=False)
	alarme = forms.BooleanField(required=False)
	access_handicape = forms.BooleanField(required=False)
	cheminee = forms.BooleanField(required=False)
	box = forms.BooleanField(required=False)
	rangements = forms.BooleanField(required=False)
	dressing = forms.BooleanField(required=False)
	placards = forms.BooleanField(required=False)
	with_photo = forms.BooleanField(label=u"Annonce uniquement avec photo", required=False)

	fields = [
			'localisation',
			'type',
			('min_prix', 'max_prix'),
			'min_surface',
			'max_surface',
			'piece',
			'chambres',
			'min_etage',
			'max_etage',
			'cuisine_americaine',
			'cuisine_separee',
			'gardien',
			'digicode',
			'interphone',
			'cave',
			'parking',
			'refait_a_neuf',
			'a_renover',
			'piscine',
			'belle_vue',
			'ascenseur',
			'dernier_etage',
			'sans_vis_a_vis',
			'alarme',
			'access_handicape',
			'cheminee',
			'box',
			'rangements',
			'dressing',
			'placards'
		]

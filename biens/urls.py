# -*- coding: utf-8 -*-
from django.conf.urls import url

from biens.views import (
	BienCreateView, BienRelatedDetailView,
	BienValidateView,BienAValiderValidateView,
	PhotoCreateView, PhotoDeleteView,
	DisponibiliteCreateView, DisponibiliteDeleteView,DisponibiliteBienDeleteView,
	NotaireFormView, BienSignatureView, BienSignatureValideView,
	BienAnnonceView, BienAnnonceListView, recherche_plus
)


from biens.views import (
	OffreAccepteePDFView, BienMandatPDFView,BienSignMandatPDFView,
	RegistreDesMandatsPDFView, registre_des_mandats_to_xlsx_view,
	GestionFinancierePDFView, gestion_financiere_to_xlsx_view,
	FactureVentePDFView
)

urlpatterns = [

	# Formulaire de creation d'une annonce => bien
	url(r'^nouveau/$',
		BienCreateView.as_view(),
		name='new'
	),

	url(r'^nouveau/infos/$',
		BienRelatedDetailView.as_view(),
		name='infos'
	),

	url(r'^nouveau/validation/$',
		BienValidateView.as_view(),
		name='validation'
	),
	url(r'^modification/validation/$',
		BienAValiderValidateView.as_view(),
		name='validation-modification'
	),

	url(r'^nouveau/signature/$',
		BienSignatureView.as_view(),
		name='signature'
	),
	url(r'^nouveau/signature-validation/(?P<nb_mandat>\d+)/$',
		BienSignatureView.as_view(),
		name='signature_delay'
	),
	url(r'^nouveau/signature-valide/(?P<nb_mandat>\d+)/$',
		BienSignatureValideView.as_view(),
		name='signature_valide'
	),

	# Ajout de photo
	# url(r'^(?P<nb_mandat>\d+)/nouveau/photos/$',
	# 	PhotoCreateView.as_view(),
	# 	name='photos'
	# ),

	url(r'^(?P<nb_mandat>\d+)/nouveau/photos/(?P<pk>\d+)/delete/$',
		PhotoDeleteView.as_view(),
		name='photos_delete'
	),


	# Ajout de disponibilités
	url(r'^(?P<nb_mandat>\d+)/nouveau/disponibilites/$',
		DisponibiliteCreateView.as_view(),
		name='disponibilites'
	),

	url(r'^(?P<nb_mandat>\d+)/nouveau/disponibilites/(?P<pk>\d+)/delete/$',
		DisponibiliteDeleteView.as_view(),
		name='disponibilites_delete'
	),
	url(r'^(?P<nb_mandat>\d+)/nouveau/disponibilites-bien/(?P<pk>\d+)/delete/$',
		DisponibiliteBienDeleteView.as_view(),
		name='disponibilites-bien_delete'
	),

	# Choix du notaire
	url(r'^(?P<nb_mandat>\d+)/nouveau/notaire/$',
		NotaireFormView.as_view(),
		name='notaire'
	),

	# Rechercher une annonce
	url(r'^$', BienAnnonceListView.as_view(), name='annonce-list'),

	# Recherche avec plus de critère
	url(r'^rechercher/$', recherche_plus, name='recherche-plus-annonce'),

	# Lien vers l'annonce
	url(r'^annonces/(?P<nb_mandat>\d+)/$',
		BienAnnonceView.as_view(),
		name='annonce'
	),


	# Documents PDF

	url(r'^telecharger/(?P<pk>\d+)/offre-acceptee.pdf$',
		OffreAccepteePDFView.as_view(),
		name='offre-acceptee'
	),

	url(r'^telecharger/(?P<nb_mandat>\d+)/mandat.pdf$',
		BienMandatPDFView.as_view(),
		name='bien-mandat'
	),
	url(r'^telecharger/(?P<nb_mandat>\d+)/sign_mandat.pdf$',
		BienSignMandatPDFView.as_view(),
		name='bien-sign-mandat'
	),

	url(r'^telecharger/registre_des_mandats.pdf$',
		RegistreDesMandatsPDFView.as_view(),
		name='registre-des-mandats-pdf'
	),

	url(r'^telecharger/registre_des_mandats.xlsx$',
		registre_des_mandats_to_xlsx_view,
		name='registre-des-mandats-xlsx'
	),

	url(r'^telecharger/gestion_financiere.pdf$',
		GestionFinancierePDFView.as_view(),
		name='gestion-financiere-pdf'
	),

	url(r'^telecharger/gestion_financiere.xlsx$',
		gestion_financiere_to_xlsx_view,
		name='gestion-financiere-xlsx'
	),

	url(r'^telecharger/(?P<pk>\d+)/facture.pdf$',
		FactureVentePDFView.as_view(),
		name='facture-vente'
	)
]

# -*- coding: utf-8 -*-



TYPES_BIENS = (
	('', u"Type de Bien"),
	('a', u"Appartement"),
	('m', u"Maison"),
	('l', u"Loft"),
	('h', u"Hôtel Particulier"),
	('c', u"Château"),
	('v', u"Villa"),
	('p', u"Parking"),
	('b', u"Box"),
	('i', u"Immeuble"),
	('e', u"Chalet"),
	('t', u"Terrain")
)

CLASSES_ENERGIE = (
	('', u"Classe énergétique"),
	('a', u"Classe énergétique A"),
	('b', u"Classe énergétique B"),
	('c', u"Classe énergétique C"),
	('d', u"Classe énergétique D"),
	('e', u"Classe énergétique E"),
	('f', u"Classe énergétique F"),
	('g', u"Classe énergétique G"),
)


# JOURS = (
# 	('', u"Jour de la semaine"),
# 	('l', u"Lundi"),
# 	('m', u"Mardi"),
# 	('r', u"Mercredi"),
# 	('j', u"Jeudi"),
# 	('v', u"Vendredi"),
# 	('s', u"Samedi"),
# 	('d', u"Dimanche")
# )


TYPES_DOCUMENTS = (
	(None, u"Choisir type document"),
	(1, u"Acte de propriété"),
	(2, u"Diagnostic surface habitable ou carrez"),
	(3, u"Diagnostics techniques obligatoires"),
	(4, u"PV d'AG du syndicat année N-1"),
	(5, u"PV d'AG du syndicat année N-2"),
	(6, u"PV d'AG du syndicat année N-3"),
	(7, u"Carnet d'entretien de l'immeuble"),
	(8, u"Autres document à partager")
)


EXPOSITIONS = (
	(None, u"Exposition"),
	('s', u"Sud"),
	('n', u"Nord"),
	('e', u"Est"),
	('o', u"Ouest"),
	('se', u"Sud Est"),
	('so', u"Sud Ouest"),
	('ne', u"Nord Est"),
	('no', u"Nord Ouest")
)


CHAUFFAGES = (
	(None, u"Chauffage"),
	('c', u"Collectif"),
	('i', u"Individuel")
)


TYPES_CHAUFFAGES = (
	(None, u"Type de chauffage"),
	('e', u"éléctrique"),
	('g', u"au gaz"),
	('s', u"au sol"),
	('f', u"fuel"),
	('c', u"condensation")
)

ETAGES = [
	(None, u"Étage"), (0, "Rez-de-chaussée"),
	(1, "1er étage")] + [
		(i, "%dème étage" % i) for i in range(2, 100)
]

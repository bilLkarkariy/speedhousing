# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-11-12 21:56
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('utilisateurs', '0001_initial'),
        ('biens', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='offre',
            name='acheteur',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='acheteur_offres', to='utilisateurs.Client'),
        ),
        migrations.AddField(
            model_name='offre',
            name='agent',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='offres', to='utilisateurs.Agent'),
        ),
        migrations.AddField(
            model_name='offre',
            name='agent_client',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='offres', to='utilisateurs.AgentClient'),
        ),
        migrations.AddField(
            model_name='offre',
            name='bien',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='offres', to='biens.Bien'),
        ),
        migrations.AddField(
            model_name='offre',
            name='offre',
            field=models.OneToOneField(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='contre_offre', to='biens.Offre', verbose_name='Offre initiale'),
        ),
        migrations.AddField(
            model_name='offre',
            name='vendeur',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='vendeur_offres', to='utilisateurs.Client'),
        ),
        migrations.AlterUniqueTogether(
            name='notaire',
            unique_together=set([('nom', 'prenom', 'email')]),
        ),
        migrations.AddField(
            model_name='mandat',
            name='bien',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='registre', to='biens.Bien'),
        ),
        migrations.AddField(
            model_name='document',
            name='bien',
            field=models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='documents', to='biens.Bien'),
        ),
        migrations.AddField(
            model_name='document',
            name='bien_a_valider',
            field=models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='documents_a_valider', to='biens.BienAValider'),
        ),
        migrations.AddField(
            model_name='bienavalider',
            name='notaire',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='biens_a_valider', to='biens.Notaire'),
        ),
        migrations.AddField(
            model_name='bienavalider',
            name='source',
            field=models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='source_origin', to='biens.Bien'),
        ),
        migrations.AddField(
            model_name='bienavalider',
            name='vendeur',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='biens_a_valider', to='utilisateurs.Client'),
        ),
        migrations.AddField(
            model_name='bien',
            name='notaire',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='biens', to='biens.Notaire'),
        ),
        migrations.AddField(
            model_name='bien',
            name='vendeur',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='biens', to='utilisateurs.Client'),
        ),
    ]

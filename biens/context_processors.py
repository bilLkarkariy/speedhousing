from biens.models import Bien

def show_last_bien(request):
    biens = Bien.objects.exclude(valide=None) \
        .exclude(offres__acceptee__isnull=False) \
        .exclude(deleted__isnull=False).order_by('-creation')[:6]


    biensAafficher = Bien.objects.exclude(a_Afficher=False)[:6]

    return {
        'biens_all': biensAafficher
    }

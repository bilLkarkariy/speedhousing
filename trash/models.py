# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.db.models.signals import pre_delete, post_delete
from django.utils.timezone import now

class TrashableQuerySet(models.QuerySet):

	def delete(self, force=False):
		if force:
			return super(TrashableQuerySet, self).delete()
		else:
			for obj in self:
				pre_delete.send(sender=self.model, instance=obj, using=self._db)

			qs = self.update(deleted=now())

			for obj in self:
				post_delete.send(sender=self.model, instance=obj, using=self._db)

			return qs

	def undelete(self):
		self.update(deleted=None)


class TrashableManager(models.Manager):

	def get_queryset(self):
		qs = self.get_full_queryset()
		return qs.filter(deleted__isnull=True)

	def get_full_queryset(self):
		return TrashableQuerySet(self.model, using=self._db)

	def get(self, *args, **kwargs):
		if "pk" in kwargs or "id" in kwargs:
			return self.get_full_queryset().get(*args, **kwargs)
		return self.get_queryset().get(*args, **kwargs)

	def filter(self, *args, **kwargs):
		if "pk" in kwargs or "id" in kwargs:
			return self.get_full_queryset().filter(*args, **kwargs)
		return self.get_queryset().filter(*args, **kwargs)

	def deleted(self):
		return self.get_full_queryset().filter(deleted__isnull=False)



class TrashManager(TrashableManager):

	def get_queryset(self):
		qs = self.get_full_queryset()
		return qs.filter(deleted__isnull=False)


class Trashable(models.Model):
	created = models.DateTimeField(auto_now_add=True, editable=False, db_index=True)
	modified = models.DateTimeField(auto_now=True, editable=False)
	deleted = models.DateTimeField(editable=False, null=True)

	trash = TrashManager()
	all_objects = models.Manager()
	objects = TrashableManager()

	class Meta:
		abstract = True
		default_manager_name = 'objects'
		default_related_name = 'objects'
		get_latest_by = '-created'

	def is_deleted(self):
		return self.deleted is not None

	def delete(self, using=None, force=False):
		if force:
			super(Trashable, self).delete(using=using)
		else:
			model_class = type(self)
			pre_delete.send(sender=model_class, instance=self, using=self._state.db)
			self.deleted = now()
			self.save()
			post_delete.send(sender=model_class, instance=self, using=self._state.db)

	def undelete(self, using=None):
		self.deleted = None
		self.save()

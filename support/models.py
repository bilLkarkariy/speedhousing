# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class FAQ(models.Model):
	TYPES = (
		('v', 'Vendeur'),
		('a', 'Acquéreur'),
		('p', 'Agent'),
		('n', 'Notaire')
	)
	type = models.CharField(max_length=1, choices=TYPES)
	question = models.CharField("Question", max_length=512)
	response = models.TextField("Reponse")

	def get_type(self):
		return dict(self.TYPES)[self.type]

	def __unicode__(self):
		return self.get_type()


	class Meta:
		verbose_name='Faq'

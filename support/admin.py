# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from sh.admin import admin_site

from support.models import FAQ

@admin.register(FAQ, site=admin_site)
class FAQAdmin(admin.ModelAdmin):
	fieldsets = (
		(None, {
			'fields': (
				('type', 'question'),
				'response'
				)
			}),
	)
	list_display = ('question', 'response')
	list_filter = ('type',)
	search_fields = ('question', 'response')

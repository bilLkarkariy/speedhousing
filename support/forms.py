# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django import forms
from django.contrib.admin.widgets import AdminFileWidget
from django.core.mail import send_mail
from django.template.loader import render_to_string

from django.conf import settings


class ContactForm(forms.Form):
	first_name = forms.CharField(label="Prénom", max_length=100)
	last_name = forms.CharField(label="Nom", max_length=100)
	subject = forms.CharField(label="Sujet", max_length=100)
	message = forms.CharField(label="Votre message", widget=forms.Textarea)
	sender = forms.EmailField(label="Votre email")
	phone = forms.CharField(label="Téléphone", max_length=14)
	cc_myself = forms.BooleanField(label="Recevoir une copie", required=False)

	def send_email(self):
		subject = self.cleaned_data['subject']
		message = self.cleaned_data['message']
		sender = self.cleaned_data['sender']
		cc_myself = self.cleaned_data['cc_myself']

		html_message = render_to_string('email/contact.html', self.cleaned_data)

		recipients = [settings.DEFAULT_TO_EMAIL]
		if cc_myself:
			recipients.append(sender)

		return send_mail(subject, "", settings.DEFAULT_FROM_EMAIL, recipients, html_message=html_message)


class NewWindowAdminFileWidget(AdminFileWidget):
	template_name = 'support/forms/widgets/clearable_file_input.html'


# -*- coding: utf-8 -*-
from django.conf.urls import url
from support.views import FaqListView, ContactView

app_name = 'support'
urlpatterns = [
	url(r'^faq/$',
		FaqListView.as_view(),
		name='faq'
	),
	url(r'^contact/$',
		ContactView.as_view(),
		name='contact'
	),
]
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

from django.views.generic.list import ListView
from django.views.generic.edit import FormView

from support.models import FAQ

from support.forms import ContactForm
from django.contrib import messages

class FaqListView(ListView):
	model = FAQ

	def get_context_data(self, **kwargs):
		context = super(FaqListView, self).get_context_data(**kwargs)
		context['types'] = FAQ.TYPES
		return context

	def get_queryset(self):
		return FAQ.objects.filter(
			type = self.request.GET.get('q', '')
		)


class ContactView(FormView):
	template_name = 'support/contact.html'
	form_class = ContactForm
	success_url = '/'

	def form_valid(self, form):
		if form.send_email():
			messages.info(self.request, "Votre message a bien été envoyé.")
		return super(ContactView, self).form_valid(form)
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import logging, requests, re, json

logger = logging.getLogger(__name__)


class DbLoader(object):
	def __init__(self):
		print("Running %s" % type(self).__name__)
		self.query_count = 0
		self.session = requests.Session()

	def log_query(self, r):
		self.query_count += 1
		log = open('/tmp/%s.%s' % (type(self).__name__, self.query_count), 'w')
		log.write(r.text.encode('utf-8'))
		log.close()

	def apply_regexps(self, text):
		# Results count
		m = re.search(self.RE_RESULTS_COUNT, text, flags=re.I+re.S+re.U)
		results_count = m.group('results_count') if m else None
		print("%s results to load" % results_count)

		# Next page link
		m = re.search(self.RE_NEXT_PAGE, text, flags=re.I+re.S+re.U)
		next = "%s%s" % (self.BASE_URI, m.group('next')) if m else None
		print("next page : %s" % next)

		# Page results iteration
		return re.finditer(self.RE_SEARCH_INFO, text, flags=re.I+re.S+re.U), next, results_count

	def list_results(self, get_url=None, post_url=None, params=None, payload=None):
		if post_url:
			# Initial search
			r = self.session.post(post_url, params=params, data=payload)
		elif get_url:
			# Following result pages or GET search
			r = self.session.get(get_url, params=params, data=payload)

		self.log_query(r)
		return self.apply_regexps(r.text)

	def get_name(self, entry):
		return entry['name']

	def update_results(self, results, data, save=False):
		print("Updating %s results" % len(results))
		for r in results:
			data[self.get_name(r)] = r
		if save:
			self.save_results(data)
		return data

	def get_all(self, update={}):
		results, next, results_count = self.list_results()

		self.update_results(results, update, save=True)

		while next is not None:
			page, next, results_count = self.list_results(get_url=next)
			self.update_results(page, update, save=True)

		return update

	def load_results(self):
		try:
			result_list = open(self.FILENAME, 'r')
			self.data = json.loads(result_list.read())
			result_list.close()
			print("Loaded %s records from %s" % (len(self.data), self.FILENAME))
			return self.data
		except:
			return {}

	def save_results(self, data):
		if data is not None:
			result_list = open(self.FILENAME, 'w')
			result_list.write(json.dumps(data))
			result_list.close()
			print("%s records saved in %s" % (len(data), self.FILENAME))

	def run(self):
		self.save_results(self.get_all(update=self.load_results()))

	def __unicode__(self):
		return unicode(self.load_results())

	class Meta:
		abstract = True


class NotairesLoader(DbLoader):
	BASE_URI   = "https://www.notaires.fr"
	SEARCH_URI = "%s/fr/annuaires-notaire" % BASE_URI
	AJAX_URI   = "%s/fr/views/ajax" % BASE_URI

	RE_SEARCH_INFO = '<a href="' \
					'(?P<doc_url>/fr/[^"]+)" class.*?">Détails.*?"title"><a[^>]*>' \
					'(?P<name>[^<]+)</a>.*?"text"><p> *' \
					'(?P<address>[^<]+?)</p>'
	RE_RESULTS_COUNT = '<span class="nb-res">.*?(?P<results_count>[0-9]+).*?résultat'
	RE_NEXT_PAGE = '<a href="(?P<next>/fr/annuaires-notaire[^"]+)">Afficher les 10 résultats suivants'

	FILENAME   = "notaires.json"
	IDF_PARAMS = {
		'type': 'liste',
		'field_office_department_value': '075',
		'field_notary_langueparlee_taxo_tid': 'All',
	}
	AJAX_PARAMS = {
		'type': 'liste',
		'field_office_department_value': '075',
		'field_notary_langueparlee_taxo_tid': 'All',
	}
	AJAX_PAYLOAD = {
		'type': 'liste',
		'page': 1,
		'pager_element': 0,
		'field_office_department_value': '075',
		'field_notary_langueparlee_taxo_tid': 'All',
		'view_base_path': 'annuaires-notaire',
		'view_display_id': 'page',
		'view_name': 'annuaires_notaire',
		'view_path': 'annuaires-notaire',
	}

	def apply_regexps(self, text):
		if self.in_loop:
			for cmd in json.loads(text):
				if cmd['command'] == 'insert' and cmd['method'] == 'replaceWith':
					text = cmd['data']
					break

		return super(NotairesLoader, self).apply_regexps(text)

	def list_results(self, *args, **kwargs):
		matches, next, results_count = super(NotairesLoader, self).list_results(*args, **kwargs)
		
		# Page results iteration
		return [
			{
				'name': m.group('name'),
				'doc_url': "%s%s" % (self.BASE_URI, m.group('doc_url')),
				'address': m.group('address')
			} for m in matches
		], next, results_count

	def get_all(self, update={}):
		self.in_loop = False
		results, next, results_count = self.list_results(get_url=self.SEARCH_URI, params=self.IDF_PARAMS)

		self.update_results(results, update, save=True)

		self.in_loop = True
		while next is not None:
			page, next, results_count = self.list_results(post_url=self.AJAX_URI, params=self.AJAX_PARAMS, payload=self.AJAX_PAYLOAD)
			print("Loaded page %s" % self.AJAX_PAYLOAD['page'])
			self.AJAX_PAYLOAD['page'] += 1
			self.update_results(page, update, save=True)



class IADLoader(DbLoader):
	BASE_URI   = "https://www.iadfrance.fr"
	SEARCH_URI = "%s/agent-search-location" % BASE_URI

	FILENAME   = 'agents-iad.json'
	IDF_PARAMS = {
		'southwestlat': 48.6803338,
		'southwestlng': 1.9408156,
		'northeastlat': 48.8499198,
		'northeastlng': 2.6370411
	}

	def get_name(self, entry):
		return "%s %s" % (entry['firstName'], entry['lastName'])

	def list_results(self):
		r = requests.get(self.SEARCH_URI, params=self.IDF_PARAMS)
		self.log_query(r)

		results = r.json()['agents']
		return results, None, len(results)


class CCILoader(DbLoader):
	BASE_URI         = "http://www.cci.fr"
	SEARCH_URI       = "%s/web/trouver-un-professionnel-de-l-immobilier/accueil" % BASE_URI
	RE_SEARCH_ACTION = '<form id="fm-caim-recherche".*?action="(?P<form_action>[^"]+)'
	RE_RESULTS_COUNT = '<h3>(?P<results_count>[0-9]+) résultats trouvés'
	RE_SEARCH_INFO   = '<td.*?class="titre_entreprise".*?<h2>' \
							'(?P<name>.+?)</h2>.*?<tr class="ligne".*?<a.*?href="' \
							'(?P<doc_url>[^"]+)">' \
							'(?P<doc_type>.+?)</a>.*?<td>' \
							'(?P<city>.+?)</td>.*?<td>' \
							'(?P<region>.+?)</td>'
	RE_NEXT_PAGE     = '<a *href="(?P<next>[^"]+)" *class="next"> *Suivant'

	FILENAME = 'agents-cci.json'

	SEARCH_PAYLOAD = {
		"_CAIM_Recherche_WAR_CAIM_Rechercheportlet_INSTANCE_jA2G_codepostal": "",
		"_CAIM_Recherche_WAR_CAIM_Rechercheportlet_INSTANCE_jA2G_enseigne": "",
		"_CAIM_Recherche_WAR_CAIM_Rechercheportlet_INSTANCE_jA2G_nompersonne": "",
		"_CAIM_Recherche_WAR_CAIM_Rechercheportlet_INSTANCE_jA2G_numerocarte": "",
		"_CAIM_Recherche_WAR_CAIM_Rechercheportlet_INSTANCE_jA2G_region": 11, # Ile-de-France
		"_CAIM_Recherche_WAR_CAIM_Rechercheportlet_INSTANCE_jA2G_siren": "",
		"_CAIM_Recherche_WAR_CAIM_Rechercheportlet_INSTANCE_jA2G_ville": "",
		"nomentreprise": "",
	}


	def __init__(self):
		super(CCILoader, self).__init__()

		r = self.session.get(self.SEARCH_URI)
		self.log_query(r)

		m = re.search(self.RE_SEARCH_ACTION, r.text, flags=re.I+re.S+re.U)
		if m is None:
			raise Exception("Search form action not found !")
	
		self.form_action = m.group('form_action')


	def list_results(self, *args, **kwargs):
		matches, next, results_count = super(CCILoader, self).list_results(*args, **kwargs)

		# Page results iteration
		return [
			{
				'name': m.group('name'),
				'doc_url': "%s%s" % (self.BASE_URI, m.group('doc_url')),
				'doc_type': re.sub('[\n\t]+', ' ', m.group('doc_type')),
				'city': m.group('city'),
				'region': m.group('region'),
			} for m in matches
		], next, results_count

	def get_all(self, update={}):
		results, next, results_count = self.list_results(post_url=self.form_action, payload=self.SEARCH_PAYLOAD)

		self.update_results(results, update, save=True)

		while next is not None:
			page, next, results_count = self.list_results(get_url=next)
			self.update_results(page, update, save=True)

		return update


#IADLoader().run()
#NotairesLoader().run()
#CCILoader().run()

#print(unicode(IADLoader()))
#print(unicode(NotairesLoader()))
#print(unicode(CCILoader()))

def test_match():
	sample1 = '''<tr><td><div class="box-result clearfix"><a href="/fr/scp-jean-marc-alexandre-olaf-dechin-dominique-devriendt-et-france-hoang-paris-75" class="btn btn-actions mq-hos">Détails</a><h2 class="title"><a href="/fr/scp-jean-marc-alexandre-olaf-dechin-dominique-devriendt-et-france-hoang-paris-75">SCP Jean-Marc ALEXANDRE, Olaf DECHIN, Dominique DEVRIENDT     et France HOANG</a></h2><div class="text"><p> 4 AVENUE VELASQUEZ -  75008 - PARIS - FRANCE</p></div></div></td></tr><tr><td><div class="box-result clearfix"><a href="/fr/scp-pierre-andre-michaud-et-benoit-herbreteau-paris-75" class="btn btn-actions mq-hos">Détails</a><h2 class="title"><a href="/fr/scp-pierre-andre-michaud-et-benoit-herbreteau-paris-75">SCP Pierre-André MICHAUD et Benoît HERBRETEAU</a></h2><div class="text"><p> 17 AVENUE D ITALIE -  75013 - PARIS - FRANCE</p></div></div></td></tr><tr><td><div class="box-result clearfix"><a href="/fr/scp-mes-philippe-bourdel-pierre-abgrall-jerome-dray-veronique-dejean-de-la-batie-fabien-liva-laurent-bouillot-paris-75" class="btn btn-actions mq-hos">Détails</a><h2 class="title"><a href="/fr/scp-mes-philippe-bourdel-pierre-abgrall-jerome-dray-veronique-dejean-de-la-batie-fabien-liva-laurent-bouillot-paris-75">SCP Mes Philippe BOURDEL, Pierre ABGRALL, Jérôme DRAY, Véronique DEJEAN de la BATIE, Fabien LIVA, Laurent BOUILLOT</a></h2><div class="text"><p> 7-11 QUAI ANDRE CITROEN -  75015 - PARIS - FRANCE</p></div></div></td></tr><tr><td><div class="box-result clearfix"><a href="/fr/scp-chardon-tarrade-le-pleux-moisy-namand-associes-paris-75" class="btn btn-actions mq-hos">Détails</a><h2 class="title"><a href="/fr/scp-chardon-tarrade-le-pleux-moisy-namand-associes-paris-75">SCP CHARDON, TARRADE, LE PLEUX, MOISY-NAMAND &amp;amp; ASSOCIES</a></h2><div class="text"><p> 83 BOULEVARD HAUSSMANN -  75008 - PARIS - FRANCE</p></div></div></td></tr><tr><td><div class="box-result clearfix"><a href="/fr/sas-c-c-notaires-paris-75" class="btn btn-actions mq-hos">Détails</a><h2 class="title"><a href="/fr/sas-c-c-notaires-paris-75">SAS C&amp;amp;C Notaires</a></h2><div class="text"><p> 72 AVENUE DE WAGRAM -  75017 - PARIS - FRANCE</p></div></div></td></tr><tr><td><div class="box-result clearfix"><a href="/fr/sel-adrien-de-saint-jacob-selurl-paris-75" class="btn btn-actions mq-hos">Détails</a><h2 class="title"><a href="/fr/sel-adrien-de-saint-jacob-selurl-paris-75">SEL Adrien de SAINT JACOB, SELURL</a></h2><div class="text"><p> 78 rue de Turbigo -  75003 - PARIS - FRANCE</p></div></div></td></tr><tr><td><div class="box-result clearfix"><a href="/fr/farrando-benoit-paris-75" class="btn btn-actions mq-hos">Détails</a><h2 class="title"><a href="/fr/farrando-benoit-paris-75">FARRANDO Benoît</a></h2><div class="text"><p> 34 36 RUE DE CONSTANTINOPLE -  75008 - PARIS - FRANCE</p></div></div></td></tr><tr><td><div class="box-result clearfix"><a href="/fr/scp-aurore-de-thuin-julien-le-besco-paris-75" class="btn btn-actions mq-hos">Détails</a><h2 class="title"><a href="/fr/scp-aurore-de-thuin-julien-le-besco-paris-75">SCP Aurore de Thuin - Julien Le Besco</a></h2><div class="text"><p> 5, Rue Monceau -  75008 - PARIS - FRANCE</p></div></div></td></tr><tr><td><div class="box-result clearfix"><a href="/fr/scp-uguen-vidalenc-associes-paris-75" class="btn btn-actions mq-hos">Détails</a><h2 class="title"><a href="/fr/scp-uguen-vidalenc-associes-paris-75">SCP  Uguen/Vidalenc &amp;amp; Associés</a></h2><div class="text"><p> 64 AVENUE KLEBER -  75116 - PARIS - FRANCE</p></div></div></td></tr><tr><td><div class="box-result clearfix"><a href="/fr/scp-14-pyramides-notaires-paris-75" class="btn btn-actions mq-hos">Détails</a><h2 class="title"><a href="/fr/scp-14-pyramides-notaires-paris-75">SCP 14 Pyramides Notaires</a></h2><div class="text"><p> 14 RUE DES PYRAMIDES -  75001 - PARIS - FRANCE</p></div></div></td></tr>'''

	s2f = open('/tmp/NotairesLoader.2')
	sample2 = json.loads(s2f.read())
	s2f.close()

	print(sample2)

	for m in re.finditer(NotairesLoader.RE_SEARCH_INFO, sample2, flags=re.S+re.U+re.I):
		print("Match :")
		print("  - name : %s" % m.group('name'))
		print("  - address : %s" % m.group('address'))

#test_match()

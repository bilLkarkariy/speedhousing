# -*- coding: utf-8 -*-

import locale, calendar
try:
	locale.setlocale(locale.LC_ALL, 'fr_FR.utf8')
except: 
	pass

JOURS = [(None, u"Jour de la semaine")]
JOURS = JOURS + [
	(i, j.title()) 
	for i, j in enumerate(list(calendar.day_name))
]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from agenda.models import RDV
from sh.admin import admin_site
from trash.models import Trashable

# Register your models here.
@admin.register(RDV, site=admin_site)
class RDVAdmin(admin.ModelAdmin):

	list_display = ('html_mandat','_statut', 'vendeur', 'agent','acheteur','agent_client','_date','_hour')
	list_display_links = ('vendeur',)
	def get_query_set(self):
		return super(RDVAdmin, self).get_query_set().order_by('date');

	def _statut(self, obj):
		if obj.deleted:
			return "Annulé"
		elif obj.visite:
			return "Visité"
		else:
			return "En cours"
	def _date(self,obj):
		return u"%s" % (
			obj.crenau.strftime("%d/%m/%Y")
		)
	def _hour(self,obj):
		return u"%s" % (
			obj.crenau.strftime("%Hh%M")
		)
	def html_mandat(self, obj):
		return u"<a href='https://speedhousing.fr/admin/biens/bien/%s'>%s</a>" % (obj.bien.id,obj.bien.nb_mandat)
	html_mandat.short_description = "N° Mandat"
	_hour.short_description = "Heure"
	_statut.short_description = "Statut visite"
	html_mandat.allow_tags = True

# -*- coding: utf-8 -*-
from django.conf.urls import url

from agenda.views import (
	CalendrierAcheteurBienView,
	CalendrierAgentBienView,
	RDVAcheteurBienView,
	RDVAgentBienView,
	RDVDeleteView,
)

urlpatterns = [
	url(r'biens/(?P<nb_mandat>\d+)/$',
		CalendrierAcheteurBienView.as_view(),
		name='bien-calendrier'
	),
	url(r'biens/(?P<nb_mandat>\d+)/(?P<client_pk>\d+)/$',
		CalendrierAgentBienView.as_view(),
		name='bien-calendrier'
	),
	# Demande de RDV acheteur
	url(r'biens/(?P<nb_mandat>\d+)/rdv/(?P<crenau>'\
		'\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2})/$',
		RDVAcheteurBienView.as_view(),
		name='bien-rdv-acheteur'
	),
	# Creation de RDV agent pour agent_client
	url(r'biens/(?P<nb_mandat>\d+)/rdv/(?P<crenau>'\
		'\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2})'\
		'/(?P<agent_client_pk>\d+)/$',
		RDVAgentBienView.as_view(),
		name='bien-rdv-agent'
	),
	# Confirmation de RDV agent pour un client utilisateur
	url(r'biens/(?P<nb_mandat>\d+)/rdv/(?P<crenau>'\
		'\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2})'\
		'/client/(?P<client_pk>\d+)/$',
		RDVAgentBienView.as_view(),
		name='bien-confirmation-rdv-agent'
	),
	# Confirmation de RDV acheteur suite a annulation
	url(r'biens/(?P<nb_mandat>\d+)/rdv/(?P<crenau>'\
		'\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2})'\
		'/client/(?P<client_pk>\d+)/(?P<previous_pk>\d+)/$',
		RDVAgentBienView.as_view(),
		name='bien-reattribution-rdv-agent'
	),
	url(r'rdv/(?P<pk>\d+)/annuler/',
		RDVDeleteView.as_view(),
		name='annuler-rdv'
	)
]

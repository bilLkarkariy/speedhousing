# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from trash.models import Trashable

from agenda import JOURS
from biens.models import Bien, BienAValider
from utilisateurs.models import Agent, Client, AgentClient

from django.core.exceptions import ValidationError

from django.utils import timezone 

from dateutil.relativedelta import relativedelta
from dateutil.rrule import rrule, rruleset
from dateutil.rrule import MINUTELY, WEEKLY
import datetime

MAINTENANT = datetime.datetime.now()
DEMAIN = MAINTENANT + datetime.timedelta(days=1)
SEMAINE_PROCHAINE = MAINTENANT + relativedelta(weeks=1)

class Disponibilite(models.Model):
	bien = models.ForeignKey(Bien,
		related_name='disponibilites', null=True, default=None)
	bien_a_valider = models.ForeignKey(BienAValider,
		related_name='disponibilites_a_valider', null=True, default=None)
	jour = models.IntegerField(u"Jours de la semaines",
		choices=JOURS)
	debut = models.TimeField(u"Début",
		choices=[(None, u"Début")] + [
				(
					datetime.time(h, m),
					"%d:%02d" % (h, m)
				)
				for h in range(7, 22)
				for m in 0, 30
			]
		)
	fin = models.TimeField(u"Fin",
		choices=[(None, u"Fin")] + [
				(
					datetime.time(h, m),
					"%d:%02d" % (h, m)
				)
				for h in range(7, 22)
				for m in 0, 30
			]
		)

	def get_jour(self):
		return dict(JOURS)[self.jour]

	def __unicode__(self):
		return "Tous les %ss de %s à %s" % (
			self.get_jour(), self.debut, self.fin
		)

	def clean(self):
		if not self.debut < self.fin:
			raise ValidationError(
				u"La fin d'une disponibilité "
				u"ne peut être egale ou inferieur"
				u" a son début.")

	# def get_debut(self):
	# 	return MAINTENANT + relativedelta(
	# 		hours=+4,
	# 		hour=self.debut.hour,
	# 		minute=self.debut.minute,
	# 		weekday=self.jour,
	# 	)
	# def get_fin(self):
	# 	return self.get_debut()+datetime.timedelta(minutes=30)

	def get_weekly_rrule(self, start=None, end=None):

		if start is None:
			start = MAINTENANT
		if end is None:
			end = SEMAINE_PROCHAINE

		return rrule(WEEKLY, byweekday=self.jour,
			dtstart=start, until=end)

	def get_minutely_rrule(self, date=None):

		in_4h = MAINTENANT + relativedelta(hours=4,
			minute=30, second=0)
		start = datetime.datetime.combine(date, self.debut)
		end = datetime.datetime.combine(date, self.fin)\
			- datetime.timedelta(minutes=30)

		if start < in_4h:
			start = in_4h

		return rrule(MINUTELY, interval=30, byminute=(0, 30),
			dtstart=start, until=end)


class JourOff(models.Model):
	bien = models.ForeignKey(Bien, related_name='jours_off', default=None, null=True)
	bien_a_valider = models.ForeignKey(BienAValider, related_name='jours_off_a_valider', default=None, null=True)
	debut = models.DateField(u"Début")
	fin = models.DateField(u"Fin")

	def clean(self):
		if self.debut > self.fin:
			raise ValidationError(
				u"La fin d'une période "
				u"ne peut être inferieur"
				u" a son début."
			)

	def get_debut(self):
		return self.debut.strftime("%d/%m/%Y")

	def get_fin(self):
		return self.fin.strftime("%d/%m/%Y")

	def __unicode__(self):
		return u"du %s au %s" % (self.get_debut(), self.get_fin())

	def get_rrule(self):
		debut = datetime.datetime.combine(self.debut,
			datetime.time(7, 0))
		fin = datetime.datetime.combine(self.fin,
			datetime.time(22, 30))

		return rrule(MINUTELY, interval=30, 
			dtstart=debut, until=fin)

class RDV(Trashable):
	vendeur = models.ForeignKey(Client, related_name='vendeur_rdvs')
	
	agent = models.ForeignKey(Agent, related_name='rdvs')

	acheteur = models.ForeignKey(Client,
		null=True, blank=True, default=None,
		related_name='acheteur_rdvs')

	
	agent_client = models.ForeignKey(AgentClient,
		null=True, blank=True, default=None, related_name='rdvs')

	rdv_precedent = models.OneToOneField('RDV', related_name='rdv_suivant', blank=True, null=True)
	agent_suivant = models.PositiveIntegerField(default=0)

	bien = models.ForeignKey(Bien, related_name='rdvs')
	crenau = models.DateTimeField(u"Crénau")

	visite = models.DateTimeField(blank=True, null=True, default=None)

	class Meta(Trashable.Meta):
		get_latest_by = 'crenau'
		ordering = ['crenau']

	def get_fdt(self):
		return u"%s à %s" % (
			self.crenau.strftime("%d/%m/%Y"),
			self.crenau.strftime("%Hh%M")
		)

	def is_confirmed(self):
		if self.is_deleted():
			return False
		elif self.vendeur and self.agent and self.agent_client:
			return True
		elif self.vendeur and self.agent and self.acheteur:
			return True
		else:
			return False

	def in_past(self):
		return self.crenau < timezone.now()

	@property
	def acquereur(self):
		if self.acheteur:
			return self.acheteur
		return self.agent_client

	def has_offer(self):
		from django.db.models import Q
		return self.bien.offres.filter(agent=self.agent, acceptee=None, refusee=None) \
			.filter(
				(Q(agent_client__isnull=False)&Q(agent_client=self.agent_client))
				| (Q(acheteur__isnull=False)&Q(acheteur=self.acheteur))
			).exists()

	def is_sold(self):
		return self.bien.get_vendu()

	def __unicode__(self):
		# if self.acheteur:
		# 	return u"%s entre %s et %s" % (
		# 		self.get_fdt,
		# 		self.vendeur,
		# 		self.acheteur
		# 	)
		# elif self.agent and self.agent_client:
		# 	return u"%s entre %s et %s representé par %s" % (
		# 		self.get_fdt(),
		# 		self.vendeur,
		# 		self.agent_client,
		# 		self.agent
		# 	)
		# else: 
		return self.get_fdt()

	def get_client(self):
		if self.acheteur:
			return u"%s" % self.acheteur
		return u"%s" % self.agent_client

	def get_agents_passes(self):
		pk_agents_passes = []
		rdv = self
		while rdv:
			pk_agents_passes.append(rdv.agent.pk)
			rdv = rdv.rdv_precedent
		return pk_agents_passes

	@classmethod
	def get_agents_dispo(cls, crenau, localisation, exclude=[]):
		from django.contrib.gis.db.models.functions import Distance

		result = list(Agent.objects.exclude(
			valide=False,
		).annotate(
			distance_agent = Distance(
				'localisation',
				localisation
			)
		).order_by('distance_agent')[:10])

		return [agent for agent in result if agent.pk not in exclude]


# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponseBadRequest, Http404, HttpResponseRedirect
from django.views.generic.detail import DetailView
from django.views.generic.edit import (
	CreateView, DeleteView, FormView, UpdateView,
)
from django.contrib.auth.mixins import (
	LoginRequiredMixin, UserPassesTestMixin
)
from django.db.models import Q


from django.http import JsonResponse
from django.contrib import messages
from django.urls import reverse

from biens.models import Bien
from agenda.models import RDV
from utilisateurs.models import Agent, Client, AgentClient
from utilisateurs.mixins import AgentTestMixin
from utilisateurs.utils import add_logentry_agentavalider, notify, notify_email

from dateutil import parser
from dateutil.rrule import rruleset

import datetime

class CreneauInvalide(Http404):
	pass

class CreneauIncorrect(Http404):
	pass

class AcheteurTestMixin(LoginRequiredMixin, UserPassesTestMixin):
	def test_func(self):
		try:
			self.acheteur = self.request.user.client
			return True
		except Client.DoesNotExist:
			return False

class VendeurTestMixin(LoginRequiredMixin, UserPassesTestMixin):
	def test_func(self):
		try:
			self.vendeur = self.request.user.client
			return True
		except Client.DoesNotExist:
			return False

class BienDetailView(DetailView):
	model = Bien
	slug_field = 'nb_mandat'
	slug_url_kwarg = 'nb_mandat'

	def get_object(self, queryset=None):
		return Bien.objects.filter(nb_mandat=self.kwargs.get('nb_mandat', None)).exclude(valide=None,deleted=None).get()

	def clean_crenau(self, bien, crenau):

		try:
			crenau = parser.parse(crenau)
		except ValueError:
			raise CreneauIncorrect()

		debut = crenau - datetime.timedelta(minutes=30)
		fin = crenau + datetime.timedelta(minutes=30)

		if not crenau in list(bien.get_rruleset(debut, fin)):
			raise CreneauInvalide()

		return crenau

class CalendrierAgentBienView(AgentTestMixin, BienDetailView):
	template_name = 'agenda/bien_calendrier.html'

	def get_context_data(self, **kwargs):
		context = super(CalendrierAgentBienView,
			self).get_context_data(**kwargs)
		context['client'] = get_object_or_404(
			AgentClient,
			pk=self.kwargs['client_pk'],
			agent=self.agent
		)
		return context

	def get(self, request, *args, **kwargs):

		if not request.is_ajax():
			return super(CalendrierAgentBienView,
				self).get(request, *args, **kwargs)


		try:
			start = parser.parse(request.GET.get('start'))
			end = parser.parse(request.GET.get('end'))
			crenaux = []
			bien = self.get_object()
			client_pk = self.kwargs.pop('client_pk')
			agent = self.agent

		except ValueError:
			return HttpResponseBadRequest("bad request")


		for rdv in agent.rdvs.filter(crenau__gt=start, crenau__lt=end, deleted=None):
			crenaux.append({
				'title': "RDV avec %s" % rdv.agent_client,
				'start': rdv.crenau,
				'end': rdv.crenau + datetime.timedelta(minutes=30),
				'color': '#d7d7d7',
				'textColor': '#333',
				'url': reverse(
					'agent-client',
					args=[rdv.agent_client.pk]
				) + "?next=" + request.path,
			})

		for c in bien.get_rruleset(start, end):
			crenaux.append({
				'title': u"Créneau de %s" % c.strftime("%Hh%M"),
				'start': c,
				'end': c + datetime.timedelta(minutes=30),
				'url': reverse(
					'bien-rdv-agent',
					args=[
						bien.nb_mandat,
						str(c),
						client_pk
					]
				)

			})

		return JsonResponse(crenaux, safe=False)



class CalendrierAcheteurBienView(AcheteurTestMixin, BienDetailView):
	template_name = 'agenda/bien_calendrier.html'

	def get(self, request, *args, **kwargs):

		if not request.is_ajax():
			return super(CalendrierAcheteurBienView,
				self).get(request, *args, **kwargs)

		try:
			start = parser.parse(request.GET.get('start'))
			end = parser.parse(request.GET.get('end'))
			crenaux = []
			bien = self.get_object()
			acheteur = self.acheteur
		except:
			return HttpResponseBadRequest()


		for rdv in acheteur.acheteur_rdvs.filter(deleted=None):
			crenaux.append({
				'title': "Visite avec %s" % rdv.agent,
				'start': rdv.crenau,
				'end': rdv.crenau + datetime.timedelta(minutes=30),
				'color': '#d7d7d7',
				'textColor': '#333',
				'url': reverse('annonce', args=[rdv.bien.nb_mandat])
			})

		for c in bien.get_rruleset(start,end):
			if any(x['start'] == c for x in crenaux):
				continue
			crenaux.append({
				'title': u"Créneau de %s " % c.strftime("%Hh%M"),
				'start': c,
				'end': c + datetime.timedelta(minutes=30),
				'url': reverse(
					'bien-rdv-acheteur',
					args=[
						bien.nb_mandat,
						str(c)
					]
				),
			})

		return JsonResponse(crenaux, safe=False)



class RDVAcheteurBienView(AcheteurTestMixin, BienDetailView):

	def get(self, request, *args, **kwargs):

		bien = self.get_object()
		crenau = self.clean_crenau(bien, kwargs.pop('crenau'))

		rdv = self.acheteur.acheteur_rdvs.filter(
			bien=bien, visite=None, deleted=None).first()

		if rdv:
			messages.error(request,
				"Vous avez déjà un RDV prévu"\
					" %s pour ce bien. Merci de l'annuler"\
					" ou de valider votre visite, si vous"\
					" l'avez déjà effectué" % rdv.get_fdt()
			)
		else:

			agents = RDV.get_agents_dispo(crenau, self.acheteur.localisation)

			if len(agents) > 0:
				url_action = request.build_absolute_uri(
					reverse('bien-confirmation-rdv-agent', kwargs={"nb_mandat":bien.nb_mandat, "crenau":str(crenau), "client_pk": self.acheteur.pk})
				).replace('http://', 'https://')

				for agent in agents:
					notify(agent.user, u'Demande de rendez-vous pour un potentiel acquéreur')
					notify_email(agent.email, u"Demande de rendez-vous pour un potentiel acquéreur", u"email/pro/nouvelle_visite.html", {
						u"fullname": agent.get_full_name(),
						u"fullname_client": self.acheteur.get_full_name(),
						u"titre_annonce": bien,
						u"reference": bien.nb_mandat,
						u"date": crenau,
						u"url_action": url_action
					})

				messages.info(request, "Votre demande de visite pour le "\
					"%s a bien été prise en compte. Celle-ci est" \
					" désormais en attente de confirmation par"\
					" un agent du secteur." % (
						u"%s à %s" % (
							crenau.strftime("%d/%m/%Y"),
							crenau.strftime("%Hh%M")
						)
					)
				)
			else:
				messages.error(request, "Aucun agent n'est disponible"\
				" actuellement dans votre secteur !")

		return redirect(reverse('annonce', args=[bien.nb_mandat]))


class RDVAgentBienView(AgentTestMixin, BienDetailView):

	def get(self, request, *args, **kwargs):

		bien = self.get_object()

		try:
			crenau = self.clean_crenau(bien, kwargs.pop('crenau'))
		except CreneauInvalide:
			if 'client_pk' in kwargs:
				messages.error(request, "Ce rendez-vous a déjà été confirmé.")
			else:
				messages.error(request, "Crénau invalide.")
			return redirect(
				reverse('annonce', args=[bien.nb_mandat])
			)

		# verifications ...
		if 'agent_client_pk' in kwargs:
			client = get_object_or_404(
				AgentClient,
				pk=kwargs.pop('agent_client_pk'),
				agent=self.agent
			)
			rdv = self.agent.rdvs.filter(
				agent_client=client, visite=None, bien=bien, deleted=None).first()
		elif 'client_pk' in kwargs:
			client = get_object_or_404(
				Client,
				pk=kwargs.pop('client_pk')
			)
			rdv = self.agent.rdvs.filter(
				acheteur=client, visite=None, bien=bien, deleted=None).first()

		if rdv:
			messages.error(request, "Vous avez déjà une visite prévue"\
				" avec %s pour ce bien %s." % (
					client, rdv.get_fdt()
				)
			)
		else:
			if type(client) == AgentClient:
				rdv = RDV(vendeur=bien.vendeur, agent=self.agent, agent_client=client,
					bien=bien, crenau=crenau)
			elif 'previous_pk' in kwargs:
				prev_rdv = RDV.trash.exclude(agent=self.agent).get(pk=kwargs.pop('previous_pk'), vendeur=bien.vendeur, acheteur=client)

				if hasattr(prev_rdv, 'rdv_suivant'):
					messages.error(request, "Le rendez-vous a déjà été confirmé par %s." % (
							prev_rdv.rdv_suivant.agent.get_full_name()
						)
					)
					return redirect(
						reverse('annonce', args=[bien.nb_mandat])
					)

				if self.agent.pk in prev_rdv.get_agents_passes():
					messages.error(request, "Vous avez déjà annulé visite pour ce bien %s." % (
							prev_rdv.get_fdt()
						)
					)
					return redirect(
						reverse('annonce', args=[bien.nb_mandat])
					)

				rdv = RDV(vendeur=bien.vendeur, agent=self.agent, acheteur=client,
					bien=bien, crenau=crenau, rdv_precedent=prev_rdv, agent_suivant=prev_rdv.agent_suivant+1)
			else:
				rdv = RDV(vendeur=bien.vendeur, agent=self.agent, acheteur=client,
					bien=bien, crenau=crenau)

			rdv.save()

			url_action = request.build_absolute_uri(
				reverse('annuler-rdv', kwargs={"pk": rdv.pk})
			).replace('http://', 'https://')

			notify(bien.vendeur.user, u'Demande de rendez-vous pour un potentiel acquéreur')
			notify_email(bien.vendeur.email, u"Demande de rendez-vous par un potentiel acquéreur", u"email/vendeur/nouvelle_visite.html", {
				u"fullname": bien.vendeur.get_full_name(),
				u"fullname_agent": self.agent.get_full_name(),
				u"titre_annonce": bien,
				u"reference": bien.nb_mandat,
				u"date": rdv.crenau,
				u"url_action": url_action,
			})

			notify(self.agent.user, u'Confirmation de rendez-vous pour un potentiel acquéreur')
			notify_email(self.agent.email, u"Confirmation de rendez-vous pour un potentiel acquéreur", u"email/pro/confirmation_visite.html", {
				u"fullname": self.agent.get_full_name(),
				u"fullname_client": client.get_full_name(),
				u"titre_annonce": bien,
				u"reference": bien.nb_mandat,
				u"date": rdv.crenau
			})

			if type(client) == Client:
				client_user = client.user
				client_email = client.email
				client_full_name = client.get_full_name()

				notify(client_user, u"Votre demande de visite vient d'être confirmée par un agent")
				notify_email(client_email, u"Confirmation de votre demande de rendez-vous", u"email/acheteur/confirmation_nouvelle_visite.html", {
					u"fullname": client_full_name,
					u"reference": rdv.bien.nb_mandat,
					u"titre_annonce": rdv.bien,
					u"date": rdv.crenau
				})

			messages.info(request, "Votre prise de rendez-vous pour le "\
				"%s a bien été prise en compte. Le vendeur du bien a" \
				" été notifié." % rdv.get_fdt()
			)

		return redirect(
			reverse('annonce', args=[bien.nb_mandat])
		)



class RDVDeleteView(LoginRequiredMixin, DeleteView):
	model = RDV

	def get_success_url(self):
		next_page = self.request.GET.get('next', None)
		if next_page:
			return next_page

		next_page = self.request.POST.get('next', None)
		if next_page:
			return next_page

		return self.request.META.get('HTTP_REFERER')

	def get(self, request, *args, **kwargs):
		try:
			rdv = self.get_object()
		except Http404 as e:
			if RDV.trash.filter(pk=kwargs['pk']).exists():
				messages.warning(request, "Ce rendez-vous a déjà été annulé.")
				rdv = RDV.trash.get(pk=kwargs['pk'])
				return redirect(
					reverse('utilisateurs:list-visites') if hasattr(request.user, 'agent') and request.user.agent
					else reverse('utilisateurs:mes-annonces-visites', args=[rdv.bien.nb_mandat])
				)
			raise e
		return super(RDVDeleteView, self).get(request, *args, **kwargs)

	def post_agent(self, request, *args, **kwargs):
		rdv = self.get_object()
		result = super(RDVDeleteView, self).post(request, *args, **kwargs)

		# l'acheteur a pris rdv => chercher un autre agent (workflow)
		if rdv.acheteur:
			agents = rdv.get_agents_dispo(rdv.crenau, rdv.acheteur.localisation, exclude=rdv.get_agents_passes())
			bien = rdv.bien

			url_action = request.build_absolute_uri(
				reverse('bien-reattribution-rdv-agent', kwargs={"nb_mandat":bien.nb_mandat, "crenau":rdv.crenau, "client_pk": rdv.acheteur.pk, "previous_pk": rdv.pk})
			).replace('http://', 'https://')

			if len(agents) > 0:
				for agent in agents:
					notify(agent.user, u'Demande de rendez-vous pour un potentiel acquéreur')
					notify_email(agent.email, u"Demande de rendez-vous pour un potentiel acquéreur", u"email/pro/nouvelle_visite.html", {
						u"fullname": agent.get_full_name(),
						u"fullname_client": rdv.acquereur.get_full_name(),
						u"titre_annonce": bien,
						u"reference": bien.nb_mandat,
						u"date": rdv.crenau,
						u"url_action": url_action
					})

				messages.info(request, "Votre rendez-vous avec %s vient d'être "\
					"annulé." % rdv.acheteur)
			else:
				if rdv.acheteur is not None:
					client_user = rdv.acheteur.user
					client_email = rdv.acheteur.email
					client_full_name = rdv.acheteur.user.get_full_name()
				else:
					client_user = None
					client_email = rdv.agent_client.email
					client_full_name = rdv.agent_client.get_full_name()

				notify(client_user, u'Votre rendez-vous a été annulé')
				notify_email(client_email, u"Votre rendez-vous a été annulé", u"email/acheteur/visite_annulee.html", {
					u"fullname": client_full_name,
					u"titre_annonce": rdv.bien,
					u"reference": rdv.bien.nb_mandat,
					u"date": rdv.crenau
				})

				messages.error(request, "Aucun autre agent n'est"\
				" disponible actuellement dans ce secteur. "\
				" Merci de contacter le support.")

			notify(rdv.vendeur.user, u'Votre rendez-vous a été annulé')
			notify_email(rdv.vendeur.email, u"Votre rendez-vous a été annulé", u"email/vendeur/visite_annulee.html", {
				u"fullname": rdv.vendeur.get_full_name(),
				u"fullname_agent": rdv.agent.get_full_name(),
				u"titre_annonce": rdv.bien,
				u"reference": rdv.bien.nb_mandat,
				u"date": rdv.crenau
			})

			notify(rdv.agent.user, u'Votre rendez-vous a été annulé')
			notify_email(rdv.agent.email, u"Votre rendez-vous a été annulé", u"email/pro/visite_annulee_par_acheteur.html", {
				u"fullname": rdv.agent.get_full_name(),
				u"titre_annonce": rdv.bien,
				u"reference": rdv.bien.nb_mandat,
				u"date": rdv.crenau
			})

			return redirect(self.get_success_url())


		# L'agent a pris rdv pour son client => notification du vendeur
		elif rdv.agent_client:
			# notification du vendeur
			notify(rdv.vendeur.user, u'Nouvelle demande de visite')
			notify_email(rdv.vendeur.email, u"Nouvelle demande de visite", u"email/vendeur/nouvelle_visite.html", {
				u"fullname": rdv.vendeur.get_full_name(),
				u"titre_annonce": rdv.bien,
				u"reference": rdv.bien.nb_mandat,
				u"date": rdv.crenau
			})
			if rdv.agent_client.notification:
				notify(rdv.agent.user, u'Nouvelle demande de visite')
				notify_email(rdv.agent.email, u"Nouvelle demande de visite", u"email/pro/confirmation_visite.html", {
					u"fullname": rdv.agent.get_full_name(),
					u"fullname_client": rdv.agent_client.get_full_name(),
					u"titre_annonce": rdv.bien,
					u"reference": rdv.bien.nb_mandat,
					u"date": rdv.crenau
				})
			messages.info(
				request, "Votre RDV avec %s vient d'etre annulé" % rdv.vendeur)

		return result

	def post_client(self, request, *args, **kwargs):
		rdv = self.get_object()
		result = super(RDVDeleteView, self).post(request, *args, **kwargs)

		# L'acheteur demande l'annulation
		if rdv.acheteur == self.client:
			# notification de l'agent et du vendeur
			messages.error(request, "Votre rendez-vous avec %s pour l'annonce : %s "\
				"a bien été annulé." % (rdv.agent, rdv.bien)
			)

			notify(rdv.vendeur.user, u'Votre rendez-vous a été annulé')
			notify_email(rdv.vendeur.email, u"Votre rendez-vous a été annulé", u"email/vendeur/visite_annulee.html", {
				u"fullname": rdv.vendeur.get_full_name(),
				u"fullname_agent": rdv.agent.get_full_name(),
				u"titre_annonce": rdv.bien,
				u"reference": rdv.bien.nb_mandat,
				u"date": rdv.crenau
			})

			notify(rdv.agent.user, u'Votre rendez-vous a été annulé')
			notify_email(rdv.agent.email, u"Votre rendez-vous a été annulé", u"email/pro/visite_annulee_par_acheteur.html", {
				u"fullname": rdv.agent.get_full_name(),
				u"titre_annonce": rdv.bien,
				u"reference": rdv.bien.nb_mandat,
				u"date": rdv.crenau
			})

		# le vendeur demande l'annulation
		elif rdv.vendeur == self.client:
			if rdv.acheteur is not None:
				client_user = rdv.acheteur.user
				client_email = rdv.acheteur.email
				client_full_name = rdv.acheteur.user.get_full_name()
			else:
				client_user = None
				client_email = rdv.agent_client.email
				client_full_name = rdv.agent_client.get_full_name()

			if client_user:
				notify(client_user, u'Votre rendez-vous a été annulé')

			notify_email(client_email, u"Votre rendez-vous a été annulé", u"email/acheteur/visite_annulee.html", {
				u"fullname": client_full_name,
				u"titre_annonce": rdv.bien,
				u"reference": rdv.bien.nb_mandat,
				u"date": rdv.crenau
			})

			if rdv.agent_client is not None:
				if rdv.agent_client.notification:
					notify(rdv.agent.user, u'Votre rendez-vous a été annulé')
					notify_email(rdv.agent.email, u"Votre rendez-vous a été annulé", u"email/pro/visite_annulee_par_vendeur.html", {
						u"fullname": rdv.agent.get_full_name(),
						u"titre_annonce": rdv.bien,
						u"reference": rdv.bien.nb_mandat,
						u"date": rdv.crenau
					})

			messages.info(request, "Votre visite a été annulée.")

		return result

	def post(self, request, *args, **kwargs):
		try:
			rdv = self.get_object()
		except Http404 as e:
			if RDV.trash.filter(pk=kwargs['pk']).exists():
				messages.warning(request, "Ce rendez-vous a déjà été annulé.")
				rdv = RDV.trash.get(pk=kwargs['pk'])
				return redirect(
					reverse('utilisateurs:list-visites') if hasattr(request.user, 'agent') and request.user.agent
					else reverse('utilisateurs:mes-annonces-visites', args=[rdv.bien.nb_mandat])
				)
			raise e

		if hasattr(request.user, 'agent') and request.user.agent:
			self.agent = request.user.agent
			self.queryset = RDV.objects.filter(agent=self.agent, deleted=None)
			return self.post_agent(request, *args, **kwargs)
		elif hasattr(request.user, 'client') and request.user.client:
			self.client = request.user.client
			self.queryset = RDV.objects.filter(deleted=None).filter(
				Q(vendeur=self.client)|Q(acheteur=self.client)
			)
			return self.post_client(request, *args, **kwargs)

		return redirect(self.get_success_url())
